---
title: Häfner Technology
layout: page
cover: /images/haefner.technology.logo.png
---

<section class="tiles">
	<article class="style1">
		<span class="image"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-00cover-index.jpg" alt="Tesla Handy Supercharger" /></span>
		<a href="/en/categories/Instructions/">
			<h2>Instructions</h2>
			<div class="content">
				<p>Do it yourself Instructions</p>
			</div>
		</a>
	</article>
</section>

<section>
	We support an add-free and open internet: <a href="https://brave.com/hfn794">Brave-Browser</a>
</section>

