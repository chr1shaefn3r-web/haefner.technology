﻿---
title: Hairy Lion
categories: Instructions
tags:
 - ExcitingPrint
 - Lion
 - Selfmade
 - Make
 - Ultimaker2
 - Season1
date: 2018/04/02 18:00:00
cover_index: /images/anleitung/hairy-lion/haefner-technology-hairy-lion-cover-index.jpg
amazon: true
sitemap: true
translator: Nicole Eigenmann
translatorLink: mailto:nicoleeigenmann@posteo.de
language: en
---
Hello everyone!
This instruction aims to show you how easy it is to get your own, 3-D printed Hairy Lion(s). One of the possible results can be seen in the picture below.
<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-fertig1.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-fertig1_small.jpg" alt="Hairy Lion, gold" /></a></span></div>
	</div>
</div>

## Materials and components for your own 3-D printer

 * 1x [Hairy Lion](https://www.thingiverse.com/thing:2007221)

<a href="https://www.3dhubs.com/service/83913" data-3dhubs-widget="button" data-hub-id="83913" data-type="orderWidget" data-color="light" data-size="large" data-text="Order via 3D-Hubs from us">Order via 3D-Hubs from us</a>
<script>!function(a,b,c,d){var e,g=(a.getElementsByTagName(b)[0],/^http:/.test(a.location)?"http":"https");a.getElementById(d)||(e=a.createElement(b),e.id=d,e.src=g+"://d3d4ig4df637nj.cloudfront.net/w/2.0.js",e.async=!0,a.body.appendChild(e))}(document,"script",1,"h3d-widgets-js");</script>

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u$"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-unbearbeitet.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-unbearbeitet_small.jpg" alt="Hairy Lion, freshly printed" /></a></span></div>
	</div>
</div>

## Removing the wall

As soon as the blank of the lion is sitting in front of you, it is time for the lion to reveal its mane. To do that, the wall surrounding its head needs to be carefully removed. Start by carefully cutting the ends of the fine hairs that are closest to the wall using slender scissors. We recommend cutting rings, removing them step by step as shown in the pictures below. 

<div class="box alt">
	<div class="row uniform">
		<div class="4u"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-haare-abschneiden.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-haare-abschneiden_small.jpg" alt="cutting the mane" /></a></span></div>
		<div class="4u"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-stuetzwand-entfernen.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-stuetzwand-entfernen_small.jpg" alt="removing the ring" /></a></span></div>
		<div class="4u$"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-stuetzwand-einschneiden.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-stuetzwand-einschneiden_small.jpg" alt="removing the supporting wall" /></a></span></div>
	</div>
</div>Repeat the process until the wall is completely removed. If done correctly, the result should look like the one in the picture below:

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u$"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-stuetzwand-komplett-entfernt.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-stuetzwand-komplett-entfernt_small.jpg" alt="wall completely removed" /></a></span></div>
	</div>
</div>

## Time to customize your Lion’s mane!

This is probably the most interesting part because you can customize the lion’s mane according to your own specifications. You will get the best result with using a  [hot air gun](https://www.amazon.de/dp/B0001D1Q2M/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=hairy-lion-21&linkId=cbfa64eca758eca08c7429ee82b63219).Using a blow dryer might also work but the hot air gun offers individually adjusting the temperature starting at the second heating level.In case you own this specific hot air gun or a similar one: We found out that a temperature of 100°C on the 2nd heating level proved to achieve the best results. Imagine blow-drying your own hair and transfer that process to the air gun and the lion: Let the heated hot air gun face the plastic feline predator using waving motions. Be careful not to blow-dry a single spot for too long since the hairs might melt and clump. Apart from that, the mane-styling is completely left up to your imagination.

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-foenen1.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-foenen1_small.jpg" alt="blow-drying" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-foenen2.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-foenen2_small.jpg" alt="continuing the styling process" /></a></span></div>
	</div>
</div>One possible result can be seen in the picture below:

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-fertig2.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-fertig2_small.jpg" alt="Your personalized lion" /></a></span></div>
	</div>
</div>


## Materials required

 * 1x   Hairy Lion

## Tools required

 * Scissors
 * [hot air gun](https://www.amazon.de/dp/B0001D1Q2M/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=hairy-lion-21&linkId=cbfa64eca758eca08c7429ee82b63219)

