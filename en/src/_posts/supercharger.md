---
title: Supercharger
categories: Instructions
tags:
 - Supercharger
 - smartphonecharger
 - smartphone
 - Season3
 - selfmade
date: 2019/04/01 13:37:00
cover_index: /images/anleitung/supercharger/haefner-technology-supercharger-00cover-index.jpg
amazon: true
sitemap: true
translator: Nicole Eigenmann
translatorLink: mailto:nicoleeigenmann@posteo.de
language: en
---
The charging station TESLA-enthusiasts have been waiting for:
<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-00Titelbild.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-00Titelbild.jpg" alt="cablebox, glued in place" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Total costs: 45 &euro;
Knowledge necessary: none
Time necessary: approx. 20 minutes

## MATERIALS AND COMPONENTS


 * 1x [frontplate](/druckmodelle/anleitung/supercharger/tesla-supercharger-frontplate.stl) (white)
 * 1x [backplate](/druckmodelle/anleitung/supercharger/tesla-supercharger-backplate.stl) (white)
 * 2x [rail](/druckmodelle/anleitung/supercharger/tesla-supercharger-rail.stl) (anthracite)
 * 1x [inlay](/druckmodelle/anleitung/supercharger/tesla-supercharger-inlay.stl) (red)
 * 1x [cablebox](/druckmodelle/anleitung/supercharger/tesla-supercharger-cablebox.stl) (black)
 * 1x [base](/druckmodelle/anleitung/supercharger/tesla-supercharger-base.stl) (anthracite)
 * 1x [stand](/druckmodelle/anleitung/supercharger/tesla-supercharger-stand.stl) (anthracite)
 * 1x [Tesla-background](/druckmodelle/anleitung/supercharger/tesla-supercharger-tesla-background.stl) (red)

## assembly

As we begin with the Supercharger's assembly, the first step is gluing the cablebox to the inlay. All you need is some superglue and a bit of dexterity.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-01Cablebox.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-01Cablebox_small.jpg" alt=" cablebox, glued in place" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Next up is inserting the inlay to the back cover plate. The difference to the front cover plate is that the cable duct can be found on the right. Due to the parts' accuracy of fit, it will work best if you start inserting it on the upper flat side towards the bottom. If you are having a hard time doing so, you may try deburring it a bit more with a carpet knife.

<div class="box alt">
	<div class="row uniform">
		<div class="1u"></div>
		<div class="5u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-02Backplate.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-02Backplate_small.jpg" alt="back cover plate" /></a></span></div>
		<div class="5u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-03BackplateTop.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-03BackplateTop_small.jpg" alt="back cover plate top" /></a></span></div>
		<div class="1u$"></div>
	</div>
</div>

Next up is inserting the charging cable. It is important to start inserting the charging cable the right way through the base component.

<div class="box alt">
	<div class="row uniform">
		<div class="1u"></div>
		<div class="5u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-03BaseMitKabel.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-03BaseMitKabel_small.jpg" alt="base, cable inserted" /></a></span></div>
		<div class="5u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-04BackplateMitKabel.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-04BackplateMitKabel_small.jpg" alt="backplate cover, cable inserted" /></a></span></div>
		<div class="1u$"></div>
	</div>
</div>

Next up is adding the TESLA background.

<div class="box alt">
	<div class="row uniform">
		<div class="1u"></div>
		<div class="5u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-05TeslaBackground.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-05TeslaBackground_small.jpg" alt="Tesla background" /></a></span></div>
		<div class="5u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-06BackplateMitKabelCloseUp.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-06BackplateMitKabelCloseUp_small.jpg" alt="back cover plate with cable, close-up" /></a></span></div>
		<div class="1u$"></div>
	</div>
</div>

The next step is putting on the front cover plate.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-07Frontplatte.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-07Frontplatte_small.jpg" alt="front cover plate, inserted" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

The charger can be added to the base after adding the rails to the left and the right side. 

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-08Base.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-08Base_small.jpg" alt="base, inserted" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

The only thing that is left to do is inserting the stand.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-00Titelbild.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-00Titelbild.jpg" alt="cablebox inserted" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

## Materials required


 * 1x [Anker 24W 2-port](https://www.amazon.de/gp/product/B00WLI5E3M/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=supercharger-21&creative=6742&linkCode=as2&creativeASIN=B00WLI5E3M&linkId=75ba4b5c5ce410c76c96b7dedb2a9109)
 * 1x black charging cable, such as:
[Anker 2x 1,8m Micro-USB](https://www.amazon.de/gp/product/B01MUJOA5E/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=supercharger-21&creative=6742&linkCode=as2&creativeASIN=B01MUJOA5E&linkId=a23c3c8b7dea082ad1253e6647127dc0) 
[Anker 1x 0,9m USB-C](https://www.amazon.de/gp/product/B01A6F3WHG/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=supercharger-21&creative=6742&linkCode=as2&creativeASIN=B01A6F3WHG&linkId=9c50c69fd6c2720c814b100230ca4ceb)
[Anker 1x 0,9m Apple Lightning](https://www.amazon.de/gp/product/B01N8W1EAE/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=supercharger-21&creative=6742&linkCode=as2&creativeASIN=B01N8W1EAE&linkId=12014efb05367c7ce5dc3fbc805b5134)

