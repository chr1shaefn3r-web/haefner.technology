---
title: LED-Tannenbaum
categories: Anleitung
tags:
 - LED-Tannenbaum
 - LED
 - Tannenbaum
 - Season4
 - Selfmade
date: 2019/12/01 13:37:42
cover_index: /images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-cover-index.jpg
amazon: false
sitemap: true
---
Weihnachten steht vor der Türe und sicherlich haben die meisten von euch schon die Weihnachtsdekoration herausgeholt. Passend dazu heute unsere Anleitung für einen beleuchteten LED-Tannenbaum! Viel Spaß beim lesen!

<div class="box alt"> <div class="row uniform"> <div class="12u$"><span class="image fit"><a href="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-00Titelbild.jpg"><img src="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-00Titelbild.jpg" alt="LED-Tannenbaum" /></a></span></div> </div> </div>

Gesamtkosten: ca. 10-20 €
Vorkenntnisse: machbar ohne jegliche Kenntnisse. Etwas Geschick, Löt- und Elektronikkenntnisse schaden nicht.
Zeitaufwand: ca. 3h


## Druckteile

Der LED-Tannenbaum besteht aus zwei Druckteilen, dem Baum an sich und dem Einsatz, an dem die Elektronik angebracht ist. Der Einsatz kann dann einfach in den Baum eingeschraubt werden.

* 1x [Tannenbaum](/druckmodelle/anleitung/led-tannenbaum/Tannenbaum.stl)
* 1x [Einsatz](/druckmodelle/anleitung/led-tannenbaum/Einsatz.stl)

Wie immer beim 3D-Druck könnt ihr hier eure eigene Farbkombination wählen. Da wir ein transluzentes Grün für den Baum gewählt haben sollte der Einsatz nicht sichtbar sein, weshalb wir für uns für "natur" entschieden haben.

Das Original-Modell des Baumes wurde abgewandelt aus idig3Ds "Christmas Tree" von [thingiverse.com](https://www.thingiverse.com/thing:1913982)
Das Modell wurde unter der ["Creative Commons - Atribution - Non-Commercial - No Derivates"](https://creativecommons.org/licenses/by-nc-nd/3.0/) Lizenz veröffentlicht.
Alle Rechte liegen dort. 


So sah der Basteltisch übrigens vor Beginn aus:

<div class="box alt"> <div class="row uniform"> <div class="12u$"> <span class="image fit">
<a href="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-01uebersicht.jpg">
<img src="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-01uebersicht.jpg" alt="Übersicht der Bauteile" />
</a> </span> </div> </div> </div>

## Der Aufbau

Wir beginnen mit dem Aufkleben der LEDs und Widerstände. Ihr solltet unbedingt überprüfen, ob die LEDs auch wirklich funktionieren! Solltet ihr die LEDs gekauft haben, die wir auch verwendet haben, so müsst ihr darauf achten, dass die abgeflachte Ecke nach UNTEN LINKS ausgerichtet ist. Dies ist übrigens der Minus-Pol. Im Allgemeinen werden wir die Elektronik hier so aufbauen, dass wir oben den Positiven Pol und unten den Negativen Pol haben. Einfach einen kleinen Klecks Sekundenkleber in den extra eingebrachten Vertiefungen auftragen (es reicht wirklich eine minimale Menge) und die LED mit Hilfe einer Pinzette andrücken. Achtet darauf, dass die LEDs mittig sitzen, so dass die drei kleinen Füße zu den Leiterbahn-Vertiefungen passen. Genauso könnt ihr auch mit den Widerständen verfahren, hier spielt die Polung allerdings keine Rolle. Achtet hierbei darauf, dass die Widerstände einigermaßen gerade und mittig zur Vertiefung der Leiterbahnen sind.

<div class="box alt"> <div class="row uniform"> <div class="6u"> <span class="image fit">
<a href="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-02LED.jpg">
<img src="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-02LED_small.jpg" alt="LED von Nahem" />
</a> </span> </div> <div class="6u$"> <span class="image fit">
<a href="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-03LEDs+Widerstaende.jpg">
<img src="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-03LEDs+Widerstaende_small.jpg" alt="LEDs und Widerstände in der Übersicht" />
</a> </span> </div> </div> </div>

Diesen Vorgang könnte ihr jetzt rundherum noch vier mal wiederholen bis alle fünf Seiten bestückt sind.

Nun kommt ein etwas kniffliger Teil. Nun werden die LEDs und Widerstände entlang der "Verbindungsgräben" mit Hilfe von Silberleitlack verbunden. Dazu den Stift gut schütteln und den Lack einigermaßen großzügig wie eine Leiterbahn in den Linien verteilen. Achtet aber darauf, dass der Lack nicht in die Bahn daneben gelangt. Die LEDs haben wie schon beschrieben kleine Füße, die es gilt mit den Bahnen zu verbinden.
Auch solltet ihr den Siberleitlack gut trocknen lassen, weil er dann erst seine vollständige Leitfähigkeit ausbildet.

TIPP: Wenn ihr von der Rückseite mit einer hellen Lampe durch den Einsatz durchleuchtet, so seht ihr viel besser, ob irgendwo eine Verbindung besteht, die nicht sein soll, bzw. ob irgendwo etwas nicht verbunden ist, was verbunden gehört.

Die nächsten beiden Bilder zeigen die Verdrahtung und die reale am Einsatz.

<div class="box alt"> <div class="row uniform"> <div class="3u"> <span class="image fit">
<a href="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-04Verdrahtung.jpg">
<img src="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-04Verdrahtung.jpg" alt="Verdrahtung" />
</a> </span> </div> <div class="9u$"> <span class="image fit">
<a href="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-05Leiterbahn.jpg">
<img src="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-05Leiterbahn_small.jpg" alt="Silberleitlack-Leiterbahn" />
</a> </span> </div> </div> </div>

Jetzt geht es an die Batteriehalter. Dazu seht ihr im folgenden einen Screenshot aus dem CAD.

<div class="box alt"> <div class="row uniform"> <div class="2u"> </div> <div class="8u"> <span class="image fit">
<a href="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-06BohrungenSchalter.jpg">
<img src="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-06BohrungenSchalter.jpg" alt="Die besagten Bohrungen" />
</a> </span> </div> <div class="2u$"></div> </div> </div>

An der Seite, wo ihr diese beiden Bohrungen findet fang ihr nun an, die Batteriehalter aufzukleben und zwar den ersten mit dem Minuspol UNTEN, den zweiten (wenn ihr den Einstaz nach LINKS weiterdreht, sprich rechts daneben) mit dem Minuspol OBEN), den dritten mit dem Minuspol wieder UNTEN usw. Wir haben hier 2-Komponentenkleber verwendet. Auch hier reicht ein kleiner Streifen, und dann den Halter sauber ausrichten und aufdrücken.

TIPP: Bevor ihr die Halter aufklebt könnt ihr die Lötfahnen etwas nach oben biegen, sodass ihr später besser dran kommt um zu Löten.


Habt ihr alle Halter angeklebt geht es nun ans Verbinden. Damit unsere Beleuchtung nachher auch genug Saft hat müssen wir alle fünf Batterien in Reihe schalten, sprich hintereinander hängen. Dafür sind die schrägen versetzten Bohrungen oben und unten gedacht. Hier könnt ihr ein Kabel durchfädeln und einfach den Pluspol des einen Batteriehalters mit dem Minuspol des nächsten Batteriehalters verbinden. Sprich es geht beim ersten oben los (da wo wir auch mit dem Festkleben angefangen haben) und gehen ebenfalls wie beim Kleben nach rechts weiter. Halter zwei und drei werden nun unten verbunden, Halter drei und vier oben usw. bis ihr einmal rum seid. Sofern ihr genau nach Anleitung vorgegangen seid solltet ihr quasi beim letzten Halter OBEN am Ende sein.
Merkt euch dies, hier haken wir später nochmal ein.

<div class="box alt"> <div class="row uniform"> <div class="6u"> <span class="image fit">
<a href="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-07BohrungenBatterien.jpg">
<img src="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-07BohrungenBatterien.jpg" alt="Die besagten Bohrungen oben und unten" />
</a> </span> </div> <div class="6u$"> <span class="image fit">
<a href="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-08BatteriehalterVerbunden.jpg">
<img src="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-08BatteriehalterVerbunden_small.jpg" alt="So kann es aussehen, alle Halter miteinander verbunden" />
</a> </span> </div> </div> </div>

Jetzt gilt es die LED-Reihen parallel zu verschalten und in den Gesamtaufbau zu integrieren.
Dazu schaffen wir zwei "Drahtkreise", einmal für den Pluspol und einmal für den Minuspol.
Hierfür verwenden wir einen beliebigen Draht, zum Beispiel den inneren Kern eines Kabels, indem ihr das Kabel auf der kompletten Länge abisoliert.
Von diesen Ringen verbauen wir zwei Stück, einmal etwas oberhalb der oberen LEDs und einmal etwas unterhalb der unteren LEDs.
Ihr könnt den Draht einfach außenrum legen, an einer Stelle etwas verzwirbeln und dann etwas Lötzinn darüber, damit es sich nicht wieder löst.
Damit sich die Kreise nicht verschieben könnt ihr mit dem Lötkolben an den "Kanten" etwas auf den Draht drücken, so dass sich dieser leicht in den gedruckten Einsatz "einschmilzt".
Ein klein wenig reicht schon, damit es sich nicht verschiebt.

Ihr erinnert euch an das Ende der fünf Batteriehalter, die wir verbunden haben.
Hier solltet ihr ja OBEN rausgekommen sein. Und hier verbindet ihr nun einfach den Halter oben (Pluspol) mit dem oberen Ring (ebenfalls Plus).


Als Zwischenschritt werden wir nun den Schalter anbringen.
Zuerst löten wir zwei Kabel an den Schalter, die je etwa 5-8cm lang sind. Ihr könnt entweder am mittleren und linken Pin anlöten oder am mittleren und rechten.
Wichtig ist nur, dass ihr auf der gleichen Seite anlötet, sprich beide vorne oder beide hinten.

<div class="box alt"> <div class="row uniform"> <div class="2u"> </div> <div class="8u"> <span class="image fit">
<a href="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-10Schalter.jpg">
<img src="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-10Schalter_small.jpg" alt="Der angelötete Schalter" />
</a> </span> </div> <div class="2u$"></div> </div> </div>

Dann werden die Kabel durch die zwei Löcher gefädelt. Den Schalter könnt ihr nun ebenfalls mit 2-Komponentenkleber vorsichtig ankleben, dabei aufpassen, dass der Schalter beweglich bleibt!
Schalter-Kabel 1 lötet ihr nun unten an den Minuspol des Batteriehalter an, an dem die Kabel des Schalters herauskommen.
Schalter-Kabel 2 wird mit dem unteren Minuspol-Ring verlötet.

<div class="box alt"> <div class="row uniform"> <div class="2u"> </div> <div class="8u"> <span class="image fit">
<a href="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-11SchalterAngeschlossen.jpg">
<img src="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-11SchalterAngeschlossen_small.jpg" alt="Der angeschlossene Schalter" />
</a> </span> </div> <div class="2u$"></div> </div> </div>

Wie angekündigt müssen die parallelen LED-Reihen nun noch mit dem Gesamtaufbau verbunden werden.
Hierbei kommt wieder der Silberleitlack zum Einsatz.
Oberhalb der oberen LEDs und unterhalb der unteren LEDs könnt ihr nun Silberleitlack auftragen und die drei Füße der LEDs miteinander verbinden. Dann zieht ihr das ganze nach oben bzw. unten und verbindet die Reihe mit dem entsprechenden Drahtring!
Lasst den Lack auch hier ausreichend trocknen und kontrolliert eure Arbeit mit Gegenlicht.

Und damit seid ihr schon am Ende.
Lasst alles gut trocknen und dann seid ihr ready to go!
Batterien einsetzen, Schalter umlegen und freuen :)


<div class="box alt"> <div class="row uniform"> <div class="2u"> </div> <div class="8u"> <span class="image fit">
<a href="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-12beleuchteterBaum.jpg">
<img src="/images/anleitung/led-tannenbaum/haefner-technology-led-tannenbaum-12beleuchteterBaum_small.jpg" alt="Der beleuchtete Baum" />
</a> </span> </div> <div class="2u$"></div> </div> </div>

## Materialien

* Lötkolben
* Pinzette
* Elektronikkabel
* 15x SMD Widerstand 100 Ohm, Bauform 1206, z.B. [50 SMD Widerstände](https://www.amazon.de/gp/product/B00JQ8UK0M/ref=as_li_ss_tl?ie=UTF8&psc=1&linkCode=ll1&tag=led-tannenbaum-21&linkId=56d13bb61a7bdf5617811d707dec7dee&language=de_DE)
* 10x SMD LED Bauform 5050, z.B. [20 SMD LED](https://www.amazon.de/gp/product/B005V6CBQY/ref=as_li_ss_tl?ie=UTF8&psc=1&linkCode=ll1&tag=led-tannenbaum-21&linkId=26e9354d9c4a9ec5b872ea91e77e7ff0&language=de_DE)
* 5x Batteriehalter AAA mit Lötanschluss, z.B. [5er Set AAA Batteriehalter](https://www.amazon.de/gp/product/B01GLOLUR0/ref=as_li_ss_tl?ie=UTF8&th=1&linkCode=ll1&tag=led-tannenbaum-21&linkId=85502acf8d1b07a91edd70b6a609c52a&language=de_DE)
* 1x [Einbauschalter](https://www.amazon.de/Schalter-Einbauschalter-mit-Pins-OFF/dp/B00DW8B6UU/ref=as_li_ss_tl?srs=3511419031&ie=UTF8&qid=1575126138&sr=8-2&linkCode=ll1&tag=led-tannenbaum-21&linkId=d54bc688a13e604447ab19be54caa463&language=de_DE)
* [Silberleitlack](https://www.amazon.de/Silber-Leitlack-Stift-8-g-Leitf%C3%A4higer-Silber-Lack/dp/B00LTWQWXW/ref=as_li_ss_tl?__mk_de_DE=%EF%BF%BDM%EF%BF%BD%25u017D%EF%BF%BD%EF%BF%BD&keywords=silberleitlack&qid=1575126615&sr=8-5&linkCode=ll1&tag=led-tannenbaum-21&linkId=781ea84923611e6747688b8c0ea7f3e3&language=de_DE)
* [2-Komponentenkleber](https://www.amazon.de/gp/product/B000KJO9FI/ref=as_li_ss_tl?ie=UTF8&psc=1&linkCode=ll1&tag=led-tannenbaum-21&linkId=c5021ec4268a8584c808f8dd2cdcd188&language=de_DE)
* [Sekundenkleber](https://www.amazon.de/gp/product/B005FN8F7W/ref=as_li_ss_tl?ie=UTF8&psc=1&linkCode=ll1&tag=led-tannenbaum-21&linkId=338fad1fd66b05898fbbb9addb3873ac&language=de_DE)
