---
title: Autohalterung-Fairphone2
categories: Anleitung
tags:
 - Auto
 - Autohalterung
 - Fairphone 2
 - Season4
 - Selfmade
date: 2019/11/01 13:37:42
cover_index: /images/anleitung/autohalterung-fairphone2/haefner-technology-autohalterung-fairphone2-cover-index.jpg
amazon: false
sitemap: true
---
Das Fairphone 2 wurde nur [115.000 mal verkauft](https://en.wikipedia.org/wiki/Fairphone_2), im Vergleich dazu wurde das [iPhone 2.2 Milliarden mal verkauft](https://en.wikipedia.org/wiki/IPhone). Entsprechend weniger Zubeh&ouml;r gibt es f&uuml;r das Fairphone 2 und eben auch keine Autohalterung. Aber mit Hilfe eines 3D-Druckers, eines Computers und ein bisschen Einarbeitungszeit in ein CAD-Programm kann man sich gut selbst behelfen. Zum Beispiel mit einer Autohalterung f&uuml;r das Fairphone 2:

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/autohalterung-fairphone2/haefner-technology-autohalterung-fairphone2-00Titelbild.jpg"><img src="/images/anleitung/autohalterung-fairphone2/haefner-technology-autohalterung-fairphone2-00Titelbild.jpg" alt="Fairphone 2 Autohalterung in einem Sharan" /></a></span></div>
	</div>
</div>

Gesamtkosten: ca. 10-20 €
Vorkenntnisse: Keine (sofern ihr keine eigene Halterung entwerfen/konstruieren wollt)
Zeitaufwand: ca. 1 h

## Druckteile

Die Autohalterung besteht aus einem Druckteil:

 * 1x [Fairphone 2 Autohalterung](/druckmodelle/anleitung/autohalterung-fairphone2/Fairphone2_Autohalterung.stl)

Wichtig hierbei ist die Filamentwahl. Unserer Erfahrung nach ist PLA nicht hitzebest&auml;ndig genug um in einem Auto das im Sommer in der Sonne steht dauerhauft seine Form zu behalten. Alternativen sind PETG-, ABS- oder ASA-Filament. Alle zunehmend hitzebest&auml;ndiger, aber schwerer zu drucken.

## Zusatzteile

Zus&auml;tzlich wird eine Halterung von [Brodit](https://www.brodit.com/brodit.html) ben&ouml;tigt auf die die Autohalterung montiert wird. Im Falle des Sharans aus dem Titelbild haben wir folgende Teile genutzt:

 * 1x [VW Sharan Center Mount](https://www.brodit.com/product.html?id=852835&pn=prod&brand=Volkswagen&model=Sharan&year=2006)
 * 1x [Tilt Swivel](https://www.brodit.com/product.html?id=215122)

Ausserdem wird ein Micro-Usb-Winkelstecker ben&ouml;tigt. Das Druckteil ist f&uuml;r folgenden Stecker ausgelegt:

 * 1x [Micro-Usb Winkelstecker](https://www.amazon.de/gp/product/B01F7Z0D9A/)

## Zusammenbauen

Das Zusammenbauen der eigentlichen Handyhalterung beinhaltet nur das Einsetzen des Winkelsteckers. Damit dieser nicht verrutscht ist das Modell sehr passgenau ausgelegt, weshalb relativ viel Kraft aufgewendet werden muss, um den Winkelstecker einzusetzen.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/autohalterung-fairphone2/haefner-technology-autohalterung-fairphone2-01Winkelstecker.jpg"><img src="/images/anleitung/autohalterung-fairphone2/haefner-technology-autohalterung-fairphone2-01Winkelstecker_small.jpg" alt="Autohalterung mit eingesetztem Winkelstecker." /></a></span></div>
		<div class="2u"></div>
	</div>
</div>

Anschliessend wird die Autohalterung nach Anleitung von Brodit ins Auto eingebaut und die Handyhalterung auf den sogenannten "Tilt Swivel" aufgeschraubt. Fertig.

## Alternative Nutzung

Die "Auto"-halterung muss nicht zwingend im Auto verwendet werden. Mit ein bisschen Kreativitaet kann man sie auch als Nachttisch-Handyhalter, zum Beispiel mit einer Holzkonstruktion, verwenden.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/autohalterung-fairphone2/haefner-technology-autohalterung-fairphone2-02Nachttisch.jpg"><img src="/images/anleitung/autohalterung-fairphone2/haefner-technology-autohalterung-fairphone2-02Nachttisch_small.jpg" alt="Autohalterung als Nachttisch-Handyhalter" /></a></span></div>
		<div class="2u"></div>
	</div>
</div>

## Materialien

 * 1x [Micro-Usb Winkelstecker](https://www.amazon.de/gp/product/B01F7Z0D9A/)

