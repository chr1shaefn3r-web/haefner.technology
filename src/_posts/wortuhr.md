---
title: Wortuhr
categories: Anleitung
tags:
 - Wortuhr
 - extraordinary
 - Season3
 - Selfmade
date: 2019/05/04 20:00:00
cover_index: /images/anleitung/wortuhr/haefner-technology-wortuhr-00cover-index.jpg
amazon: false
sitemap: true
---
Jeder kennt sie, die altmodische analoge Zeigeruhr. In fast jedem Büro hängt sie an der Wand. Hin und wieder hat man auch eine digitale Uhr.
Heute stellen wir euch ein Projekt vor, an dessen Ende ihr eine Uhr habt, die nahezu einmalig ist. Kaum einer hat oder kennt sie, jeder will sie haben, glaubt uns! :)

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-00Titelbild.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-00Titelbild.jpg" alt="So kann die Wortuhr zum Schluss aussehen" /></a></span></div>
	</div>
</div>

Gesamtkosten: ca. 80-120 €
Vorkenntnisse: machbar ohne jegliche Kenntnisse. Etwas Geschick, Löt- und Elektronikkenntnisse schaden nicht.
Zeitaufwand: ca. 8-12h

## Grundsätzlicher Aufbau

Die Wortuhr ist aus zwei Hauptteilen aufgebaut, die 3D gedruckt sind. Im und auf dem Einsatzstück wird die komplette Elektronik  montiert. Das Gehäuse ist für die Darstellung der Uhrzeit und beinhaltet den Stromanschluss und die Taster zur Einstellung der Optionen.

Der Elektronik-Aufbau basiert auf der Arbeit von [Christians Bastellladen](http://www.christians-bastel-laden.de/). Das Gehäuse  und der Einsatz sind unser Design.
Ganz unten in dieser Anleitung findet ihr auch noch einige weiterführende Links (z.B. Löten der Platine).

 * 1x [Einsatz](/druckmodelle/anleitung/wortuhr/Einsatz.stl)
 * 1x [Gehäuse](/druckmodelle/anleitung/wortuhr/Gehaeuse.stl)

Bestellt die Wortuhr-Druckteile direkt in euren Wunschfarben bei uns!
[info@haefner.technology](mailto:/info@haefner.technology)

## Der Aufbau

Starten solltet ihr mit dem Bestücken des Einsatzes. Legt ihn wie im zweiten Bild richtig herum vor euch, sodass die Rückseite mit den Löchern für LEDs zu euch zeigt.

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-01Einsatz1.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-01Einsatz1_small.jpg" alt="Der Einsatz von vorne" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-02Einsatz2.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-02Einsatz2_small.jpg" alt="Der Einsatz von hinten" /></a></span></div>
	</div>
</div>

Dann schneiden wir Gewinde in die vier Aufnahmepins des Einsatzes, damit wir die Platine später mit Maschinenschrauben festschrauben können.
Falls ihr keinen M3 Gewindebohrer habt könnt ihr auch Spaxschrauben nehmen, oder es mit Maschinenschrauben versuchen. Ihr solltet die Löcher dann nur noch etwas aufbohren (~Ø2,5-2,8mm), damit ihr die Pins nicht sprengt, wenn ihr die Schrauben ohne Gewinde eindreht.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-03GewindeSchneiden.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-03GewindeSchneiden_small.jpg" alt="Die fertig gelötete LED-Matrix" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>


Wir beginnen nun mit dem "Einlöten" der Ø5 LEDs. Diese haben standardmäßig eine abgeflachte Seite, die ihr auch im Einsatz wiederfindet, womit wir eine Verdrehsicherung haben. Sofern ihr den Einsatz richtig ausgerichtet habt kommen die LEDs mit der abgeflachten Seite nach rechts unten. Noch einfacher erkennt ihr die Seite übrigens an dem kurzen Bein der LED! (Kathode = Minus-Pol)
ACHTUNG: Bestückt zuerst nur die ganz linke Spalte! Und testet unbedingt vorher jede LED (z.B. mit Knopfbatterien)! Ihr wollt nicht später einzelne wieder auslöten, weil diese schon von Anfang an kaputt waren! Verwendet dafür auf jeden Fall einen Vorwiderstand, damit die LEDs nicht kaputt gehen.
Falls die ein oder andere LED nicht mit etwas Kraft in die richtige Position rutscht so könnt ihr einfach mit einem Ø5 Bohrer die Löcher nachbohren.

Nehmt nun ein Stück Silberdraht und legt es in die dafür vorgesehene Nut der linken Spalte. Am besten führt ihr den Silberdraht jeweils außen am Bein der LED vorbei. Nun könnt ihr alle LED-Beinchen (kurze Beinchen, Kathoden) der Spalte an den Silberdraht anlöten. Achtet darauf, dass der Silberdraht wirklich ganz unten in der Nut liegt. Nicht dass es nachher einen Kurzschluss gibt, wenn die Anoden eingelötet werden. Dies sollte in etwa wie auf dem folgenden Bild aussehen. Bitte beachtet: Wir haben beim Aufbau fälschlicherweise mit der ersten Zeile angefangen. Die Nut für die Zeilen (Anoden) ist weniger tief. Macht man diese zuerst, so muss man alle anderen Silberdrahtverbindungen mühsam darunter durchfädeln.


<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-04LEDs1.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-04LEDs1_small.jpg" alt="Die ersten LEDs sind verlötet" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-05LEDs2.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-05LEDs2_small.jpg" alt="Die ersten LEDs sind verlötet" /></a></span></div>
	</div>
</div>


Im nächsten Schritt könnt ihr nun Spalte für Spalte die LEDs mit Silberdraht verbinden. Nehmt euch dafür Zeit, eine kaputte oder unsaubere Lötstelle finden ist nicht immer einfach!

Habt ihr dies erledigt kommen die Zeilen dran, beginnt hier mit der untersten und arbeitet euch nach oben hin vor. Habt ihr auch das geschafft, könnt ihr die überstehenden Beinchen abknipsen und das Ganze sollte dann so aussehen:

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-06Matrix.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-06Matrix_small.jpg" alt="Die fertig gelötete LED-Matrix" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Überprüft auch nun nochmal, ob alle LEDs funktionieren!
Jetzt benötigen wir die Platine. Solltet ihr euch für die fertig gelötete Variante entschieden haben, dann herzlichen Glückwunsch. Falls ihr die Platine selber lötet, viel Glück! (Es ist aber tatsächlich nicht so schwer)

Ist die Platine fertig bestückt, so werden im nächsten Schritt die Verbindungskabel zur LED-Matrix angelötet. In unserem Fall haben wir zusätzlich abgewinkelte Steckerleisten verwendet, man kann die Kabel aber auch direkt an der Platine anlöten. Wir benötigen dafür 15 (11+4) blaue (Kathoden) und 14 (10+4) rote Kabel (Anoden), ca. 20cm lang. Entsprechend dem Bild lötet ihr oben links von links nach rechts die 15 blauen Kabel an und oben rechts von rechts nach links 10 rote an. Die restlichen 4 roten lötet ihr parallel zu den ersten 4 Kontakten an! Das heißt an den ersten 4 Kontakten sind je zwei Kabel angelötet.


<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-07AnodenKathoden.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-07AnodenKathoden_small.jpg" alt="Anoden und Kathoden Kabel angelötet" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>


Nun müssen wir die Kabel an die LED-Matrix anlöten. Dies ist recht simpel, da man quasi überall in einer Zeile oder Spalte das Kabel festlöten kann. Man muss jedoch auf die richtige Verkabelung achten, sonst zeigt die Uhr später nur Schwachsinn an. Das linke erste blaue Kabel muss an die rechte erste Spalte. Das zweite blaue Kabel von links an die zweite Spalte von rechts usw. Ihr werdet merken, dass ihr dann noch 4 blaue Kabel übrig habt! Diese sind folgerichtig für die Eck-LEDs (jeweils ans kurze Bein anlöten!).
Und zwar fortlaufend die oben rechte an Kabel Nr. 12 von links, unten rechts an Nr. 13, unten links an Nr. 14 und oben links and Nr. 15!

Nun geht es mit den roten Kabeln weiter. Das erste rote von rechts muss an die unterste Zeile, das zweite von rechts an die zweite Zeile von unten usw. Wie ihr vermutlich schon richtig vermutet kommen die langen Beinchen der Eck-LEDs an die vier ersten doppeltbelegten roten Pins. Oben rechts an Nr. 1 von rechts, unten rechts an Nr. 2, unten links an Nr. 3 und oben links an Nr. 4.
[HIER](http://christians-bastel-laden.de/docs/NBV2_Anschluss_Matrix.pdf) findet ihr eine schöne PDF von Christians Bastelladen, wo ihr die Verdrahtung nochmal anschauen könnt. ACHTUNG: Dies ist die Ansicht von VORNE, wenn ihr lötet schaut ihr von HINTEN auf die Uhr.
Mit Zwischenschritten sollte es nun so aussehen:

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-08MatrixVerbinden.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-08MatrixVerbinden_small.jpg" alt="Die ersten Kabel sind angelötet" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

<div class="box alt">
	<div class="row uniform">
		<div class="4u"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-09KathodenDetail.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-09KathodenDetail_small.jpg" alt="Kathoden im Detail" /></a></span></div>
		<div class="4u"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-10AnodenKathodenDetail.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-10AnodenKathodenDetail_small.jpg" alt="Anoden und Kathoden im Detail" /></a></span></div>
		<div class="4u$"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-11Eck-LED-Detail.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-11Eck-LED-Detail_small.jpg" alt="Eck-LED im Detail" /></a></span></div>
	</div>
</div>

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-12MatrixVerbunden.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-12MatrixVerbunden_small.jpg" alt="Die Platine ist mit der LED-Matrix verbunden" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Nun bereiten wir den Anschluss der Taster vor:

Dazu löten wir pro Taster ein rotes und ein blaues Kabel an die Taster auf der Platine. Auf der Platine gibt es 5 verschiedene Taster, für die Wortuhr benötigen wir jedoch nur den Modus-, den Stunden- und den Minutentaster. Im Bild seht ihr, wo ihr anlöten könnt (wenn die Platine mit den Bauteilen nach oben vor euch liegt, rechts unten).

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-13TasterVerbinden.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-13TasterVerbinden_small.jpg" alt="Kabel für Taster so anlöten" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Als nächstes bereiten wir den externen Stromanschluss vor. Dazu brauchen wir wieder ein rotes und ein blaues Kabel. Verlötet die beiden wie im Bild gezeigt mit dem Winkelstecker auf der einen und der Buchse auf der anderen Seite.
ACHTUNG: Schraubt die Buchse noch NICHT ein!
Sollte ihr eine andere Buchse oder einen anderen Stecker haben: der Minuspol muss außen liegen, innen der Pluspol (center positive)!

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-14Stromanschluss1.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-14Stromanschluss1_small.jpg" alt="So an den Stecker löten" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-15Stromanschluss2.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-15Stromanschluss2_small.jpg" alt="So an die Buchse löten" /></a></span></div>
	</div>
</div>


Bevor wir zur Endmontage kommen solltet ihr noch die Taster ins Gehäuse einkleben. Der Einfachheit halber benutzen wir hier Heißkleber.


<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-16TasterEinkleben.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-16TasterEinkleben_small.jpg" alt="Nicht schön, aber dafür selten!" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>


## Endmontage

Nun ist alles bereit für die Endmontage.
Schraubt die Platine auf den Einsatz auf, verstaut dabei die Kabel vernünftig unter der Platine. Die Muttern, die wir als Distanzen verwendet haben, benötigt ihr nicht, das Modell wurde entsprechend angepasst.
ACHTUNG: Solltet ihr die Status-LEDs angelötet haben, legt unbedingt ein Stück Karton zwischen Platine und Einsatz, sonst leuchten die LEDs nach vorne durch!
Setzt nun den Einsatz vorsichtig ins Gehäuse ein. Dazu ist es wichtig, dass ihr den Einsatz sehr parallel ansetzt und ins Gehäuse drückt, sonst verkantet er. Wenn ihr das geschafft habt könnt ihr mit vier Spaxschrauben den Einsatz im Gehäuse befestigen.
Noch die Schalter anlöten (an jedem Schalter überkreuz) und die Buchse montieren, fertig!


<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-17EndmontageTaster.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-17EndmontageTaster_small.jpg" alt="Endmontage der Taster" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/wortuhr/haefner-technology-wortuhr-18EndmontageStromversorgung.jpg"><img src="/images/anleitung/wortuhr/haefner-technology-wortuhr-18EndmontageStromversorgung_small.jpg" alt="Endmontage der Stromversorgung" /></a></span></div>
	</div>
</div>


Nun ist es an der Zeit eure Uhr mit Strom zu versorgen!
Funktioniert alles?
Allerherzlichsten Glückwunsch, dass war eine klasse Leistung!

Es klappt nicht? Keine Sorge, es hat noch nie auf Anhieb funktioniert :)
Tipp: Überprüft die Lötstellen eurer Platine. Habt ihr evtl. welche vergessen?

[Hier](http://www.christians-bastel-laden.de/DOWNLOADS/Anleitung_Firmware_3.3.pdf) findet ihr übrigens die Anleitung, wie ihr die Wortuhr mit den drei Tastern bedienen könnt!


## Materialien

 * [Platine](https://www.elmotex.de/bausaetze--module/bausaetze--module/bausatz-nb_v03.php)
 * [114x LEDs Ø5, rund, hier Orange, gibt es gerade nicht in kaltweiss](https://www.amazon.de/ProGoods-energieeffizienter-herk%C3%B6mmliche-Innenraumbelechtung-Antistatikverpackung/dp/B0778TVQPF/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=progoods+led+kaltweiss&qid=1556965549&s=lighting&sr=1-1-fkmr0&th=1&linkCode=ll1&tag=wortuhr-21&linkId=be9411b5c4dadbabb07628d608bf958c&language=de_DE)
 * [alternative sehr günstige LEDs in kaltweiss](https://www.amazon.de/ogeled-100x-leds-transparend-kaltwei%C3%9F/dp/B01GGO085U/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=led+5mm+kaltweiss&qid=1556965926&s=lighting&sr=1-5&linkCode=ll1&tag=wortuhr-21&linkId=81c87e6d64a78edcde98350dfe4953dc&language=de_DE)
 * [Netzteil](https://www.amazon.de/gp/product/B002E4YQHS/ref=as_li_ss_tl?ie=UTF8&psc=1&linkCode=ll1&tag=wortuhr-21&linkId=cacda8231a2eb5a5617aa74abc86cde7&language=de_DE)
 * [DC-Einbaubuchse](https://www.amazon.de/DeLock-65690-65690-Delock-Einbaubuchse-Schwarz/dp/B01I0IHDZ8/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=dc+einbaubuchse+5,5+2,5&qid=1556986489&s=ce-de&sr=1-1&linkCode=ll1&tag=wortuhr-21&linkId=9c4dba633e7e52f4e4b8f29cb77136ee&language=de_DE)
 * [DC-Winkelstecker](https://www.amazon.de/Stromstecker-abgewinkelt-Innendurchmesser-Au%C3%9Fendurchmesser-Schaftl%C3%A4nge/dp/B00FBCIYHS/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=dc+winkelstecker+5,5+2,5&qid=1556986993&s=ce-de&sr=1-2&linkCode=ll1&tag=wortuhr-21&linkId=34c68d8b36572500b41d0525c5c16267&language=de_DE)
 * [Silberdraht](https://www.amazon.de/Silber-Eisendraht-Meter-0-5mm-Spule/dp/B01C90Q2R8/ref=as_li_ss_tl?__mk_de_DE=%EF%BF%BDM%EF%BF%BD%25u017D%EF%BF%BD%EF%BF%BD&keywords=silberdraht+0.5mm&qid=1556995012&s=gateway&sr=8-12&linkCode=ll1&tag=wortuhr-21&linkId=045e6e0c237d94cfbdd2da0f819c2daf&language=de_DE)
 * [Spaxschrauben 4x20mm zur Befestigung des Einsatzes im Gehäuse](https://www.amazon.de/St%C3%BCck-Edelstahl-Schrauben-Vollgewinde-Spanplattenschrauben-Edelstahlschrauben/dp/B01DC8V4HO/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=spax+4x20&pd_rd_i=B01DC8V4HO&pd_rd_r=f55d70b8-1515-4f19-a1cb-a9983944949c&pd_rd_w=WauYg&pd_rd_wg=J2I4C&pf_rd_p=c09363a8-6487-426f-8bb2-3cf3beff8c00&pf_rd_r=YV551DWC14BXHNJ2698E&qid=1557067888&s=gateway&linkCode=ll1&tag=wortuhr-21&linkId=66e2772d490e3227f14a8673c83a763c&language=de_DE) 
 * [Isopropanol zum Reinigen der Platine](https://www.amazon.de/gp/product/B001FSXLMU/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=wortuhr-21&linkId=b522a729d7fb78da3ec6689e3e267636&language=de_DE)


## Benötigtes Werkzeug

 * Lötstation + Lötzinn
 * Seitenschneider
 * Digitalmultimeter zum Durchmessen und zur Fehlersuche

## Weitere Materialien zum Nachlesen oder als unterstützende Hilfe

 * [Anleitung zum Löten der Platine von Elmotex](http://christians-bastel-laden.de/NBV03_MANUAL/index.html)
 * [Die Firmware der Wortuhr](http://christians-bastel-laden.de/DOWNLOADS/Qlockthree_v3.4.9.zip)
 * [Die Arduino-Homepage, wo ihr die Software findet, um die Firmware zu öffnen, zu ändern und/oder aufzuspielen](https://www.arduino.cc/)
 * [HIER noch eine Anleitung zum Upload der Firmware](http://www.christians-bastel-laden.de/firmwareupload.html)
 * [FTDI-Adapter zum Aufspielen der Firmware](https://www.amazon.de/Adapter-FT232RL-Arduino-Christians-Technikshop/dp/B0178HVEH0/ref=as_li_ss_tl?__mk_de_DE=%EF%BF%BDM%EF%BF%BD%25u017D%EF%BF%BD%EF%BF%BD&keywords=ftdi+basic+breakout+5v&qid=1556988229&s=gateway&sr=8-49-spons&psc=1&linkCode=ll1&tag=wortuhr-21&linkId=4bfe368acc995ca80e6afeadffd84fe6&language=de_DE)

