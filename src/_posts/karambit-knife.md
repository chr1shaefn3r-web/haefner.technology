---
title: Karambit Knife
categories: Anleitung
tags:
 - CSGO
 - GlobalOffensive
 - KarambitKnife
 - Knife
 - Make
 - Season1
 - Selfmade
 - Skins
date: 2018/03/01 13:37:00
cover_index: /images/anleitung/karambit-knife/haefner-technology-karambit-knife-cover-index.jpg
amazon: true
sitemap: true
---
An alle CS:GO-Fans, die sich ein Ingame Messer nicht leisten wollen, sondern lieber den Nachmittag damit verbringen den coolsten Brief&ouml;ffner &uuml;berhaupt zu basteln. So kann das fertige Teil aussehen:
<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-cover.jpg"><img src="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-cover.jpg" alt="Karambit Knife" /></a></span></div>
	</div>
</div>

## Gr&ouml;&szlig;en Tabelle

Der erste Schritt ist, die richtige Gr&ouml;&szlig;e herauszufinden. Um das zu vereinfachen haben wir mehrere Ausdrucke angefertigt und mit verschiedenen Handgr&ouml;&szlig;en getestet.
Die Spannweite wird gemessen von Daumen bis kleiner Finger.

 Spannweite | Skalierungsfaktor |
------------|-------------------|
 22&nbsp;cm |  98&nbsp;%        |
 24&nbsp;cm | 100&nbsp;%        |
 26&nbsp;cm | 103&nbsp;%        |

## Druckteile

 * 2x [Karambit-Messer H&auml;lfte](https://www.thingiverse.com/thing:324433)
   (*Hinweis*: Skalierungsfaktor nicht vergessen)
 * 1x [Podest/Stand](https://www.thingiverse.com/thing:614101)
   (*Hinweis*: Skalierungsfaktor nicht vergessen)

## Verkleben

Nachdem Ihr die Druckteile erhalten oder selbst erstellt habt, m&uuml;ssen die zwei H&auml;lften verklebt werden. Besonders Drucke aus PLA lassen sich sehr leicht mit fast allen Klebern verkleben (Sekundenkleber, Plastikkleber, etc.).
Ich habe [Sekundenkleber](https://www.amazon.de/gp/product/B004V4B60Y/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B004V4B60Y&linkId=a38efc9070d28366f8056082d7de045d) verwendet, auf beiden H&auml;lften d&uuml;nn aufgetragen, insgesamt ca. 0,5-1&nbsp;g Sekundenkleber. Achtet beim Verkleben auf Pr&auml;zision und dass Ihr beim Andr&uuml;cken nicht verrutscht. Um auf der gesamten Fl&auml;che konstant Druck auszu&uuml;ben, sind z.B. [Klemmen](https://www.amazon.de/gp/product/B0167JC1CM/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B0167JC1CM&linkId=a4a26aff96b93da837da0bb98184561e) ein guter Helfer.

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-verkleben-1.jpg"><img src="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-verkleben-1_small.jpg" alt="Karambit Knife Druckteile" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-verkleben-2.jpg"><img src="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-verkleben-2_small.jpg" alt="Karambit Knife mit Klammern" /></a></span></div>
	</div>
</div>

F&uuml;r Modelle bei denen wir Fl&auml;chen verkleben, die nicht auf der Glasplatte gedruckt wurden, nutzen wir gerne den PU-Kleber [Gorillaglue](https://www.amazon.de/gp/product/B01MQ4JADL/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B01MQ4JADL&linkId=6b4c221f0fe77d2423e46b1a3b2c30f1), da er die Eigenschaft hat, sich leicht auszudehnen, wodurch Zwischenr&auml;ume und Unebenheiten ausgef&uuml;llt werden.

## Schleifen

Auch wenn das Verkleben perfekt funktioniert hat, lohnt es sich, das Modell nachzuschleifen. Bew&auml;hrt haben sich 180er Schleifpapier f&uuml;r gr&ouml;bere Ecken und 320er Schleifpapier zum feinen Nachschleifen. Wer es noch feiner mag kann gerne auch mit >400er Schleifpapier nochmal &uuml;ber die wichtigen Stellen gehen.
Bei diesem Modell besonders wichtig ist der hintere Ring. Hier sollte man sich besonders viel M&uuml;he geben alle Unebenheiten zu entfernen, um sp&auml;ter m&ouml;glichst viel Spa&szlig; mit dem Karambit-Messer zu haben. Ich rolle dazu das [Schleifpapier](https://www.amazon.de/gp/product/B075R43P33/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B075R43P33&linkId=27cf5b5b3d330783f98a9a5723f8cfae) auf, stecke es durch den Ring und drehe und drehe und drehe. Hierbei ist viel Geduld gefragt.

## Farbkombination

Eurer Fantasie sind keine Grenzen gesetzt. Sucht euch eure Lieblingsfarbkombination aus. Sei es Gelb-Schwarz als Bumblebee-, oder Rot-Blau als Spidermann-Messer. Um sich einen Lackierungsvorgang zu sparen, hat es sich bew&auml;hrt das Messer direkt in einer Farbe eurer Wahl zu drucken.
Ein weiterer wichtiger Aspekt ist die Deckkraft. Am einfachsten ist es sich von hell nach dunkel vorzuarbeiten. Das hei&szlig;t die dunklen Farben zuletzt aufzutragen, da sie die bestehenden hellen Farben leicht &uuml;berdecken. Das Messer in Schwarz zu drucken und dann nachtr&auml;glich ein knalliges Gelb zu erzielen ist mit viel Aufwand verbunden (mehrere Farbschichten, etc.)

### Abkleben

Vor jedem Lackieren muss das Model sorgf&auml;ltig abgeklebt werden. Hierzu verwenden wir [Kreppband](https://www.amazon.de/gp/product/B0007OEBEU/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B0007OEBEU&linkId=de3dcb0424f94cc2d243ae55a6abb837). Um die Ecken und Rundungen auszusparen kommt ein Teppichmesser zum Einsatz. Brecht die Klinge ruhig nochmal ab um eine wirklich scharfe Messerspitze zu haben. Achtet unbedingt darauf, dass ihr das Kreppband gut andr&uuml;ckt, damit die Farbe nicht zwischen Kreppband und Modell l&auml;uft.

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-abkleben-1.jpg"><img src="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-abkleben-1_small.jpg" alt="Karambit Knife Abgeklebt" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-abkleben-2.jpg"><img src="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-abkleben-2_small.jpg" alt="Karambit Knife Schneide Abgeklebt" /></a></span></div>
	</div>
</div>

### Lackieren

Zum Lackieren nutzen wir handels&uuml;blichen Spr&uuml;hlack, z.B. [Blau Metallic](https://www.amazon.de/gp/product/B007TWNPIG/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B007TWNPIG&linkId=78cc45f434fb6cfc2cfa2125d946fc40).
Das Allerwichtigste ist, mehrere Farbschichten aufzutragen. Dadurch k&ouml;nnt ihr euch bei jeder einzelnen Farbschicht mit der Menge der aufgetragenen Farbe zur&uuml;ckhalten und lauft viel weniger Gefahr, Farbnasen zu produzieren. Zus&auml;tzlich verringert es deutlich das Risiko, dass sich in einem Eck Farbe sammelt und wom&ouml;glich doch zwischen Kreppband und Model l&auml;uft. Au&szlig;erdem k&ouml;nnt ihr so sicherstellen, dass ihr auch bei ung&uuml;nstigen Farbkombination (hell auf dunkel) ein optimales Ergebnis erzielt.
Hier bei diesem Beispiel habe ich sechs Farbschichten aufgetragen, bis ich alle Ecken erwischt habe und das Metallic Blau sauber aussah. Und auch hier war der Schl&uuml;ssel zum Erfolg, Geduld zu bewahren und nicht gleich bei der ersten Schicht "viel hilft viel" anzuwenden und das Modell in Farbe zu ertr&auml;nken.

<div class="box alt">
    <div class="row uniform">
        <div class="6u"><span class="image fit"><a href="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-lackieren-1.jpg"><img src="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-lackieren-1_small.jpg" alt="Karambit Knife lackiert und abgeklebt" /></a></span></div>
        <div class="6u$"><span class="image fit"><a href="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-lackieren-3.jpg"><img src="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-lackieren-3_small.jpg" alt="Karambit Knife lackiert" /></a></span></div>
    </div>
</div>

### Bemalen

Um Details auszumalen hat sich Revell Farbe bew&auml;hrt. Wenn ihr vorsichtig und mit einem kleinen Pinsel arbeitet, k&ouml;nnt ihr euch das Abkleben sparen. Wer auf Nummer sicher gehen will benutzt auch hier wieder das Malerkrepp.
Gerade bei diesem Messer machen sich metallische Farben sehr gut. [Silber Metallic](https://www.amazon.de/gp/product/B0001B19MQ/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B0001B19MQ&linkId=9f3e4ddd8106b1dc5d0444de7a08b0a2) zum Beispiel kann den Ring erstaunlich echt aussehen lassen.

<div class="box alt">
    <div class="row uniform">
        <div class="12u$"><span class="image fit"><a href="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-bemalen-1.jpg"><img src="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-bemalen-1.jpg" alt="Karambit Knife bemalt" /></a></span></div>
    </div>
</div>

## Spitze stutzen

Das Modell kommt mit einer sehr filigranen Spitze. Um das Messer als mehr als nur einen Staubf&auml;nger zu verwenden ist sie meiner Erfahrung nach zu filigran. Fr&uuml;her oder sp&auml;ter bleibt man daran h&auml;ngen und sie bricht unkontrolliert ab. Mit z.B. einem Seitenscheider kann man sie kontrolliert abzwicken und der Spitze mehr Stabilit&auml;t verleihen.

### Klarlack

Um die Best&auml;ndigkeit der Farben zu erh&ouml;hen lohnt es sich, noch ein bis zwei Schichten Klarlack ([Gl&auml;nzend](https://www.amazon.de/gp/product/B007TVVPDE/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B007TVVPDE&linkId=1e3e23ba065f79ebe1c362f217960eed) oder [Matt](https://www.amazon.de/gp/product/B007TVVQM4/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B007TVVQM4&linkId=3546e7c3ca6ccdf18e95d44c03872bc8)) aufzutragen. Das verhindert, dass das Messer bereits nach den ersten Benutzungen abgegriffen aussieht.
Klarlack gleichm&auml;&szlig;ig aufzutragen verlangt nach etwas &Uuml;bung und Erfahrung, da man im Gegensatz zu Farbe nicht sieht wo man schon war. Auch hier gilt: Lieber d&uuml;nnere Schichten und daf&uuml;r noch eine mehr auftragen.

## Podest

Um das Karambit-Messer richtig in Szene zu setzen, w&auml;rten wir das Podest mit Malfarbe auf.
In diesem Fall habe ich [Wei&szlig;](https://www.amazon.de/gp/product/B0018DL62O/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B0018DL62O&linkId=24a6071390847defb50af951fea7858a) genommen, um das "CS GO"-Logo hervorstechen zu lassen. Man h&auml;tte aber auch Silber, Gold oder die passend Farbe zum Messer nehmen k&ouml;nnen.
Wer m&ouml;chte, kann auch seine Initialen auf der R&uuml;ckseite des Podests verewigen.

<div class="box alt">
    <div class="row uniform">
        <div class="6u"><span class="image fit"><a href="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-podest-1.jpg"><img src="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-podest-1_small.jpg" alt="Karambit Knife Podest unbemalt" /></a></span></div>
        <div class="6u$"><span class="image fit"><a href="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-podest-2.jpg"><img src="/images/anleitung/karambit-knife/haefner-technology-karambit-knife-podest-2_small.jpg" alt="Karambit Knife Podest bemalt" /></a></span></div>
    </div>
</div>

## Materialien pro Messer

 * Farblack, z.B. [Blau Metallic](https://www.amazon.de/gp/product/B007TWNPIG/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B007TWNPIG&linkId=78cc45f434fb6cfc2cfa2125d946fc40)
 * Klarlack, z.B. [Gl&auml;nzend](https://www.amazon.de/gp/product/B007TVVPDE/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B007TVVPDE&linkId=1e3e23ba065f79ebe1c362f217960eed) oder [Matt](https://www.amazon.de/gp/product/B007TVVQM4/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B007TVVQM4&linkId=3546e7c3ca6ccdf18e95d44c03872bc8)
 * Bemalfarben, z.B. [Silber Metallic](https://www.amazon.de/gp/product/B0001B19MQ/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B0001B19MQ&linkId=9f3e4ddd8106b1dc5d0444de7a08b0a2) und [Wei&szlig;](https://www.amazon.de/gp/product/B0018DL62O/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B0018DL62O&linkId=24a6071390847defb50af951fea7858a)
 * [Sekundenkleber](https://www.amazon.de/gp/product/B004V4B60Y/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B004V4B60Y&linkId=a38efc9070d28366f8056082d7de045d)
 * [Pinsel](https://www.amazon.de/gp/product/B0009I77FE/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B0009I77FE&linkId=0b62c8c5f50f9f8cecae785491ededf4)
 * [Kreppband](https://www.amazon.de/gp/product/B0007OEBEU/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B0007OEBEU&linkId=de3dcb0424f94cc2d243ae55a6abb837)
 * Schleifpapier, z.B. als [Set](https://www.amazon.de/gp/product/B075R43P33/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B075R43P33&linkId=27cf5b5b3d330783f98a9a5723f8cfae)

## Benötigtes Werkzeug

 * [Klemmen](https://www.amazon.de/gp/product/B0167JC1CM/ref=as_li_tl?ie=UTF8&tag=karambit-knife-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B0167JC1CM&linkId=a4a26aff96b93da837da0bb98184561e)

