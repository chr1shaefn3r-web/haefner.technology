---
title: Stempel
categories: Anleitung
tags:
 - Stempel
 - Logo
 - Season4
 - Selfmade
date: 2019/10/01 20:00:00
cover_index: /images/anleitung/stempel/haefner-technology-stempel-00cover-index.jpg
amazon: false
sitemap: true
---
Ein Stempel mit dem eigenen Logo - ein Muss für jede Firma und auch für viele Bastler privat.
Diese Anleitung soll euch dabei helfen, einen tauglichen Stempel selbst zu entwerfen.

<div class="box alt">
	<div class="row uniform">
		<div class="12u"><span class="image fit"><a href="/images/anleitung/stempel/haefner-technology-stempel-00Titelbild.jpg"><img src="/images/anleitung/stempel/haefner-technology-stempel-00Titelbild.jpg" alt="Das Häfner Technology Logo als Beispiel" /></a></span></div>
	</div>
</div>

Gesamtkosten: ca. 10 €
Vorkenntnisse: Grundlagen 3D-Modellierung
Zeitaufwand: ca. 30 min

## Material

Prinzipiell haftet Stempelfarbe an üblichem Filament. Aber da man den Stempel kaum exakt gerade aufsetzen kann, ist etwas Flexibilität von Vorteil. Eine Möglichkeit ist, das Logo als Negativ in Silikon oder Acryl zu drücken. Allerdings ist es relativ schwer, die Form gleichmäßig zu treffen und vor allem das Acryl verzieht sich beim Trocknen sehr stark (siehe Bild).

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/stempel/haefner-technology-stempel-01acrylverzogen.jpg"><img src="/images/anleitung/stempel/haefner-technology-stempel-01acrylverzogen_small.jpg" alt="Unsauberer Abdruck in Acryl" /></a></span></div>
	</div>
</div>

Die beste Lösung, die wir gefunden haben, ist direkt flexibles Filament zu drucken (Thermoplastisches Elastomer - TPE). In unserem Fall haben wir das "Innoflex 45" von Innofill verwendet. Leider sind TPE-Filamente etwas teurer und lassen sich nicht ganz so einfach drucken - vor allem nicht in die Höhe. Deshalb bietet es sich an, die grobe Form des Stempels aus gewöhnlichem PLA oder ABS zu drucken und nur das Logo aus TPE (siehe Bild). Um eine wirklich glatte Oberfläche zu bekommen, sollte man die Oberfläche anschließend noch abschleifen (weiter unten mehr dazu).

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/stempel/haefner-technology-stempel-02zweimaterialien.jpg"><img src="/images/anleitung/stempel/haefner-technology-stempel-02zweimaterialien_small.jpg" alt="Der Stempel besteht aus zwei Materialien" /></a></span></div>
	</div>
</div>

## Abmessungen

Bei der Konstruktion gibt es ein paar wenige Dinge zu beachten:
 - Die kleinsten Details sollten nicht kleiner als ca. 2x Düsendurchmesser sein
 - Um das Logo sollte möglichste wenig Rand sein, um Artefakte zu vermeiden (siehe Bild)
 - Das Profil sollte ca. 1 mm hoch sein. Bei weniger Höhe steigt die Gefahr für ungewollte Artefakte (siehe Bild). Bei mehr Höhe wird es unangenehm zu Schleifen und ist Verschwendung von Material

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/stempel/haefner-technology-stempel-03artefakte.jpg"><img src="/images/anleitung/stempel/haefner-technology-stempel-03artefakte_small.jpg" alt="Ungewollter kreisförmiger Rand und weitere Artefakte" /></a></span></div>
	</div>
</div>

## Vorgehen und weitere Tipps

Achtet beim Schleifen darauf, das Logo entweder gleichmäßig eben zu bewegen, damit es plan wird. Oder aber "von vorne nach hinten" zu bewegen, damit es konvex wird und man beim Stempeln vorne aufsetzen und nach hinten rollen kann.

Am besten schleift man auch hier in mehreren Stufen. Zum Beispiel zuerst mit 180er Körnung, um die groben Unebenheiten zu entfernen und danach mit 320er oder 400er Körnung, um die Kratzer auszupolieren. Tipp: Legt oder klebt das Schleifpapier auf eine ebene Fläche (z.B. eine Glasplatte) und bewegt den Stempel darüber.

## Material und Werkzeug

 * Schleifpapier, z.B. als [Set](https://www.amazon.de/gp/product/B075R43P33/ref=as_li_tl?ie=UTF8&tag=stempel05-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B075R43P33&linkId=3dfaece571206c67fc979c8c9f479fd1)
 * Großes [Stempelkissen](https://www.amazon.de/gp/product/B001QLM0FA/ref=as_li_tl?ie=UTF8&camp=1638&creative=6742&creativeASIN=B001QLM0FA&linkCode=as2&tag=stempel05-21&linkId=9110daab7dddd0c3100adeb9f8f418cc)
