---
title: Lithophan LED Bilderrahmen
categories: Anleitung
tags:
 - Lithophan
 - Bilderrahmen
 - Fotografie
 - Foto
 - Memories
 - Season5
 - Selfmade
date: 2020/05/01 13:37:42
cover_index: /images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-cover-index.jpg
amazon: true
sitemap: true
---
Dank des 3D-Drucks lassen sich besondere Bilder erstellen, sogenannte Lithophane.
Dabei "[...] handelt es sich um eine Reliefdarstellung in transluzentem Material [...], welche ihre Wirkung erst im Gegenlicht entfaltet." (Quelle: [Wikipedia.org](https://de.wikipedia.org/wiki/Lithophanie))

Für die Erstellung der Lithophane gibt es schon zahlreiche Tools, sowohl online als auch als Software. So kann z.B. das frei erhältliche Slice-Programm "Cura", was wir übrigens auch für unseren Drucker verwenden, ebenfalls dafür verwendet werden.

Was bietet sich also neben dem Druck des Lithophans noch an?
Natürlich: Ein eigens entwickelter Bilderrahmen, in dem das Lithophan und auch ein entsprechendes Gegenlicht unterkommt!

Im Folgenden findet ihr dazu die Anleitung:

<div class="box alt"> <div class="row uniform"> <div class="12u"> <span class="image fit"> <a href="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-00TitelbildHell.jpg"> <img src="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-00TitelbildHell_small.jpg" alt="Der fertige Bilderrahmen" /> </a> </span> </div> </div> </div>

Gesamtkosten: ca. 25-30 €
Vorkenntnisse: Machbar ohne jegliche Kenntnisse. 
Zeitaufwand: ca. 30-40min

## Druckteile

Der beleuchtet Lithophan Bilderrahmen besteht aus drei fertigen Druckteilen, dazu kommt noch das individuelle Lithophan.

* 1x [Rahmen](/druckmodelle/anleitung/lithophan-bilderrahmen/Rahmen.stl)
* 1x [Deckel](/druckmodelle/anleitung/lithophan-bilderrahmen/Deckel.stl)
* 1x [Standfuss](/druckmodelle/anleitung/lithophan-bilderrahmen/Standfuss.stl)

## Materialien

* [USB-LED-Stripe](https://www.amazon.de/gp/product/B07ML3NNY9/ref=as_li_ss_tl?ie=UTF8&psc=1&linkCode=ll1&tag=lithophan-bilderrahmen-21&linkId=7a26b8083e0d60821d345fe93f40e6df&language=de_DE)
* [Bilderrahmen Hängehaken Aufhänger](https://www.amazon.de/gp/product/B07CMWS29V/ref=as_li_ss_tl?ie=UTF8&psc=1&linkCode=ll1&tag=lithophan-bilderrahmen-21&linkId=ab5b29993ce1ab403a89796960c8dea6&language=de_DE)
* [8x DIN 7991 Senkkopfschraube mit Innensechskant M3x18mm](https://www.amazon.de/Senkkopfschrauben-Edelstahl-Senkschrauben-Innensechskant-Vollgewinde/dp/B06VYHN6D1/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=din+7991+m3&qid=1588187486&s=diy&sr=1-5&th=1&linkCode=ll1&tag=lithophan-bilderrahmen-21&linkId=85067181e312b96a948e97c701f25ac2&language=de_DE)
* [2x DIN 912 Zylinderkopfschraube mit Innensechskant M3x14mm](https://www.amazon.de/Zylinderschrauben-mit-Innensechskant-Vollgewinde-Zylinderkopfschrauben/dp/B078TP4ZRL/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=din912+m4x14&qid=1588188673&s=diy&sr=1-4&th=1&linkCode=ll1&tag=lithophan-bilderrahmen-21&linkId=d6ee35e8ff0e2d27e47358aff2c0f17d&language=de_DE)

<div class="box alt"><div class="row uniform"><div class="12u"><span class="image fit"> <a href="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-00Bauteiluebersicht.jpg"> <img src="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-00Bauteiluebersicht_small.jpg" alt="Bauteilübersicht" /></a> </span></div></div></div>

## Vorbereitungen - M3 Gewinde schneiden

Die Vorbereitungen sind schnell abgeschlossen.
Einfach im Rahmen 8x, im Deckel 2x M3 Gewinde schneiden, so wie auf den Bildern angedeutet.
Sofern ihr die Teile übrigens bei uns bestellt schneiden wir die Gewinde schon selbst für euch hinein :)

<div class="box alt"><div class="row uniform"><div class="6u"><span class="image fit"> <a href="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-01GewindeRahmen.jpg"> <img src="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-01GewindeRahmen_small.jpg" alt="Rahmen" /></a> </span></div><div class="6u"> <span class="image fit"> <a href="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-02GewindeDeckel.jpg"> <img src="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-02GewindeDeckel_small.jpg" alt="Deckel" /></a> </span></div></div></div>

## Aufbau

Der Aufbau ist dieses Mal sehr simpel. Wir haben den Bilderrahmen so optional wie möglich gestaltet. Ihr habt die Möglichkeit den Bilderrahmen entweder mit unserem Standfuss auf einen Tisch oder euer Sideboard zu stellen, ihr könnt ihn aber auch mit dem Haken an die Wand hängen. Desweiteren könnt ihr den Lithophan Bilderrahmen sowohl quer als auch hochkant verwenden!

Wir beginnen mit dem Rahmen und dem LED-Stripe. Diesen legt ihr in die vorgesehende Nut im Rahmen und zwar so, wie im Bild gezeigt. Im Normalfall würden wir das Anschlusskabel nach unten herausführen, ihr habt aber auch die Möglichkeit das Kabel zur Seite rauszuführen. Bevor wir den LED-Stripe nun gleich festkleben machen wir den Aufbau erstmal fertig, um das Ganze erstmal zu testen. Nach dem Einlegen des Stripes können wir schon das Lithophan-Bild einlegen. Achtet darauf, dass ihr die Bildseite (rauhe Seite) außen habt und dass es nicht auf dem Kopf steht!

<div class="box alt"><div class="row uniform"><div class="6u"><span class="image fit"> <a href="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-03LEDStripeUndLithophan.jpg"> <img src="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-03LEDStripeUndLithophan_small.jpg" alt="Rahmen" /></a> </span></div><div class="6u"> <span class="image fit"> <a href="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-04LEDStripeUndLithophan.jpg"> <img src="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-04LEDStripeUndLithophan_small.jpg" alt="Deckel" /></a> </span></div></div></div>

Nun können wir schonmal probehalber den Deckel mit dem Rahmen verschrauben mit Hilfe der 8x M3x18mm Senkkopfschrauben. So gemacht könnt ihr als nächstes die beiden Zylinderkopfschrauben M3x14mm in die zwei Kreisrunden Taschen einschrauben. Diese dienen der Fixierung des Lithophans, also sanft anziehen, aber nicht überdrehen! Danach ist auch schon entweder der Standfuss unten, oder wie bei uns der Haken oben dran. Die passende Schraube liegt übrigens entweder dem Haken bei, oder ihr bezieht das Ganze "Kleinteilepaket" inkl. Druckteile über uns.

<div class="box alt"><div class="row uniform"><div class="4u"><span class="image fit"> <a href="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-05Verschraubung.jpg"> <img src="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-05Verschraubung_small.jpg" alt="Verschraubung" /></a> </span></div><div class="4u"> <span class="image fit"> <a href="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-06Haken.jpg"> <img src="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-06Haken_small.jpg" alt="Der Aufhänger" /></a> </span></div><div class="4u"> <span class="image fit"> <a href="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-07Standfuss.jpg"> <img src="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-07Standfuss_small.jpg" alt="Alternative mit Standfuss" /></a> </span></div></div></div>

Und damit sind wir auch schon fertig! Zumindest mit dem Probedurchlauf ;)

<div class="box alt"><div class="row uniform"><div class="12u"><span class="image fit"> <a href="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-00Titelbild.jpg"> <img src="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-00Titelbild_small.jpg" alt="Fertig!" /></a> </span></div></div></div>

Schnell noch den USB-Stecker in ein handelsübliches USB-Smartphone-Ladegerät stecken, und schon sollte es leuchten :)
Sofern ihr den gleichen RGB-LED-Stripe wie wir über Amazon bezogen habt ihr sogar die Auswahl diverser Modi und Farben.

Nachdem hoffentlich alles funktioniert könnt ihr den Deckel wieder abschrauben und den LED-Stripe aufkleben. Achtet darauf, dass die Fläche fett- und staubfrei ist, am besten vorher säubern und gut trocknen lassen. Danach wieder alles zusammenschrauben, und FERTIG ist ein schöner UNIKAT Bilderrahmen.

Selbstverständlich könnt ihr solche Lithophane von uns auch einzeln beziehen.
Schickt dazu einfach das gewünschte Bild an
<a href="mailto:bestellung@haefner.technology?subject=Bestellung%20Lithophan">bestellung@haefner.technology</a>
