---
title: Supercharger
categories: Anleitung
tags:
 - Supercharger
 - Smartphonecharger
 - Smartphone
 - Season3
 - Selfmade
date: 2019/04/01 13:37:00
cover_index: /images/anleitung/supercharger/haefner-technology-supercharger-00cover-index.jpg
amazon: true
sitemap: true
---
Die passende Smartphone-Ladestation f&uuml;r alle Tesla-Fans.
<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-00Titelbild.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-00Titelbild.jpg" alt="Cablebox eingeklebt" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Gesamtkosten: 45 &euro;
Vorkenntnisse: Keine
Zeitaufwand: ca. 20 Minuten

## Druckteile

 * 1x [Frontplate](/druckmodelle/anleitung/supercharger/tesla-supercharger-frontplate.stl) (wei&szlig;)
 * 1x [Backplate](/druckmodelle/anleitung/supercharger/tesla-supercharger-backplate.stl) (wei&szlig;)
 * 2x [Rail](/druckmodelle/anleitung/supercharger/tesla-supercharger-rail.stl) (anthrazit)
 * 1x [Inlay](/druckmodelle/anleitung/supercharger/tesla-supercharger-inlay.stl) (rot)
 * 1x [Cablebox](/druckmodelle/anleitung/supercharger/tesla-supercharger-cablebox.stl) (schwarz)
 * 1x [Base](/druckmodelle/anleitung/supercharger/tesla-supercharger-base.stl) (anthrazit)
 * 1x [Stand](/druckmodelle/anleitung/supercharger/tesla-supercharger-stand.stl) (anthrazit)
 * 1x [Tesla-Background](/druckmodelle/anleitung/supercharger/tesla-supercharger-tesla-background.stl) (rot)

## Aufbau

Wir beginnen mit dem Aufbau des Superchargers. Als erstes wird die Cablebox in das Inlay geklebt. Hierf&uuml;r reicht Sekundenkleber und ein wenig Geschick.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-01Cablebox.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-01Cablebox_small.jpg" alt="Cablebox eingeklebt" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Das Inlay wird jetzt in die Backplate eingesetzt. Die Backplate unterscheidet sich von der Frontplate durch den Kabelkanal an der rechten Seite. Da die Teile sehr passgenau konstruiert sind, funktioniert es am Besten oben (an der flachen Seite) anzusetzen und nach unten hin einzuf&uuml;hren. Wenn es gar nicht reinrutschen will, kann man die Kanten noch etwas aggressiver Entgraten zum Beispiel mit einem Teppichmesser.

<div class="box alt">
	<div class="row uniform">
		<div class="1u"></div>
		<div class="5u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-02Backplate.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-02Backplate_small.jpg" alt="Backplate" /></a></span></div>
		<div class="5u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-03BackplateTop.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-03BackplateTop_small.jpg" alt="Backplate Top" /></a></span></div>
		<div class="1u$"></div>
	</div>
</div>

Als n&auml;chstes wird das Ladekabel eingebaut. Wichtig ist hierbei das Ladekabel zu erst richtig herum durch das Base-Teil zu f&uuml;hren.

<div class="box alt">
	<div class="row uniform">
		<div class="1u"></div>
		<div class="5u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-03BaseMitKabel.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-03BaseMitKabel_small.jpg" alt="Base mit Kabel" /></a></span></div>
		<div class="5u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-04BackplateMitKabel.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-04BackplateMitKabel_small.jpg" alt="Backplate mit Kabel" /></a></span></div>
		<div class="1u$"></div>
	</div>
</div>

Anschlie&szlig;end wird der Tesla-Background eingelegt.

<div class="box alt">
	<div class="row uniform">
		<div class="1u"></div>
		<div class="5u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-05TeslaBackground.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-05TeslaBackground_small.jpg" alt="Tesla Background" /></a></span></div>
		<div class="5u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-06BackplateMitKabelCloseUp.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-06BackplateMitKabelCloseUp_small.jpg" alt="Backplate mit Kabel Closeup" /></a></span></div>
		<div class="1u$"></div>
	</div>
</div>

Nun kann die Frontplate aufgesteckt werden.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-07Frontplatte.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-07Frontplatte_small.jpg" alt="Frontplatte aufgesteckt" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Zusammen mit den Rails links und rechts kann der Charger jetzt in die Base gesteckt werden.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-08Base.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-08Base_small.jpg" alt="Base aufgesteckt" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Nach dem Aufstecken des Stand ist der Supercharger auch schon fertig.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/supercharger/haefner-technology-supercharger-00Titelbild.jpg"><img src="/images/anleitung/supercharger/haefner-technology-supercharger-00Titelbild.jpg" alt="Cablebox eingeklebt" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

## Materialien

 * 1x [Anker 24W 2-port](https://www.amazon.de/gp/product/B00WLI5E3M/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=supercharger-21&creative=6742&linkCode=as2&creativeASIN=B00WLI5E3M&linkId=75ba4b5c5ce410c76c96b7dedb2a9109)
 * 1x schwarzes Ladekabel, z.B.:
[Anker 2x 1,8m Micro-USB](https://www.amazon.de/gp/product/B01MUJOA5E/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=supercharger-21&creative=6742&linkCode=as2&creativeASIN=B01MUJOA5E&linkId=a23c3c8b7dea082ad1253e6647127dc0) 
[Anker 1x 0,9m USB-C](https://www.amazon.de/gp/product/B01A6F3WHG/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=supercharger-21&creative=6742&linkCode=as2&creativeASIN=B01A6F3WHG&linkId=9c50c69fd6c2720c814b100230ca4ceb)
[Anker 1x 0,9m Apple Lightning](https://www.amazon.de/gp/product/B01N8W1EAE/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=supercharger-21&creative=6742&linkCode=as2&creativeASIN=B01N8W1EAE&linkId=12014efb05367c7ce5dc3fbc805b5134)

