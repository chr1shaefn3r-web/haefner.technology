---
title: Notenpult-Ablage
categories: Anleitung
tags:
 - Musiker
 - Notenpult
 - Ablage
 - mehrplatz
 - Musik
 - Season2
 - Selfmade
date: 2018/10/01 13:37:42
cover_index: /images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-cover-index.jpg
amazon: false
sitemap: true
---
Fast alle Musiker kennen das Problem: Auf dem Notenständer ist einfach zu wenig Platz!
Aber auch dieses Problem lässt sich mit Hilfe eines 3D-Druckers lösen. Und für die Grundversion muss man auch kein begnadeter Bastler sein ;)

<div class="box alt">
	<div class="row uniform">
		<div class="12u"><span class="image fit"><a href="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-00Titelbild.jpg"><img src="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-00Titelbild.jpg" alt="So kann die Notenpult-Ablage zum Schluss aussehen" /></a></span></div>
	</div>
</div>

Gesamtkosten: ca. 10-20 €
Vorkenntnisse: Keine
Zeitaufwand: ca. 2 h

## Druckteile

Die Notenpult-Ablage funktioniert mit dem Prinzip des Klemmens. Dadurch ist sie gut portabel, weil sie schnell und sehr sicher auf- und abgebaut werden kann. Allerdings muss dafür ein passender Adapter eingesetzt werden. Dazu zuerst den Durchmesser des Notenpult-Rohres messen (maximal 25&nbsp;mm möglich). Rohre mit 25&nbsp;mm Durchmesser benötigen gar keinen Adapter.

 * 1x [Ablage](/druckmodelle/anleitung/notenpult-ablage/Ablage.stl)
 * 2x [Einsatz 25 mm](/druckmodelle/anleitung/notenpult-ablage/Einsatz25.stl)
 * opt. 2x [Adapter 16 mm](/druckmodelle/anleitung/notenpult-ablage/Adapter16.stl)
 * opt. 2x [Adapter 14 mm](/druckmodelle/anleitung/notenpult-ablage/Adapter14.stl)
 * opt. 2x [Adapter 11 mm](/druckmodelle/anleitung/notenpult-ablage/Adapter11.stl)

## Der Kreativität sind keine Grenzen gesetzt

Bei diesem Projekt kann jeder für sich selbst entscheiden, wie viel Liebe in die Details gesteckt werden soll. Wir stellen hier zwei etwas einfachere Varianten kurz vor; die anspruchsvollere wird dann ausführlicher beschrieben.
Natürlich sind begnadeten Bastlern keine Grenzen gesetzt, die Ablage noch nach Lust und Laune zu verzieren ;)

### 1. Die faule Variante

Für die einfachste Variante einfach das Brett auf die 3D-gedruckte Ablage kleben und fertig.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-01VarianteFaul.jpg"><img src="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-01VarianteFaul_small.jpg" alt="Die einfachste Variante" /></a></span></div>
		<div class="2u"></div>
	</div>
</div>

### 2. Die vernünftige Variante

Um die Ablage auch im Konzert verwenden zu können, sollten zwei Mängel der faulen Variante behoben werden:

 a. Klappergeräusche beim Ablegen von Dingen
 b. Die auffällige Farbe von hellem Holz

Legt man einen Gegenstand (z.B. einen Bleistift) auf das blanke Holz, so hört sich das in der Regel sehr klapperig an, selbst wenn man ganz behutsam vorgeht. Das ist bedingt durch die große, aber dünne und leichte Holzplatte, die sich hervorragend als Resonanzkörper macht.
Ein wenig Bastelfilz schafft hier schnell und einfach Abhilfe.

Die in diesem Beispiel verwendete Sperrholzplatte ist aus Pappelholz. Dieses Holz ist sehr hell und fällt natürlich extrem auf, vor allem im Konzert. Mit ein wenig Farbe und/oder Lack sieht die Notenpult-Ablage gleich viel hochwertiger aus.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-02VarianteVernuenftig.jpg"><img src="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-02VarianteVernuenftig_small.jpg" alt="Die vernünftige Variante" /></a></span></div>
		<div class="2u"></div>
	</div>
</div>

### 3. Die edle Variante

Ein noch stimmigeres Gesamtkonzept erhält man, wenn man zusätlich zur zweiten Variante noch folgende Verbesserungen vornimmt:

 a. Eine Umrandung
 b. Eine Aussparung für den Notenständer

Eine Umrandung ist sehr sinnvoll, damit runde Gegenstände (z.B. Stifte) nicht herunter rollen und auch andere Dinge bei leichten Erschütterungen nicht gleich herunter fallen können. Für diese Umrandung kann jeder selbst seiner Kreativität freien Lauf lassen. Vor allem vorne und hinten ist eine Begrenzung sinnvoll, an den Seiten kann sie auch weggelassen werden.
In dieser Anleitung verwenden wir ein einfaches Winkelprofil aus schwarzem Kunststoff.

Wenn die Ablagefläche nicht nur plump "vor" dem Notenpult hervorsteht, sondern sich um die senkrechte Röhre schmiegt, sieht das Ganze gleich viel raffinierter aus und ist auch viel stabiler.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-00Titelbild.jpg"><img src="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-00Titelbild_small.jpg" alt="Die edle Variante" /></a></span></div>
		<div class="2u"></div>
	</div>
</div>

## Arbeitsschritte

Zuerst sollte das 3D-Druck-Teil "Ablage" kopfüber mittig (Lineal benutzen!) auf die Holzplatte gelegt und angezeichnet werden, was heraus geschnitten werden muss:

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-03Anzeichnen.jpg"><img src="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-03Anzeichnen_small.jpg" alt="Aussparung anzeichnen" /></a></span></div>
		<div class="6u"><span class="image fit"><a href="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-04Angezeichnet.jpg"><img src="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-04Angezeichnet_small.jpg" alt="Etwas Toleranz geben" /></a></span></div>
	</div>
</div>

Dieser Bereich muss nun entfernt werden. Vorsichtshalber sollte auf allen Seiten ca.&nbsp;1&nbsp;mm mehr entfernt werden. Mit einem Multicutter oder Dremel geht das sehr schnell und mühelos. Im Zweifelsfall funktioniert bei weichem Holz aber sogar ein einfaches Cuttermesser einwandfrei.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-05Aussparung.jpg"><img src="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-05Aussparung_small.jpg" alt="Aussparung entfernt" /></a></span></div>
		<div class="2u"></div>
	</div>
</div>

Anschließend kann die Holzplatte lackiert werden. Die Stellen, an die das 3D-Druck-Teil "Ablage" und die Umrandungen geklebt werden, müssen nicht lackiert werden. Genau wie die Oberseite, die im Anschluss mit Filz beklebt wird.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-06Lackiert.jpg"><img src="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-06Lackiert_small.jpg" alt="Auf die Oberseite kommt im Anschluss das Filz" /></a></span></div>
		<div class="2u"></div>
	</div>
</div>

Nun müssen noch die Stücke der Umrandung zurechtgeschnitten werden. Zum Schluss dann die Umrandung ankleben, das Filz ankleben und das 3D-Druck-Teil "Ablage" von unten ankleben (hier auf jeden Fall starken Klebstoff verwenden).

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-07AblageMitKlebeband.jpg"><img src="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-07AblageMitKlebeband_small.jpg" alt="Die Ablage wird mit doppelseitigem Klebeband befestigt" /></a></span></div>
		<div class="6u"><span class="image fit"><a href="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-08AblageGeklebt.jpg"><img src="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-08AblageGeklebt_small.jpg" alt="Ablage angegeklebt" /></a></span></div>
	</div>
</div>

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-09UmrandungGeklebt.jpg"><img src="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-09UmrandungGeklebt_small.jpg" alt="Umrandung angeklebt" /></a></span></div>
		<div class="6u"><span class="image fit"><a href="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-10FilzGeklebt.jpg"><img src="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-10FilzGeklebt_small.jpg" alt="Filz angeklebt" /></a></span></div>
	</div>
</div>

Fertig!

## Verwendung

Die verschiedenen Teile sind wie folgt zu verwenden:

Adapter in Einsatz stecken. Die zwei Einsätze von zwei Seiten an das Rohr des Notenständers ansetzen und miteinander verschränken. Die Breite muss nach oben hin kleiner werden.

Mit der anderen Hand die Ablage einfädeln und nach unten - gegen die Einsätze - drücken. Mit etwas Druck gegeneinander verkanten - allerdings nicht zu viel, sonst wird es problematisch beim Lösen.

Zum Auseinanderbauen mit einem Daumen die zwei Einsätze fixieren und mit der anderen Hand ein wenig an der Ablage wackeln, bis sich diese löst.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-11Verwendung.jpg"><img src="/images/anleitung/notenpult-ablage/haefner-technology-notenpult-ablage-11Verwendung_small.jpg" alt="Nahaufnahme der Verklemmung" /></a></span></div>
		<div class="2u"></div>
	</div>
</div>

## Materialien (gibt es alles im Baumarkt)

 * 1x Bastelfilz (z.B. 20x30 cm)
 * 1x Sperrholzplatte, Größe passend zum Filz (z.B. 6 mm Pappelholz) - im Baumarkt zuschneiden lassen ;)
 * opt. Umrandung (z.B. Kunststoff-Profil oder Carbon-Vierkantrohr)
 * Farbe oder Lack (z.B. Acryl-Wasserlack tiefschwarz matt)
 * Klebstoff (z.B. Epoxidharz) und/oder doppelseitiges Klebeband

## Benötigtes Werkzeug

 * Lineal und Bleistift
 * Säge, Dremel, Multicutter oder Cuttermesser

