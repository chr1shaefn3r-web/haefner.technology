---
title: Baby Groot
categories: Anleitung
tags:
 - GotG2
 - BabyGroot
 - Speaking
 - Bombe
 - Make
 - Season1
 - Selfmade
date: 2018/06/01 10:00:00
cover_index: /images/anleitung/baby-groot/haefner-technology-baby-groot-cover-index.jpg
amazon: true
sitemap: true
---
An alle Baby-Groot-Fans, die sich schon immer ihren eigenen sprechenden Baby Groot gew&uuml;nscht haben. Wir h&auml;tten da was f&uuml;r euch:
<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/baby-groot/haefner-technology-baby-groot-cover.jpg"><img src="/images/anleitung/baby-groot/haefner-technology-baby-groot-cover.jpg" alt="Baby Groot" /></a></span></div>
	</div>
</div>

## Druckteile

 * 1x [Baby Groot](https://www.thingiverse.com/thing:324433)
 * 1x [Bombe](/druckmodelle/anleitung/baby-groot/baby-groot-bomb/zip) (Bombe, Buttons und Einschub sind die wichtigen)

## Audio &uuml;bertragen

Zuerst m&uuml;ssen die richtigen Sounds per Micro-USB auf das Audioboard &uuml;bertragen werden. Dazu haben wir f&uuml;r euch ein Groot-Paket vorbereitet. Ihr m&uuml;sst es nur noch herunterladen, auspacken und auf das Audioboard kopieren.
Wichtig ist es, die vorinstallierten Beispielsounds _richtig_ zu l&ouml;schen. D.h. nicht nur in den Papierkorb verschieben, sondern endg&uuml;ltig l&ouml;schen. Unter Windows ist die Tastenkombination daf&uuml;: Shift + Entf.

 * [Audio english](/assets/anleitung/baby-groot/baby-groot-sounds-en.zip)
 * [Audio deutsch](/assets/anleitung/baby-groot/baby-groot-sounds-de.zip)

## Verkabeln

 * Batterie Plus (rot) an beide Boards (Y-Kabel) Vin
 * Batterie Minus (schwarz) an beide Boards (Y-Kabel) Gnd
 * Lautsprecher mit Verst&auml;rker verbinden (Polung egal)
 * Audioboard mit dem Verst&auml;rker verbinden: R & Gnd -> A+ & A-
 * Taster mit dem Audioboard verbinden: Gnd & 0 und Gnd & 1

F&uuml;r unsere Experimentierphase war es sinnvoll, alle Kabel mittels Crimp-Kontakten steckbar zu machen, anstatt sie einfach nur zu verl&ouml;ten. Dann sieht es wie im folgenden Bild aus. Zum Nachbasteln ist das aber nicht notwendig. Einfaches Zusammenl&ouml;ten reicht vollkommen aus und spart Platz. Wichtig ist ein kurzer Check, ob alles funktioniert _bevor_ man sich die M&uuml;he macht alles einzubauen.
<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u$"><span class="image fit"><a href="/images/anleitung/baby-groot/haefner-technology-baby-groot-verkabelung.jpg"><img src="/images/anleitung/baby-groot/haefner-technology-baby-groot-verkabelung_small.jpg" alt="Elektrik Verkabelung" /></a></span></div>
		<div class="2u"></div>
	</div>
</div>

## Elektronik einbauen

Nachdem die elektronischen Bauteile verbunden sind und die Funktion sichergestellt ist, k&ouml;nnen sie in die Bombe eingebaut werden. Alle Bauteile (au&szlig;er den Tastern) werden auf dem Einschub befestigt und auf diesem dann nur noch ins Geh&auml;use geschoben. Zu Beginn werden Audioboard, Verst&auml;rker und Batteriefach auf die Bodenplatte gesteckt und mit einem Tropfen Klebstoff fixiert.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u$"><span class="image fit"><a href="/images/anleitung/baby-groot/haefner-technology-baby-groot-elektrik-eingesteckt.jpg"><img src="/images/anleitung/baby-groot/haefner-technology-baby-groot-elektrik-eingesteckt_small.jpg" alt="Elektrik Eingesteckt" /></a></span></div>
		<div class="2u"></div>
	</div>
</div>

Danach wird der Lautsprecher in die Seite gelegt und die Taster in den Deckel gesteckt. Beiden werden mit [Sekundenkleber](https://www.amazon.de/gp/product/B004V4B60Y/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=baby-groot-21&creative=6742&linkCode=as2&creativeASIN=B004V4B60Y&linkId=658ad5973af1ca472db9b564b66e8348) fixiert.
Bei den Tastern ist das etwas kniffliger - dort haben wir auch mit [Montagekleber](https://www.amazon.de/gp/product/B004OHP15Y/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=baby-groot-21&creative=6742&linkCode=as2&creativeASIN=B004OHP15Y&linkId=e4c722492bd4ce3b6ff8af0359cc3ce3) nachgeholfen, um die Unebenheiten besser auszugleichen. Au&szlig;erdem muss der Taster auch gut genug befestigt sein, um ihn von oben dr&uuml;cken zu k&ouml;nnen ohne abzufallen.
Wichtig: Den Klebstoff unbedingt vom Taster-Stift fernhalten! Im Zweifelsfall z&uuml;gig mit K&uuml;chenpapier abwischen.

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/baby-groot/haefner-technology-baby-groot-lautsprecher-eingeklebt.jpg"><img src="/images/anleitung/baby-groot/haefner-technology-baby-groot-lautsprecher-eingeklebt_small.jpg" alt="Lautsprecher eingeklebt" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/baby-groot/haefner-technology-baby-groot-taster-eingeklebt.jpg"><img src="/images/anleitung/baby-groot/haefner-technology-baby-groot-taster-eingeklebt_small.jpg" alt="Taster eingeklebt" /></a></span></div>
	</div>
</div>

Sind die Taster befestigt und verl&ouml;tet bzw. aufgesteckt, kann der Einschub in die Bombe geschoben und diese verschlossen werden. Zum Zuschrauben k&ouml;nnen zwei M4 Senkschrauben oder kleine Spax-Schrauben aus der [Fischer Power-Fast-Box](https://www.amazon.de/gp/product/B0049AD504/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=baby-groot-21&creative=6742&linkCode=as2&creativeASIN=B0049AD504&linkId=5e97c015fc015b9387244ed06964b7e4) verwendet werden.

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/baby-groot/haefner-technology-baby-groot-lautsprecher-eingesteckt.jpg"><img src="/images/anleitung/baby-groot/haefner-technology-baby-groot-lautsprecher-eingesteckt_small.jpg" alt="Lautsprecher eingesteckt" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/baby-groot/haefner-technology-baby-groot-bombe-zusammengesteckt.jpg"><img src="/images/anleitung/baby-groot/haefner-technology-baby-groot-bombe-zusammengesteckt_small.jpg" alt="Bombe zusammengesteckt" /></a></span></div>
	</div>
</div>

## Extras Aufstecken

Damit das ganze noch mehr nach _der_ Bombe aus dem Film aussieht gibt es ein paar Extras zur Dekoration dazu. Einfach aufstecken und aufkleben, fertig. Nat&uuml;rlich darf die Bombe auch noch weiter mit Draht u.&Auml;. verziert werden.

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/baby-groot/haefner-technology-baby-groot-bomben-extras.jpg"><img src="/images/anleitung/baby-groot/haefner-technology-baby-groot-bomben-extras_small.jpg" alt="Bomben Extras" /></a></span></div>
		<div class="6u"><span class="image fit"><a href="/images/anleitung/baby-groot/haefner-technology-baby-groot-bombe-fertig.jpg"><img src="/images/anleitung/baby-groot/haefner-technology-baby-groot-bombe-fertig_small.jpg" alt="Bombe Fertig" /></a></span></div>
	</div>
</div>

## Baby Groot

Jetzt muss nurnoch der Baby-Groot dazugestellt werden, fertig. In Aktion kann man sich den sprechende Baby-Groot auf unserem Youtube-Kanal anschauen.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="https://youtu.be/UVSkmaubtRk"><img src="/images/anleitung/baby-groot/haefner-technology-baby-groot-cover-yt_small.jpg" alt="Baby Groot YouTube Video" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

## Materialien

 * [Adafruit Audioboard](https://www.amazon.de/gp/product/B00Q3U42DM/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=baby-groot-21&creative=6742&linkCode=as2&creativeASIN=B00Q3U42DM&linkId=7e647d8c5879e637134a515b3271fea2)
 * [Adafruit Verst&auml;rker](https://www.amazon.de/gp/product/B00PY2YSI4/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=baby-groot-21&creative=6742&linkCode=as2&creativeASIN=B00PY2YSI4&linkId=8a8c71211e747f08673480213feb1317)
 * [Batteriefach](https://www.amazon.de/gp/product/B077HZ9C2Q/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=baby-groot-21&creative=6742&linkCode=as2&creativeASIN=B077HZ9C2Q&linkId=733868d5e8545ea62d3c24113145eb77)
 * [Lautsprecher](https://www.amazon.de/gp/product/B019JMV0JY/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=baby-groot-21&creative=6742&linkCode=as2&creativeASIN=B019JMV0JY&linkId=09862662dbaf2e247bb8e332346f3a47)
 * Drucktaster, z.B. [als Set](https://www.amazon.de/gp/product/B01N67ICEC/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=baby-groot-21&creative=6742&linkCode=as2&creativeASIN=B01N67ICEC&linkId=564db7100cc8119392a8e78a395724d9) oder [einzeln](https://www.reichelt.com/de/de/Kurzhubtaster/TASTER-3301D/3/index.html?ACTION=3&LA=2&ARTICLE=27894&GROUPID=7587&artnr=TASTER+3301D&trstct=pol_44)
 * Kabel, z.B. [Kupferlitze](https://www.amazon.de/gp/product/B01BI1G88C/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=baby-groot-21&creative=6742&linkCode=as2&creativeASIN=B01BI1G88C&linkId=b29707b804bb1b7aa20ac5e1cf50942a)
 * Schrauben, z.B. [fischer Power-Fast-Box](https://www.amazon.de/gp/product/B0049AD504/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=baby-groot-21&creative=6742&linkCode=as2&creativeASIN=B0049AD504&linkId=5e97c015fc015b9387244ed06964b7e4)
 * Kleber, z.B. [Sekundenkleber](https://www.amazon.de/gp/product/B004V4B60Y/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=baby-groot-21&creative=6742&linkCode=as2&creativeASIN=B004V4B60Y&linkId=658ad5973af1ca472db9b564b66e8348) und [Montagekleber](https://www.amazon.de/gp/product/B004OHP15Y/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=baby-groot-21&creative=6742&linkCode=as2&creativeASIN=B004OHP15Y&linkId=e4c722492bd4ce3b6ff8af0359cc3ce3)

## Benötigtes Werkzeug

 * [L&ouml;tkolben](https://www.amazon.de/gp/product/B0019PQVH6/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=baby-groot-21&creative=6742&linkCode=as2&creativeASIN=B0019PQVH6&linkId=11430afceab57712136834a481c8558b)
 * [L&ouml;tzinn](https://www.amazon.de/gp/product/B000V8JZ2A/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=baby-groot-21&creative=6742&linkCode=as2&creativeASIN=B000V8JZ2A&linkId=61ef533977e4ff6da88958ef12469972)

## Copyright Disclaimer

This model is fanwork using models and characters from the Marvel cinematic universe, which are trademarked by Marvel Studios LLC, a Walt Disney Company. The movie's 3D models are created an owned by Marvel Studios and we do not claim any ownership over them or anything else from the Marvel cinematic universe. Our model may not be used for commercial purposes, it is intended for private entertainment only.

