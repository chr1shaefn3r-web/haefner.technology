---
title: Iron Man Handschuh
categories: Anleitung
tags:
 - Iron Man
 - Cosplay
 - Marvel
 - Avengers
 - Season2
 - Selfmade
date: 2018/11/02 13:37:42
cover_index: /images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-cover-index.jpg
amazon: true
sitemap: true
---
Viele von euch kennen mittlerweile die Marvel-Comic-Verfilmungen.
Wer hat nicht schon davon geträumt mal in einem Iron Man Anzug rumzulaufen oder damit zum Fasching zu gehen? Heute dürfen wir euch mit etwas Stolz einen ersten Entwurf des Iron Man Handschuhs vorstellen! :)
Dieses mal braucht ihr ein klein wenig mehr handwerkliches Geschick als sonst, es hält sich aber alles noch im Rahmen.

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-00Titelbild.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-00Titelbild.jpg" alt="So kann die iron-man-handschuh zum Schluss aussehen" /></a></span></div>
	</div>
</div>

Gesamtkosten: ca. 10-20€ pro Handschuh
Vorkenntnisse: dremeln, schleifen, kleben
Zeitaufwand: ca. 5h pro Handschuh

## Der Kreativität sind keine Grenzen gesetzt

Bei diesem Projekt kann jeder für sich selbst entscheiden, wie viel Liebe er in die Details stecken will. Ihr werdet an einigen Stellen merken, bzw. wir werden euch darauf hinweisen, dass es noch Verbesserungsmöglichkeiten gibt.

## Druckteile

Sobald ihr die Druckteile vorliegen habt könnt ihr beginnen.

 * 1x [Alle Finger](/druckmodelle/anleitung/iron-man-handschuh/Finger.stl)
 * 1x [Handschuh](/druckmodelle/anleitung/iron-man-handschuh/Rechter-Handschuh.stl)

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-01Druckteile.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-01Druckteile_small.jpg" alt="Übersicht der Druckteile" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

## Start

Es gibt zwei Dinge, mit denen ihr beginnen könnt. Entweder ihr kümmert euch zuerst um die Finger, oder ihr beginnt mit dem Zusammenfügen der beiden Teilen der Handfläche.

Damit ihr es später einfacher mit dem Handling habt raten wir euch mit dem Zusammenfügen anzufangen. Schnappt euch dafür den Dremel, haltet beide Teile aneinander und schneidet die unpassenden Stellen nach.

Verbesserung 1: Für eine verbesserte Version werden wir die Druckdaten anpassen, damit möglichst wenig nachgearbeitet werden muss. Kontaktiert uns gerne, wenn ihr Interesse habt!


## Verbinden

Sobald die Teile gut aneinanderpassen können wir uns um den Schließmechanismus kümmern. Wir haben uns hier auf der einen Seite für einen Gummizug und auf der anderen Seite für Magnete entschieden. Die Magnete können ganz einfach mit Sekundenkleber fixiert werden (unbedingt Handschuhe tragen und vorsichtig agieren, Sekundenkleber ist nicht ungefährlich, Sicherheitshinweise lesen!)

Tipp: Beim Andrücken der Gummibänder eine Lage Aluminiumfolie dazwischen legen. Dadurch vermeidet ihr Hautkontakt mit dem Sekundenkleber und dieser verbindet sich mit der glatten Alufolie nicht allzu gut.

Achtet unbedingt auf die Polung der Magnete!

<div class="box alt">
	<div class="row uniform">
		<div class="4u"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-02Magnete.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-02Magnete_small.jpg" alt="Magnete fixiert mit Sekundenkleber" /></a></span></div>
		<div class="4u"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-03Magnete2.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-03Magnete2_small.jpg" alt="Magnete fixiert mit Sekundenkleber" /></a></span></div>
		<div class="4u$"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-04Magnete3.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-04Magnete3_small.jpg" alt="Magnete fixiert mit Sekundenkleber" /></a></span></div>
		</div>
</div>

Sobald die Magnete angebracht sind könnt ihr auf der anderen Seite den Gummizug verkleben, hier funktioniert wiederum Sekundenkleber einwandfrei.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-05Gummizug.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-05Gummizug_small.jpg" alt="Gummizug" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>


## Finger verbinden

Für die Finger benötigen wir ebenfalls Gummizug und Sekundenkleber. Klebt zuerst das Gummi in der Fingerkuppe fest.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-06Fingerkuppe.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-06Fingerkuppe_small.jpg" alt="Fingerkuppe" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Danach die restlichen Fingerteile auf den Gummizug aufziehen, das Ganze an das Handteil halten, minimal spannen, Gummizug abschneiden und mit Sekundekleber am Handteil festkleben.

<div class="box alt">
	<div class="row uniform">
		<div class="4u"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-07Fingeraufgereiht.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-07Fingeraufgereiht_small.jpg" alt="Finger aufgereiht" /></a></span></div>
		<div class="4u"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-08Fingerverbunden.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-08Fingerverbunden_small.jpg" alt="Finger mit Handfläche verbunden" /></a></span></div>
		<div class="4u$"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-09AlleFingerverbunden.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-09AlleFingerverbunden_small.jpg" alt="Alle Finger verbunden" /></a></span></div>
		</div>
</div>

Damit ist das Grundgerüst des Handschuhs im Prinzip fertig!

<div class="box alt">
	<div class="row uniform">
		<div class="4u"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-10Grundgeruest-fertig.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-10Grundgeruest-fertig_small.jpg" alt="Alles verbunden - Faust" /></a></span></div>
		<div class="4u"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-11Grundgeruest-fertig2.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-11Grundgeruest-fertig2_small.jpg" alt="Alles verbunden - Oberseite" /></a></span></div>
		<div class="4u$"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-12Grundgeruest-fertig3.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-12Grundgeruest-fertig3_small.jpg" alt="Alle Finger verbunden - Unterseite" /></a></span></div>
		</div>
</div>


## Repulsor

Für den Repulsor müssen wir am Modell wieder etwas nacharbeiten. Dremelt das herausstehende Teil ein gutes Stück ab, so dass der Repulsor schön in der Mitte sitzt. Achtet darauf, dass auch die Rundung einigermaßen passt.

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-13RepulsorNacharbeit.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-13RepulsorNacharbeit_small.jpg" alt="Repulsor Nacharbeit" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-14RepulsorNacharbeit2.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-14RepulsorNacharbeit2_small.jpg" alt="Repulsor Nacharbeit" /></a></span></div>
	</div>
</div>

Nun muss der Repulsor selbst noch an dem Handschuh befestigt werden. Dazu haben wir das Druckteil mit Sekundenkleber fixiert und danach mit 2-Komponenten Epoxidharz von Innen verklebt.


<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-15Repulsorbefestigt.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-15Repulsorbefestigt_small.jpg" alt="Repulsor angeklebt" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-16Repulsorbefestigt2.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-16Repulsorbefestigt2_small.jpg" alt="Repulsor angeklebt" /></a></span></div>
	</div>
</div>


Um das Ganze noch etwas zu stabilisieren haben wir den Boden eines Joghurtbechers abgetrennt, zurecht geschnitten und verklebt.

Verbesserung 2a: Hier könnte man noch eine Schicht Backpapier verwenden oder einen etwas matteren Joghurtbecher, damit das Licht noch etwas gebrochen wird und diffuser erscheint.

Verbesserung 2b: Auch an dieser Stelle wäre eine Anpassung der Druckdaten sinnvoll, um das ganze Teil ohne weitere Nacharbeit einfach festkleben zu können.


<div class="box alt">
	<div class="row uniform">
		<div class="4u"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-17Joghurtbecher.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-17Joghurtbecher_small.jpg" alt="Joghurtbecher" /></a></span></div>
		<div class="4u"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-18Becherboden.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-18Becherboden_small.jpg" alt="Becherboden" /></a></span></div>
		<div class="4u$"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-19Bodenverklebt.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-19Bodenverklebt_small.jpg" alt="Boden verklebt" /></a></span></div>
	</div>
</div>


## Beleuchtung

Für die Beleuchtung gibt es dieses mal kein Tutorial, da es sehr darauf ankommt, wie ihr den Handschuh verwenden wollt. Wenn er im Regal stehen soll könnt ihr einfach von Innen eine normale Lampe reinstellen. Wollt ihr wie wir irgendwann mal einen kompletten Anzug haben solltet ihr auf eine Flache bauweise achten.
Wir haben uns dazu eine kleine Platine gelötet. Da wir aber auch noch nicht wissen, wie das später mal im Ganzen ausschauen wird haben wir die Platine nicht fixiert.

Und so sieht unser Ergebnis aus:

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-20fertig.jpg"><img src="/images/anleitung/iron-man-handschuh/haefner-technology-Iron-Man-Handschuh-20fertig_small.jpg" alt="Endergebnis" /></a></span></div>
	</div>
</div>



## Materialien

 * [Sekundenkleber](https://www.amazon.de/Pattex-Sekundenkleber-Ultra-Matic-PSG5C/dp/B004T2M0BC/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=iron-man-handschuh-21&linkId=8337c4bfea94eea88c3c87593dcc7324&language=de_DE)
 * [Zwei-Komponenten Epoxidharz Klebstoff](https://www.amazon.de/UHU-45700-Schnellfest-2-Komponenten-Epoxidharzkleber-L%C3%B6sungsmittel/dp/B000KJO9FI/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=iron-man-handschuh-21&linkId=f2aafc82e08ba59860a67ada1622b06e&language=de_DE)
 * [Gummiband](https://www.amazon.de/Gro%C3%9Fhandel-f%C3%BCr-Schneiderbedarf-Gummiband-schwarz/dp/B00KTKYHS2/ref=as_li_ss_tl?s=kitchen&ie=UTF8&qid=1541178970&sr=1-5&keywords=gummiband+20mm+schwarz&linkCode=ll1&tag=iron-man-handschuh-21&linkId=f04440c52454f2f5de05e3212f1379e6&language=de_DE)
 * [Magnete](https://www.amazon.de/gp/product/B0768KJTNT/ref=as_li_ss_tl?ie=UTF8&psc=1&linkCode=ll1&tag=iron-man-handschuh-21&linkId=a22338f94ac4aa3453ed7b3d06b384ea&language=de_DE)
 
 
## Benötigtes Werkzeug/Hilfsmittel

 * Dremel oder ähnliches Multi-Tool
 * Cuttermesser
 * Schere
 * [Handschuhe](https://www.amazon.de/Meditrade-1286M-Nitril-BestGen-St%C3%BCck/dp/B00KVAP1RQ/ref=as_li_ss_tl?ie=UTF8&qid=1541180337&sr=8-4&keywords=handschuhe+nitril&th=1&linkCode=ll1&tag=iron-man-handschuh-21&linkId=6210ad0fca84d06545cea97c3a249a7e&language=de_DE)
 * [Alufolie](https://www.amazon.de/Toppits-746574-Alufolie-10-m/dp/B013E6TK3E/ref=as_li_ss_tl?ie=UTF8&qid=1541180294&sr=8-3&keywords=aluminiumfolie+k%C3%BCche+abdecken&linkCode=ll1&tag=iron-man-handschuh-21&linkId=4df40011c3c59c4759f907a7f7146cbc&language=de_DE)(optional)

