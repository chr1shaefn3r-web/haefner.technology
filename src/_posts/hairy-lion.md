---
title: Hairy Lion
categories: Anleitung
tags:
 - ExcitingPrint
 - Lion
 - Selfmade
 - Make
 - Ultimaker2
 - Season1
date: 2018/04/02 18:00:00
cover_index: /images/anleitung/hairy-lion/haefner-technology-hairy-lion-cover-index.jpg
amazon: true
sitemap: true
---
Herzlich Willkommen zur Anleitung für euren eigenen Hairy Lion!
Im folgenden erfahrt ihr, wie ihr euch ganz einfach einen oder mehrere solcher frisierten Löwen herstellen könnt!

So kann der fertige Lion aussehen:
<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-fertig1.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-fertig1_small.jpg" alt="Hairy Lion, Gold" /></a></span></div>
	</div>
</div>

## Druckteile

 * 1x [Hairy Lion](https://www.thingiverse.com/thing:2007221)

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u$"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-unbearbeitet.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-unbearbeitet_small.jpg" alt="Hairy Lion, frisch gedruckt" /></a></span></div>
	</div>
</div>

## Stützwand entfernen

Sobald ihr den Löwen vorliegen habt kann es an die Nachbearbeitung gehen. Dazu muss zuerst die Stützwand entfernt werden. Dies funktioniert am besten, wenn man mit einer schmalen Schere von oben die Fäden (Haare) möglichst weit außen abschneidet, und dann einen Ring der Stützwand entfernt. Danach wieder ein Stück schneiden und den nächsten Ring entfernen.

<div class="box alt">
	<div class="row uniform">
		<div class="4u"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-haare-abschneiden.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-haare-abschneiden_small.jpg" alt="Haare abschneiden" /></a></span></div>
		<div class="4u"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-stuetzwand-entfernen.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-stuetzwand-entfernen_small.jpg" alt="Ring entfernen" /></a></span></div>
		<div class="4u$"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-stuetzwand-einschneiden.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-stuetzwand-einschneiden_small.jpg" alt="Stützwand einschneiden" /></a></span></div>
	</div>
</div>

Dies solange wiederholen, bis die komplette Stützwand entfernt ist. Das ganze sollte dann in etwa so aussehen:

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u$"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-stuetzwand-komplett-entfernt.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-stuetzwand-komplett-entfernt_small.jpg" alt="Stützwand komplett entfernt" /></a></span></div>
	</div>
</div>

## Frisur zurechtfönen

Nun kommt der interessante Teil, der euren Löwen einzigartig werden lässt. Dazu braucht ihr eine [Heißluftpistole](https://www.amazon.de/dp/B0001D1Q2M/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=hairy-lion-21&linkId=cbfa64eca758eca08c7429ee82b63219).
Ein Fön funtioniert eventuell auch, die Heißluftpistole hat aber den Vorteil, dass man die Temperatur ab Stufe2 einstellen kann.

Falls ihr die gleiche oder eine ähnliche Heißluftpistole habt haben sich 100°C auf Stufe2 bewährt, um den Löwen zu frisieren :)
Einfach von vorne mit schwenkenden Bewegungen (so wie ihr euch selbst fönen würdet) die Haare nach hinten fönen. Nicht zu lange auf einer Stelle bleiben, sonst krüllen sich die Haare zu stark und fangen an zu schmelzen. Ansonsten geht probieren über studieren!

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-foenen1.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-foenen1_small.jpg" alt="Fönen" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-foenen2.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-foenen2_small.jpg" alt="und noch mehr Fönen" /></a></span></div>
	</div>
</div>

Und so kann der Löwe dann fertig aussehen:

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-fertig2.jpg"><img src="/images/anleitung/hairy-lion/haefner-technology-hairy-lion-fertig2_small.jpg" alt="Und fertig ist der Löwe" /></a></span></div>
	</div>
</div>


## Materialien

 * 1x   Hairy Lion

## Benötigtes Werkzeug

 * Schere
 * [Heißluftpistole](https://www.amazon.de/dp/B0001D1Q2M/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=hairy-lion-21&linkId=cbfa64eca758eca08c7429ee82b63219)

