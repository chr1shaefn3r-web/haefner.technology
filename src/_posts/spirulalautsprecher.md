---
title: Spirula-Lautsprecher
categories: Anleitung
tags:
 - Lautsprecher
 - Hifi
 - Selfmade
 - Make
date: 2017/10/8 13:37:00
cover_index: /images/anleitung/spirulalautsprecher/spirulalautsprecher-cover-index.jpg
cover: /images/anleitung/spirulalautsprecher/IMG_2625.JPG
amazon: true
description: Anleitung um einen Spirula-Lautsprecher selbst zu bauen. 3D-Druck Teile, Lautsprecher, Kleber, zack-fertig, Spirula-Lautsprecher.
---
Herzlich Willkommen zur Bauanleitung für einen Spirula-Lautsprecher!
Im folgenden erfahrt ihr, wie ihr euch ganz einfach einen oder mehrere solcher Lautsprecher selbst herstellen und zusammenbauen könnt!
So soll das fertige Teil aussehen:

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2625.JPG" alt="Spirallautsprecher Anthrazit, Orange" /></span></div>
	</div>
    <div class="row uniform">
        <div class="4u"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2504.JPG" alt="Spirallautsprecher Anthrazit, Orange" /></span></div>
        <div class="4u"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2507.JPG" alt="Spirallautsprecher Anthrazit, Orange" /></span></div>
        <div class="4u$"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2516.JPG" alt="Spirallautsprecher Anthrazit, Orange" /></span></div>
    </div>
</div>

## Druckteile

 * 2x [Lautsprechergehäusehalbschale](https://www.thingiverse.com/download:3522983)
   (*Hinweis*: muss auf 96% skaliert werden)
 * 3x [Fusspin](/druckmodelle/anleitung/spirulalautsprecher/pin-35mm.stl)

<a href="https://www.3dhubs.com/service/83913" data-3dhubs-widget="button" data-hub-id="83913" data-type="orderWidget" data-color="light" data-size="large" data-text="via 3D-Hubs bei uns bestellen">via 3D-Hubs bei uns bestellen</a>
<script>!function(a,b,c,d){var e,g=(a.getElementsByTagName(b)[0],/^http:/.test(a.location)?"http":"https");a.getElementById(d)||(e=a.createElement(b),e.id=d,e.src=g+"://d3d4ig4df637nj.cloudfront.net/w/2.0.js",e.async=!0,a.body.appendChild(e))}(document,"script",1,"h3d-widgets-js");</script>

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2504.JPG" alt="Spirallautsprecher Haelfte" /></span></div>
	</div>
</div>

## Buchsen einschrauben

Sobald ihr die Druckteile vorliegen habt kann es an die Montage gehen.
Dazu zuerst die Buchsen wie auf dem Bild einschrauben, eine pro Hälfte.

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2507.JPG" alt="Spirallautsprecher Buchse" /></span></div>
	</div>
</div>

## Stützfüße einschrauben

Danach müssen die Fußpins angeschraubt werden.
Unsere Empfehlung: Verwendet [M3x20 Spax-Schrauben](https://www.amazon.de/SPAX-Universalschraube-Kreuzschlitz-Vollgewinde-1081020300123/dp/B0057WZ8L2/ref=as_li_ss_tl?ie=UTF8&qid=1506266987&sr=8-5&keywords=spax+3mm+10%22%20ADD_DATE=%221506268049%22%20LAST_MODIFIED=%221506268051%22%20ICON_URI=%22https://www.amazon.de/favicon.ico%22%20ICON=%22data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAC3UlEQVRYhbWXvWvbQBTABZIDhU7N0iHuWrLZ3pulFnR0huQPKNQeC4GKTt3iOcUaC4FAiZc0crukkI8hXWrLBEoDUTu0iwTxkiAnHdKSX4ezbH1cbMWxHzwQ5t17v/dxdz5F6QlwH3gNnDA9+QasAPeUsACPgZ9TDCwDeRTO/FbBfd/Htu2+ttttPM8bB2JGAd6kXbFtWRR1HS2T6asa+i7qOtuWdRuIlwopsvd9n6KuR4INU8Mw0gLYShqrpeXlaNaaNhImZSX+jASwLEsaIFcosLS8nGhJoE91PQ0AIwFelMsjS2wYhhRiIgDlSiXh2Pf9iI3jOKi9gVQnDQDRbddut6U2cUh1kgBx8TwP27apmSY10+wPqapp0wPwPI/VapVcoSDNdqoV2LYs5rJZ6aAN244TAbBtOxIoHNAwDGqmyd7e3vQqkMvnE86Luo7ruhG7qVTAcRypY9u2I3ae5yVaMpEKWI1Gouwyx7IWaJlMqhtyKEDNNKWO4weR7LDSMhmsRuNuAEFm4f0dP4o3NjakZ4CWyZAvFBKwtwII9zY+4bl8nrls9sZtGPxeM83xASB5Fd+ksgNqLpvFcZy7AQR/RhIZ9kqu9lrS7Xb7t2JQoVHBUwEEEDXT7IOomkauUKBcqSS25Gq1SrlSodsN9f7ChU4LTptwFZ2JKMD3Nfi1lYYpnVy6sF+CugKbPT1eGwJwdgJbs2JRp3V3gL9d4efCFZl/mhdJ3ggAwnBnQdDul0RFrrrjAbi7gwqcnQifp80EwD8pxNeVfumuNxX48lzQd1qJPvbXdFrwY13Ybs2KgHUFfn8QMJ8X4qvOFcQDQS6dFnycHzgK9zLQeuw7rAclOO+99DpNUYWoHCrAq6FlBJHBzpNBsHo04HUcqLmSdobKCjADHKWx5tIVMMdvRW/3S3CwOGiPuytvj1wOATV4Hz4ERt8ck5P3wAMlLsAisM6wuRhfjoB3wLNwzP/8XcqRf+mhpAAAAABJRU5ErkJggg==&linkCode=ll1&tag=spirulalautsprecher-21&linkId=0377603f53996adc816a1ec0ca356861).
Den Fußpin, der in der Mitte zwischen beiden Hälften sitzt, solltet ihr in eine der Hälften einkleben.

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2520.JPG" alt="Spirallautsprecher Fussschraube" /></span></div>
		<div class="6u$"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2521.JPG" alt="Spirallautsprecher Fuss angeschraubt" /></span></div>
	</div>
</div>

## Lautsprecherkabel vorbereiten

Danach folgt die Vorbereitung der [Lautsprecherkabel](https://www.amazon.de/gp/product/B006LW0WDQ/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=spirulalautsprecher-21&linkId=857ef4d3dd922ab1cf78ae881dde5dfd). Dazu ca. 10-15cm Kabel abschneiden und Abisolieren.
Dann die [Lautsprecherkabel](https://www.amazon.de/gp/product/B006LW0WDQ/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=spirulalautsprecher-21&linkId=857ef4d3dd922ab1cf78ae881dde5dfd) mit Lötzinn bedecken, damit sich die Adern nicht vereinzeln.

<div class="box alt">
	<div class="row uniform">
		<div class="4u"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2508.JPG" alt="Spirulalautsprecher" /></span></div>
		<div class="4u"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2509.JPG" alt="Spirulalautsprecher" /></span></div>
		<div class="4u$"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2510.JPG" alt="Spirulalautsprecher" /></span></div>
	</div>
</div>

## Lautsprecherkabel anlöten

Im nächsten Schritt das Kabel an den Lautsprecher anlöten.

## Watte einlegen

Wenn der mittlere, geklebte Fußpin fest und sicher ist geht es mit der Vorbereitung zum Zusammenkleben der Hälften weiter.
Dazu die beiden Lautsprecherhälften mit jeweils 100g [Watte](https://www.amazon.de/gp/product/B00133N1QI/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=spirulalautsprecher-21&linkId=041269dcb8a4be894aeff4623791e7db) füllen.

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2516.JPG" alt="Spirallautsprecher Watte" /></span></div>
	</div>
</div>

## Verkleben

Nun beide Hälfte zusammenkleben. Dabei aufpassen, dass die Watte nicht zwischen die Klebeflächen gerät!
Beide Hälften gut zusammenpressen, damit kein Spalt entsteht.

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2519.JPG" alt="Spirallautsprecher Verklebt" /></span></div>
	</div>
</div>

## Befestigungslöcher bohren und Lautsprecher einschrauben

Nun geht es an die Endmontage.
Den Lautsprecher mit zwei DIN912 M4x10 Zylinderkopfschrauben provisorisch befestigen.
Leider passen die restlichen vier Löcher des Lautsprechers _nicht_ zu den vorgedruckten Löchern,
daher müssen sie mit einem 4er Bohrer vorgebohrt werden.
Danach die restlichen vier Schrauben einschrauben.
Optional: Mit einem 3.5er Bohrer vorbohren und ein M4 Gewinde schneiden.

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2622.JPG" alt="Lautsprecher eingeschraubt" /></span></div>
	</div>
</div>

## Bananenstecker verlöten

Hier ist es nun wichtig, dass ihr eine ungefähre Vorstellung davon habt, wo einmal die Lautsprecher stehen sollen
und wo der Verstärker stehen wird. Grundsätzlich lieber ein paar Zentimeter mehr abschneiden, damit ihr euch später nicht ärgert.
Auch hier sollten dann die Enden mit Lötzinn benetzt werden. Anschließend die Enden in den Bananensteckern mit Hilfe der Madenschrauben befestigen.


<div class="box alt">
	<div class="row uniform">
		<div class="4u"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2501.JPG" alt="Spirulalautsprecher" /></span></div>
		<div class="4u"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2502.JPG" alt="Spirulalautsprecher" /></span></div>
		<div class="4u$"><span class="image fit"><img src="/images/anleitung/spirulalautsprecher/IMG_2503.JPG" alt="Spirulalautsprecher" /></span></div>
	</div>
</div>

## Materialien pro Lautsprecher

 * 2x   [Lautsprechergehäusehalbschale](https://www.thingiverse.com/download:3522983)
 * 3x   [Fußpin](/druckmodelle/anleitung/spirulalautsprecher/pin-35mm.stl)
 * 1x   [Lautsprecher](https://www.amazon.de/gp/product/B0045V1VKI/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=spirulalautsprecher-21&linkId=b494ff901e2976f9d7ba18ab8921afda)
 * 200g [Watte](https://www.amazon.de/gp/product/B00133N1QI/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=spirulalautsprecher-21&linkId=041269dcb8a4be894aeff4623791e7db)
 * 2x   [Buchse](https://www.amazon.de/gp/product/B0047N09G6/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=spirulalautsprecher-21&linkId=b2d15523ab89a5864b2a39a9d4a59356)
 * 2x   [Bananenstecker](https://www.amazon.de/gp/product/B01NATXCXS/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=spirulalautsprecher-21&linkId=4736c31b1fd6e44e5a9aa9dd380e5568)
 * 6x   [DIN 912 M4x10 Zylinderkopfschrauben](https://www.amazon.de/DealMux-M4x10mm-Edelstahl-Kantschraube-Schraube/dp/B072N7SMXM/ref=as_li_ss_tl?ie=UTF8&qid=1506267183&sr=8-3&keywords=m4+din912+10mm%22%20ADD_DATE=%221506268044%22%20LAST_MODIFIED=%221506268046%22%20ICON_URI=%22https://www.amazon.de/favicon.ico%22%20ICON=%22data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAC3UlEQVRYhbWXvWvbQBTABZIDhU7N0iHuWrLZ3pulFnR0huQPKNQeC4GKTt3iOcUaC4FAiZc0crukkI8hXWrLBEoDUTu0iwTxkiAnHdKSX4ezbH1cbMWxHzwQ5t17v/dxdz5F6QlwH3gNnDA9+QasAPeUsACPgZ9TDCwDeRTO/FbBfd/Htu2+ttttPM8bB2JGAd6kXbFtWRR1HS2T6asa+i7qOtuWdRuIlwopsvd9n6KuR4INU8Mw0gLYShqrpeXlaNaaNhImZSX+jASwLEsaIFcosLS8nGhJoE91PQ0AIwFelMsjS2wYhhRiIgDlSiXh2Pf9iI3jOKi9gVQnDQDRbddut6U2cUh1kgBx8TwP27apmSY10+wPqapp0wPwPI/VapVcoSDNdqoV2LYs5rJZ6aAN244TAbBtOxIoHNAwDGqmyd7e3vQqkMvnE86Luo7ruhG7qVTAcRypY9u2I3ae5yVaMpEKWI1Gouwyx7IWaJlMqhtyKEDNNKWO4weR7LDSMhmsRuNuAEFm4f0dP4o3NjakZ4CWyZAvFBKwtwII9zY+4bl8nrls9sZtGPxeM83xASB5Fd+ksgNqLpvFcZy7AQR/RhIZ9kqu9lrS7Xb7t2JQoVHBUwEEEDXT7IOomkauUKBcqSS25Gq1SrlSodsN9f7ChU4LTptwFZ2JKMD3Nfi1lYYpnVy6sF+CugKbPT1eGwJwdgJbs2JRp3V3gL9d4efCFZl/mhdJ3ggAwnBnQdDul0RFrrrjAbi7gwqcnQifp80EwD8pxNeVfumuNxX48lzQd1qJPvbXdFrwY13Ybs2KgHUFfn8QMJ8X4qvOFcQDQS6dFnycHzgK9zLQeuw7rAclOO+99DpNUYWoHCrAq6FlBJHBzpNBsHo04HUcqLmSdobKCjADHKWx5tIVMMdvRW/3S3CwOGiPuytvj1wOATV4Hz4ERt8ck5P3wAMlLsAisM6wuRhfjoB3wLNwzP/8XcqRf+mhpAAAAABJRU5ErkJggg==&linkCode=ll1&tag=spirulalautsprecher-21&linkId=533b1ff7a85eaf7be51adfc594b60995)
   oder [Matt-Schwarze Zylinderkopfschrauben](https://www.amazon.de/Aerzetix-Schrauben-Zylinderkopfschrauben-geschw%C3%A4rzte-Aufdruck/dp/B072X7ZH31/ref=as_li_ss_tl?ie=UTF8&qid=1506267183&sr=8-2&keywords=m4+din912+10mm%22%20ADD_DATE=%221506268038%22%20LAST_MODIFIED=%221506268040%22%20ICON_URI=%22https://www.amazon.de/favicon.ico%22%20ICON=%22data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAC3UlEQVRYhbWXvWvbQBTABZIDhU7N0iHuWrLZ3pulFnR0huQPKNQeC4GKTt3iOcUaC4FAiZc0crukkI8hXWrLBEoDUTu0iwTxkiAnHdKSX4ezbH1cbMWxHzwQ5t17v/dxdz5F6QlwH3gNnDA9+QasAPeUsACPgZ9TDCwDeRTO/FbBfd/Htu2+ttttPM8bB2JGAd6kXbFtWRR1HS2T6asa+i7qOtuWdRuIlwopsvd9n6KuR4INU8Mw0gLYShqrpeXlaNaaNhImZSX+jASwLEsaIFcosLS8nGhJoE91PQ0AIwFelMsjS2wYhhRiIgDlSiXh2Pf9iI3jOKi9gVQnDQDRbddut6U2cUh1kgBx8TwP27apmSY10+wPqapp0wPwPI/VapVcoSDNdqoV2LYs5rJZ6aAN244TAbBtOxIoHNAwDGqmyd7e3vQqkMvnE86Luo7ruhG7qVTAcRypY9u2I3ae5yVaMpEKWI1Gouwyx7IWaJlMqhtyKEDNNKWO4weR7LDSMhmsRuNuAEFm4f0dP4o3NjakZ4CWyZAvFBKwtwII9zY+4bl8nrls9sZtGPxeM83xASB5Fd+ksgNqLpvFcZy7AQR/RhIZ9kqu9lrS7Xb7t2JQoVHBUwEEEDXT7IOomkauUKBcqSS25Gq1SrlSodsN9f7ChU4LTptwFZ2JKMD3Nfi1lYYpnVy6sF+CugKbPT1eGwJwdgJbs2JRp3V3gL9d4efCFZl/mhdJ3ggAwnBnQdDul0RFrrrjAbi7gwqcnQifp80EwD8pxNeVfumuNxX48lzQd1qJPvbXdFrwY13Ybs2KgHUFfn8QMJ8X4qvOFcQDQS6dFnycHzgK9zLQeuw7rAclOO+99DpNUYWoHCrAq6FlBJHBzpNBsHo04HUcqLmSdobKCjADHKWx5tIVMMdvRW/3S3CwOGiPuytvj1wOATV4Hz4ERt8ck5P3wAMlLsAisM6wuRhfjoB3wLNwzP/8XcqRf+mhpAAAAABJRU5ErkJggg==&linkCode=ll1&tag=spirulalautsprecher-21&linkId=ebe0ab0f1367a4c6177a5ce39acb4953)
 * 3x   [M3x20 Spax-Schrauben](https://www.amazon.de/SPAX-Universalschraube-Kreuzschlitz-Vollgewinde-1081020300123/dp/B0057WZ8L2/ref=as_li_ss_tl?ie=UTF8&qid=1506266987&sr=8-5&keywords=spax+3mm+10%22%20ADD_DATE=%221506268049%22%20LAST_MODIFIED=%221506268051%22%20ICON_URI=%22https://www.amazon.de/favicon.ico%22%20ICON=%22data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAC3UlEQVRYhbWXvWvbQBTABZIDhU7N0iHuWrLZ3pulFnR0huQPKNQeC4GKTt3iOcUaC4FAiZc0crukkI8hXWrLBEoDUTu0iwTxkiAnHdKSX4ezbH1cbMWxHzwQ5t17v/dxdz5F6QlwH3gNnDA9+QasAPeUsACPgZ9TDCwDeRTO/FbBfd/Htu2+ttttPM8bB2JGAd6kXbFtWRR1HS2T6asa+i7qOtuWdRuIlwopsvd9n6KuR4INU8Mw0gLYShqrpeXlaNaaNhImZSX+jASwLEsaIFcosLS8nGhJoE91PQ0AIwFelMsjS2wYhhRiIgDlSiXh2Pf9iI3jOKi9gVQnDQDRbddut6U2cUh1kgBx8TwP27apmSY10+wPqapp0wPwPI/VapVcoSDNdqoV2LYs5rJZ6aAN244TAbBtOxIoHNAwDGqmyd7e3vQqkMvnE86Luo7ruhG7qVTAcRypY9u2I3ae5yVaMpEKWI1Gouwyx7IWaJlMqhtyKEDNNKWO4weR7LDSMhmsRuNuAEFm4f0dP4o3NjakZ4CWyZAvFBKwtwII9zY+4bl8nrls9sZtGPxeM83xASB5Fd+ksgNqLpvFcZy7AQR/RhIZ9kqu9lrS7Xb7t2JQoVHBUwEEEDXT7IOomkauUKBcqSS25Gq1SrlSodsN9f7ChU4LTptwFZ2JKMD3Nfi1lYYpnVy6sF+CugKbPT1eGwJwdgJbs2JRp3V3gL9d4efCFZl/mhdJ3ggAwnBnQdDul0RFrrrjAbi7gwqcnQifp80EwD8pxNeVfumuNxX48lzQd1qJPvbXdFrwY13Ybs2KgHUFfn8QMJ8X4qvOFcQDQS6dFnycHzgK9zLQeuw7rAclOO+99DpNUYWoHCrAq6FlBJHBzpNBsHo04HUcqLmSdobKCjADHKWx5tIVMMdvRW/3S3CwOGiPuytvj1wOATV4Hz4ERt8ck5P3wAMlLsAisM6wuRhfjoB3wLNwzP/8XcqRf+mhpAAAAABJRU5ErkJggg==&linkCode=ll1&tag=spirulalautsprecher-21&linkId=0377603f53996adc816a1ec0ca356861)
 * 10cm [Lautsprecherkabel](https://www.amazon.de/gp/product/B006LW0WDQ/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=spirulalautsprecher-21&linkId=857ef4d3dd922ab1cf78ae881dde5dfd) (für die innenliegende Verkabelung)
 * 2m [Lautsprecherkabel](https://www.amazon.de/gp/product/B006LW0WDQ/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=spirulalautsprecher-21&linkId=857ef4d3dd922ab1cf78ae881dde5dfd) (die eigentlichen Anschlusskabel der Lautsprecher)
 * (Sekunden)-[Kleber](https://www.amazon.de/gp/product/B06XCM6DL8/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=spirulalautsprecher-21&linkId=96dd3c4f138f03668bbafd4443b477b7)


## Gesamtsetup

Um die Lautsprecher betreiben zu können benötigt ihr in jedem Fall einen Verstärker.
Um das ganze System abzurunden, empfehlen wir euch auch einen Subwoofer zu verwenden.
So habt ihr ein schickes 2.1 System.
Um das System auch am Fernseher verwenden zu können benötigt ihr vermutlich einen Digital-Analog Konverter,
da die meisten Fernseher nur Digitalausgänge haben (außer ihr verwendet den Kopfhörerausgang).

 * 1x [Verstärker](https://www.amazon.de/gp/product/B01N6KLCFH/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=spirulalautsprecher-21&linkId=9b102fba89ba86ff3fffda52ecc8bcdf)
 * 1x [Subwoofer](https://www.amazon.de/SW-300-passive-Subwoofer-Lautsprecher-Reckhorn/dp/B017H80KL6/ref=as_li_ss_tl?ie=UTF8&qid=1506265743&sr=8-1&keywords=sw-300+reckhorn&linkCode=ll1&tag=spirulalautsprecher-21&linkId=f21e6de50de84cc2cda0211fcafac3dd)
 * 1x [Cinch-Kabel](https://www.amazon.de/gp/product/B01D5H8P0G/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=spirulalautsprecher-21&linkId=573bdc7cf0411ed08b2601462a62d9a1)

Optional:

 * 1x [Analog-Digital-Wandler](https://www.amazon.de/gp/product/B005H5JTMI/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=spirulalautsprecher-21&linkId=8a07443240cd99252adaf66b0f268293)

## Benötigtes Werkzeug

Lötkolben inkl. Lötzinn
Schraubendreher

