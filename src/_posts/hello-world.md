---
title: Hello World
tags:
 - HelloWorld
cover_index: /images/pic01.jpg
cover_detail: /images/3dprinting_center.jpg
sitemap: false
---
Welcome to [Hexo](https://hexo.io/)! This is your very first post. Check [documentation](https://hexo.io/docs/) for more info. If you get any problems when using Hexo, you can find the answer in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).

## Quick Start

### Create a new post

``` bash
$ hexo new "My New Post"
```

More info: [Writing](https://hexo.io/docs/writing.html)

### Run server

``` bash
$ hexo server
```

More info: [Server](https://hexo.io/docs/server.html)

### Generate static files

``` bash
$ hexo generate
```

More info: [Generating](https://hexo.io/docs/generating.html)

### Deploy to remote sites

``` bash
$ hexo deploy
```

More info: [Deployment](https://hexo.io/docs/deployment.html)

### Lorem Ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sapien tellus, luctus nec tempor vel, tincidunt eget massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec in nibh nec enim tincidunt sollicitudin. Nullam lobortis felis et nunc sollicitudin sodales. Cras eget risus efficitur neque semper malesuada. Vestibulum volutpat neque mi, non sollicitudin turpis facilisis at. Nam gravida sagittis gravida. In placerat blandit risus, ac convallis mi hendrerit ac. Etiam auctor auctor orci sit amet tempus. Etiam varius ut ipsum non blandit. Donec efficitur finibus posuere. Donec a ornare justo. Proin malesuada interdum libero nec condimentum. Duis elit enim, feugiat ac euismod sagittis, interdum vel lorem. Sed vehicula pretium tellus eget pellentesque.

Quisque laoreet ultrices nisl ornare accumsan. Praesent accumsan ut nisl volutpat pulvinar. In pretium mauris sed bibendum pellentesque. Suspendisse eget justo tempor, suscipit ante in, imperdiet eros. Morbi euismod nibh odio, sed vehicula mauris efficitur et. Vivamus sed consequat nunc. Donec ac tempor ipsum, vel porta nunc. Sed eros ipsum, bibendum sed bibendum sit amet, aliquam eget odio. Proin a turpis sit amet turpis tempus ullamcorper. Cras fermentum nunc id lorem faucibus, ultrices viverra tellus molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sapien tellus, luctus nec tempor vel, tincidunt eget massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec in nibh nec enim tincidunt sollicitudin. Nullam lobortis felis et nunc sollicitudin sodales. Cras eget risus efficitur neque semper malesuada. Vestibulum volutpat neque mi, non sollicitudin turpis facilisis at. Nam gravida sagittis gravida. In placerat blandit risus, ac convallis mi hendrerit ac. Etiam auctor auctor orci sit amet tempus. Etiam varius ut ipsum non blandit. Donec efficitur finibus posuere. Donec a ornare justo. Proin malesuada interdum libero nec condimentum. Duis elit enim, feugiat ac euismod sagittis, interdum vel lorem. Sed vehicula pretium tellus eget pellentesque.

Quisque laoreet ultrices nisl ornare accumsan. Praesent accumsan ut nisl volutpat pulvinar. In pretium mauris sed bibendum pellentesque. Suspendisse eget justo tempor, suscipit ante in, imperdiet eros. Morbi euismod nibh odio, sed vehicula mauris efficitur et. Vivamus sed consequat nunc. Donec ac tempor ipsum, vel porta nunc. Sed eros ipsum, bibendum sed bibendum sit amet, aliquam eget odio. Proin a turpis sit amet turpis tempus ullamcorper. Cras fermentum nunc id lorem faucibus, ultrices viverra tellus molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sapien tellus, luctus nec tempor vel, tincidunt eget massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec in nibh nec enim tincidunt sollicitudin. Nullam lobortis felis et nunc sollicitudin sodales. Cras eget risus efficitur neque semper malesuada. Vestibulum volutpat neque mi, non sollicitudin turpis facilisis at. Nam gravida sagittis gravida. In placerat blandit risus, ac convallis mi hendrerit ac. Etiam auctor auctor orci sit amet tempus. Etiam varius ut ipsum non blandit. Donec efficitur finibus posuere. Donec a ornare justo. Proin malesuada interdum libero nec condimentum. Duis elit enim, feugiat ac euismod sagittis, interdum vel lorem. Sed vehicula pretium tellus eget pellentesque.

Quisque laoreet ultrices nisl ornare accumsan. Praesent accumsan ut nisl volutpat pulvinar. In pretium mauris sed bibendum pellentesque. Suspendisse eget justo tempor, suscipit ante in, imperdiet eros. Morbi euismod nibh odio, sed vehicula mauris efficitur et. Vivamus sed consequat nunc. Donec ac tempor ipsum, vel porta nunc. Sed eros ipsum, bibendum sed bibendum sit amet, aliquam eget odio. Proin a turpis sit amet turpis tempus ullamcorper. Cras fermentum nunc id lorem faucibus, ultrices viverra tellus molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sapien tellus, luctus nec tempor vel, tincidunt eget massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec in nibh nec enim tincidunt sollicitudin. Nullam lobortis felis et nunc sollicitudin sodales. Cras eget risus efficitur neque semper malesuada. Vestibulum volutpat neque mi, non sollicitudin turpis facilisis at. Nam gravida sagittis gravida. In placerat blandit risus, ac convallis mi hendrerit ac. Etiam auctor auctor orci sit amet tempus. Etiam varius ut ipsum non blandit. Donec efficitur finibus posuere. Donec a ornare justo. Proin malesuada interdum libero nec condimentum. Duis elit enim, feugiat ac euismod sagittis, interdum vel lorem. Sed vehicula pretium tellus eget pellentesque.

Quisque laoreet ultrices nisl ornare accumsan. Praesent accumsan ut nisl volutpat pulvinar. In pretium mauris sed bibendum pellentesque. Suspendisse eget justo tempor, suscipit ante in, imperdiet eros. Morbi euismod nibh odio, sed vehicula mauris efficitur et. Vivamus sed consequat nunc. Donec ac tempor ipsum, vel porta nunc. Sed eros ipsum, bibendum sed bibendum sit amet, aliquam eget odio. Proin a turpis sit amet turpis tempus ullamcorper. Cras fermentum nunc id lorem faucibus, ultrices viverra tellus molestie.
