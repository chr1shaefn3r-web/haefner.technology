---
title: Raspberry Pi Rack
categories: Anleitung
tags:
 - RaspberryPi
 - sixinch
 - Rack
 - MiniCloud
 - Season2
 - Selfmade
date: 2018/09/01 13:37:00
cover_index: /images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-cover-index.jpg
amazon: true
sitemap: true
---
Welcher geneigte Informatiker hat sich nicht schonmal ein Raspberry Pi Projekt mit mehreren Pis vorgenommen... F&uuml;r das stilechte Unterbringen in einem (mini) Server-Rack sorgen wir in dieser Anleitung.
<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-fertig.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-fertig.jpg" alt="Raspberry Pi Rack fertig aufgebaut" /></a></span></div>
	</div>
</div>

Gesamtkosten: nur das Gestell (ohne Versandkosten): ca 45 €
Vorkenntnisse: Keine
Zeitaufwand: ca. 4 h

## Druckteile

 * 6x [Rack u14 Schraubenleiste](/druckmodelle/anleitung/raspberry-pi-rack/enclosure-h-nutless-trap-u14.stl)
 * 2x [Rack Panel mit Griffen](/druckmodelle/anleitung/raspberry-pi-rack/enclosure-j-panel-with-handle.stl)
 * 1x [Rack Deckel](/druckmodelle/anleitung/raspberry-pi-rack/enclosure-e-end-grid.stl), [Rack Boden](/druckmodelle/anleitung/raspberry-pi-rack/enclosure-f-end-chimney.stl)
 * 12x [Griffe](/druckmodelle/anleitung/raspberry-pi-rack/R3_Handles.stl)
 * 1x [Netgear Geh&auml;use](/druckmodelle/anleitung/raspberry-pi-rack/GS_Cabinet.stl), [Frontplatte](/druckmodelle/anleitung/raspberry-pi-rack/GS_Front.stl), [R&uuml;ckplatte](/druckmodelle/anleitung/raspberry-pi-rack/GS_Rear.stl), [Deckel](/druckmodelle/anleitung/raspberry-pi-rack/GS_Lid.stl)
 * 1x [Anker Geh&auml;use](/druckmodelle/anleitung/raspberry-pi-rack/A5_Cabinet.stl), [Frontplatte](/druckmodelle/anleitung/raspberry-pi-rack/A5_Front.stl), [R&uuml;ckplatte](/druckmodelle/anleitung/raspberry-pi-rack/A5_Rear.stl), [Deckel](/druckmodelle/anleitung/raspberry-pi-rack/A5_Lid.stl)
 * 4x [Raspberry Pi Geh&auml;use](/druckmodelle/anleitung/raspberry-pi-rack/R3_Cabinet.stl), [Frontplatte](/druckmodelle/anleitung/raspberry-pi-rack/R3_Front.stl), [R&uuml;ckplatte](/druckmodelle/anleitung/raspberry-pi-rack/R3_Rear.stl), [Deckel](/druckmodelle/anleitung/raspberry-pi-rack/R3_Lid.stl)

<a href="https://www.3dhubs.com/service/83913" data-3dhubs-widget="button" data-hub-id="83913" data-type="orderWidget" data-color="light" data-size="large" data-text="via 3D-Hubs bei uns bestellen">via 3D-Hubs bei uns bestellen</a>
<script>!function(a,b,c,d){var e,g=(a.getElementsByTagName(b)[0],/^http:/.test(a.location)?"http":"https");a.getElementById(d)||(e=a.createElement(b),e.id=d,e.src=g+"://d3d4ig4df637nj.cloudfront.net/w/2.0.js",e.async=!0,a.body.appendChild(e))}(document,"script",1,"h3d-widgets-js");</script>

Alle Modelle basieren auf der Arbeit von [KronBjorn](https://www.thingiverse.com/KronBjorn), der all seinen [OpenSCAD](https://de.wikipedia.org/wiki/OpenSCAD) Code in seinem [SixInchRack-Repo](https://github.com/KronBjorn/SixInchRack) teilt. Schaut auf jeden Fall auch bei seinem Tag '[sixinch](https://www.thingiverse.com/tag:sixinch)' vorbei um die ganze Welt seiner Rack-Einsch&uuml;be kennen zu lernen.
Alles was man f&uuml;r dieses Projekt braucht stellen wir in unserem [raspberryPiRack-Repo](https://gitlab.christophhaefner.de/3dprintingmodels/raspberryPiRack) kostenlos und frei zur Verf&uuml;gung.

## Rack

Wir beginnen mit dem Aufbau des eigentlichen Racks.

### Alu-Profile nacharbeiten

Die Alu-Profile haben kein Gewinde und die Schnittkanten haben noch einen Grat, wodurch sie unangenehm Scharfkantig sind. Gegen den Grat reicht es, mit einer kleinen Feile die Schnittkanten nachzubearbeiten.

Das Innengewinde schneiden wir mit dem [M6 Gewindeschneider](https://www.amazon.de/gp/product/B003U0KTO4/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B003U0KTO4&linkId=ea327ad733a578f58a719722dbe01c06) und [Windeisen](https://www.amazon.de/gp/product/B00200XB9U/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B00200XB9U&linkId=1822e6d2c1e0a644bf30a53705d00de1). Wichtig beim Gewindeschneiden ist das gerade Ansetzen. Zugegebnerma&szlig;en nicht ganz leicht, wird aber mit etwas &Uuml;bung immer besser. Au&szlig;erdem gilt es zu beachten, nicht auf einmal durchzuschneiden sondern immer wieder nach ein bis anderthalb Umdrehungen auch eine viertel Umdrehung zur&uuml;ck zu machen um den Span zu brechen. Ansonsten lauft ihr Gefahr, dass sich der Gewindeschneider fest frisst und ihr ihn kaum mehr wieder rausbekommt. Auch schadet es nicht, mit 1-2 Tropfen Ballistol (oder ähnlichem Öl) zu schmieren.

Wenn ihr beides geschafft habt sehen die Aluprofile wie folgt aus:
<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-alu-profil-vorher.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-alu-profil-vorher_small.jpg" alt="Alu Profil Vorher" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-alu-profil-nachher.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-alu-profil-nachher_small.jpg" alt="Alu Profil Nachher" /></a></span></div>
	</div>
</div>
(Ich habe eine relativ grobe Feile benutzt um den Grat zu entfernen. Dadurch sehen die Kanten zwar etwas vermackt aus, aber die Schnittkante f&uuml;hlt sich trotzdem deutlich besser an)

### Aufbau

Nachdem die Alu-Profile fertig sind, ist es nun ein Leichtes, das eigentliche Rack aufzubauen.
Es m&uuml;ssen lediglich die Lochleisten eingef&uuml;hrt, die Panels links und rechts (mit [Zylinderkopfschrauben M4x8](https://www.amazon.de/gp/product/B018XL85JK/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B018XL85JK&linkId=b3595ef63b8d64968ec2e3e3ee44f07c)), sowie Boden und Deckel (mit [Senkkopfschrauben M6x12](https://www.amazon.de/gp/product/B06W5KTKNP/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B06W5KTKNP&linkId=99de325da0d3042ec86cbca8cf52f5f4)) angeschraubt werden.
<div class="box alt">
	<div class="row uniform">
		<div class="1u"></div>
		<div class="10u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-aufgebaut.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-aufgebaut_small.jpg" alt="Rack aufgebaut" /></a></span></div>
		<div class="1u$"></div>
	</div>
	<div class="row uniform">
		<div class="2u"></div><div class="2u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-alu-profil-mit-lochleisten.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-alu-profil-mit-lochleisten_small.jpg" alt="Alu Profil mit Lochleisten" /></a></span></div><div class="2u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-alu-profil-mit-lochleisten_closeup.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-alu-profil-mit-lochleisten_closeup_small.jpg" alt="Alu Profil mit Lochleisten Nahaufnahme" /></a></span></div><div class="2u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-alu-profil-mit-panel.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-alu-profil-mit-panel_small.jpg" alt="Alu Profile mit Panel" /></a></span></div><div class="2u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-alu-profil-mit-panel_closeup.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-alu-profil-mit-panel_closeup_small.jpg" alt="Alu Profile mit Panel Nahaufnahme" /></a></span></div><div class="2u$"></div>
	</div>
</div>

## Raspberry Pis

Beim Zusammenbau der Einsch&uuml;be fangen wir mit den eigentlichen Raspberry Pis an.

### Front- und R&uuml;ckplatte

Wenn man will kann man hier mehrfarbige Frontplatten drucken - in passenden Farben zu den mehrfarbigen LAN-Kabeln aus unserer Materialliste.
Da beim 3D-Druck dann doch meist keine perfekte Fl&auml;che rauskommt, nutzen wir hier Schleifpapier (K&ouml;rnung: ~320 oder weniger) und matte Spr&uuml;hfarbe um die Frontplatten zu veredeln.

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-frontplatte-original.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-frontplatte-original_small.jpg" alt="Frontplatte Original" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-frontplatte-matt.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-frontplatte-matt_small.jpg" alt="Frontplatte Matt" /></a></span></div>
	</div>
</div>

Nun k&ouml;nnen die Griffe angeklebt werden. Hierbei gilt es darauf zu achten, m&ouml;glichst nicht zu viel [Sekundenkleber](https://www.amazon.de/gp/product/B001IVZMBM/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B001IVZMBM&linkId=09de605348ac23a1ab0aa7d597088677) aufzutragen, damit es nicht auf allen Seiten herauskommt. Als Richtwert: Wenn man denkt es ist zu wenig, ist es wahrscheinlich gerade richtig.

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-frontplatten-mit-griffen.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-frontplatten-mit-griffen_small.jpg" alt="Frontplaten mit Griffen" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-frontplatte-mit-griff.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-frontplatte-mit-griff_small.jpg" alt="Frontplatte mit Griff Nahaufnahme" /></a></span></div>
	</div>
</div>

Auch die R&uuml;ckplatte wird festgeklebt und auch hier sollte man auf ein dezentes Auftragen achten.

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-rueckplatte.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-rueckplatte_small.jpg" alt="Gehäuse mit Rückplatte" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-rueckplatte_closeup.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-rueckplatte_closeup_small.jpg" alt="Gehäuse mit Rückplatte Nahaufnahme" /></a></span></div>
	</div>
</div>

### Pi einlegen und zusammenschrauben

Den [Raspberry Pi](https://www.amazon.de/gp/product/B07BDR5PDW/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B07BDR5PDW&linkId=9d22742c94a97c146be379079200cbfe) kann man in diesem Modell einfach einlegen und einklipsen. Nun kann das Geh&auml;use zugeschraubt werden. Hierzu nutzen wir die [Spax Schrauben 3,5x16](https://www.amazon.de/gp/product/B00MPIFOJW/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B00MPIFOJW&linkId=5771f5ca6ccb49c330a1f44168aeeb82). Denkt daran, die richtige L&auml;nge an Micro-USB-Kabel einzustecken bevor ihr den Deckel drauf schraubt. F&uuml;r die beiden oberen Kabel nutze ich hier welche mit 0,9&nbsp;m, f&uuml;r die beiden unteren mit 0,3&nbsp;m Länge.

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-pi-offen.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-pi-offen_small.jpg" alt="Raspberry Pi Gehäuse offen" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-pi-geschlossen.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-pi-geschlossen_small.jpg" alt="Raspberry Pi Gehäuse geschlossen" /></a></span></div>
	</div>
</div>

## Stromversorgung (Anker PowerPort)

Weiter gehts mit der Stromversorgung und dem Anker Einschub. Entsprechend dem Pi-Geh&auml;use werden die Griffe an die Fronplatte geklebt und die R&uuml;ckplatte an das Geh&auml;use. Danach muss der [Anker Power Port 5](https://www.amazon.de/gp/product/B00VUGOSWY/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B00VUGOSWY&linkId=a20f6ae83684c36f8e18d3d3fd356c8e) nurnoch eingelegt und das Geh&auml;use zugeschraubt werden.

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-anker-offen.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-anker-offen_small.jpg" alt="Anker Gehäuse offen" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-anker-geschlossen.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-anker-geschlossen_small.jpg" alt="Anker Gehäuse geschlossen" /></a></span></div>
	</div>
</div>

## Switch (Netgear)

Auch das Switch-Geh&auml;use wird &auml;hnlich der vorherigen Geh&auml;use zusammengebaut: Die Griffe an die Fronplatte und die R&uuml;ckplatte an das Geh&auml;use kleben. Danach den [Netgear Switch](https://www.amazon.de/gp/product/B0000X5IQ8/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B0000X5IQ8&linkId=afee0f4df990da3b63eef1ed0d931bab) einlegen und das Geh&auml;use zuschrauben.

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-netgear-offen.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-netgear-offen_small.jpg" alt="Netgear Switch Gehäuse offen" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-netgear-geschlossen.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-netgear-geschlossen_small.jpg" alt="Netgear Switch Gehäuse geschlossen" /></a></span></div>
	</div>
</div>

## Einlegen und Festschrauben

Alle Einsch&uuml;be sind nun fertig und k&ouml;nnen ins Rack eingeschraubt werden. Am besten funktioniert das Einschrauben, wenn man die Schrauben schr&auml;g ansetzt, dann greifen sie besser. Die Schraubenl&ouml;cher haben noch kein Gewinde, die Schrauben schneiden sich selbst ein Gewinde in den Kunststoff, daher geht es auch recht schwer zu schrauben.
Ich habe die Einsch&uuml;be bereits f&uuml;r meinen Anwendungsfall angeordnet. Ihr k&ouml;nnt sie nat&uuml;rlich in beliebiger Reihenfolge einschrauben.

Ich empfehle, Netzwerk und Strom immer unten einzulegen. Diese sind die schwersten Teile und mit einem m&ouml;glichst tiefen Schwerpunkt steht das Rack am stabilsten.

<div class="box alt">
	<div class="row uniform">
		<div class="3u"></div><div class="6u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-eingeschraubt.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-eingeschraubt_small.jpg" alt="Raspberry Pi Rack Alle Einschübe eingeschraubt" /></a></span></div><div class="3u$"></div>
	</div>
</div>

### Verkabeln

Micro-USB- und LAN-Kabel einstecken und mit wahlweise Kabelbinder oder Anker-Klettverschluss alles sortieren. Ich habe die langen 0,9&nbsp;m Micro-USB-Kabel in Schlaufen gelegt und sowohl einzeln als auch nochmal im Paket festgezurrt. Wahlweise kann man die LAN-Kabel auch nochmal zusammenbinden. Allerdings sind die auch schon so recht stabil und aufger&auml;umt.

<div class="box alt">
	<div class="row uniform">
		<div class="3u"></div><div class="6u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-verkabelt.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-verkabelt_small.jpg" alt="Raspberry Pi Rack Alle Einschübe verkabelt" /></a></span></div><div class="3u$"></div>
	</div>
</div>

## Raspberry Pi Rack fertig

<div class="box alt">
	<div class="row uniform">
		<div class="1u"></div>
		<div class="10u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-fertig.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-fertig_small.jpg" alt="Raspberry Pi Rack fertig aufgebaut" /></a></span></div>
		<div class="1u$"></div>
	</div>
	<div class="row uniform">
		<div class="2u"></div><div class="2u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-fertig-pis-nahaufname.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-fertig-pis-nahaufname_small.jpg" alt="Raspberry Pi Rack fertig Pis Nahaufnahme" /></a></span></div><div class="2u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-fertig-verkabelung-nahaufname.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-fertig-verkabelung-nahaufname_small.jpg" alt="Raspberry Pi Rack fertig Verkabelung Nahaufnahme" /></a></span></div><div class="2u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-fertig-ecke-nahaufname.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-fertig-ecke-nahaufname_small.jpg" alt="Raspberry Pi Rack fertig Ecke Nahaufnahme" /></a></span></div><div class="2u"><span class="image fit"><a href="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-fertig-switch-kabel-nahaufname.jpg"><img src="/images/anleitung/raspberry-pi-rack/haefner-technology-raspberry-pi-rack-fertig-switch-kabel-nahaufname_small.jpg" alt="Raspberry Pi Rack fertig Switch Nahaufnahme" /></a></span></div><div class="2u$"></div>
	</div>
</div>

## Materialien

 * 4x [Raspberry Pi](https://www.amazon.de/gp/product/B07BDR5PDW/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B07BDR5PDW&linkId=9d22742c94a97c146be379079200cbfe)
 * 4x [Speicherkarte](https://www.amazon.de/gp/product/B073S8LQSL/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B073S8LQSL&linkId=7c43f59a6d3f08da0111c95e49e98420)
 * 1x [Anker Power Port 5](https://www.amazon.de/gp/product/B00VUGOSWY/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B00VUGOSWY&linkId=a20f6ae83684c36f8e18d3d3fd356c8e)
 * 1x [Netgear GS105E-200](https://www.amazon.de/gp/product/B0000X5IQ8/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B0000X5IQ8&linkId=afee0f4df990da3b63eef1ed0d931bab)
 * 1x [Mehrfarbige LAN-Kabel](https://www.amazon.de/gp/product/B01J8KFTB2/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B01J8KFTB2&linkId=851ac37506149a170e4f233ed2e91a82)
 * 1x [Micro-USB Kabel Set](https://www.amazon.de/gp/product/B016BFB24A/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B016BFB24A&linkId=b98c9fa7560df961dd93475abf7a9aff)
 * 1x [Zylinderkopfschrauben M4x8](https://www.amazon.de/gp/product/B018XL85JK/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B018XL85JK&linkId=b3595ef63b8d64968ec2e3e3ee44f07c)
 * 1x [Senkkopfschrauben M6x12](https://www.amazon.de/gp/product/B06W5KTKNP/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B06W5KTKNP&linkId=99de325da0d3042ec86cbca8cf52f5f4)
 * 1x [Spax Schrauben 3,5x16](https://www.amazon.de/gp/product/B00MPIFOJW/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B00MPIFOJW&linkId=5771f5ca6ccb49c330a1f44168aeeb82)
 * 1x [M6 Gewindeschneider](https://www.amazon.de/gp/product/B003U0KTO4/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B003U0KTO4&linkId=ea327ad733a578f58a719722dbe01c06)
 * 1x [Ruko Windeisen](https://www.amazon.de/gp/product/B00200XB9U/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B00200XB9U&linkId=1822e6d2c1e0a644bf30a53705d00de1)
 * [Sekundenkleber](https://www.amazon.de/gp/product/B001IVZMBM/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B001IVZMBM&linkId=09de605348ac23a1ab0aa7d597088677)

## Benötigtes Werkzeug

 * [Schraubendreher TX20](https://www.amazon.de/gp/product/B0001P17WY/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B0001P17WY&linkId=9082b38980d652fdb824af6ce564e847)
 * [Inbus-Sechskant](https://www.amazon.de/gp/product/B009ODV0OE/ref=as_li_tl?ie=UTF8&tag=raspberrypirack-21&camp=1638&creative=6742&linkCode=as2&creativeASIN=B009ODV0OE&linkId=0d31eba0b1ae8d72646bf66abe3d7a2b), Gr&ouml;&szlig;e 3 und 4 wird ben&ouml;tigt

