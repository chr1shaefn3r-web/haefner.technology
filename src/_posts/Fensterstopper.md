---
title: Fensterstopper
categories: Anleitung
tags:
 - Holdthewindow
 - Summer
 - Heat
 - Window
 - Selfmade
 - Season1
date: 2018/05/01 18:00:00
cover_index: /images/anleitung/fensterstopper/haefner-technology-fensterstopper-cover-index.jpg
amazon: true
sitemap: true
---
Herzlich Willkommen zur Anleitung für euren individuellen Fensterstopper!
Mit einem kleinen Trick wird aus einem schnöden Plastikteil ein echt stabiler Fensterstopper. So kann das dann in Aktion aussehen:

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-cover.jpg"><img src="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-cover_small.jpg" alt="Fensterstopper im Aktion" /></a></span></div>
	</div>
</div>

Der Clou sind die zwei Schichten aus Silikon, die als Anti-Rutsch-Auflage dienen. Diese unterstützen die keilförmige Konstruktion, die das Fenster mühelos exakt in Position hält. Außerdem ist der Fensterstopper flach genug um unter dem Fenster durch zu passen, solange er nicht benötigt wird. (Abstand Sims-Fenster min. 3&nbsp;cm)

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-antislip.jpg"><img src="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-antislip_small.jpg" alt="Anti-Rutsch-Schicht" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-flat.jpg"><img src="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-flat_small.jpg" alt="Passt auch unterm Fenster durch" /></a></span></div>
	</div>
</div>

## Vorbereitung

 * Abstand zwischen Fensterrahmen und Fenstersims messen (auf ca. &plusmn;1&nbsp;mm genau)
 * Ausrechnen, welche Zwischenteile benötigt werden

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u$"><span class="image fit"><a href="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-measure.jpg"><img src="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-measure_small.jpg" alt="Messung mit Stahllineal oder Messchieber" /></a></span></div>
		<div class="2u"></div>
	</div>
</div>

Für solcherlei Messungen eignet sich am besten ein [Stahllineal](https://www.amazon.de/gp/product/B005DKMMSU/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=hairy-lion-21&creative=6742&linkCode=as2&creativeASIN=B005DKMMSU&linkId=02fa3888769028a0faa1ae207f37bd43), weil einfach auf Anschlag gemessen werden kann. Auch ein Messchieber eignet sich recht gut. Allerdings ist es sinnvoll, an genau der Stelle zu messen, wo nachher der Fensterstopper liegen soll - viele Fenstersimse sind etwas schief (sowohl vorne-hinten als auch rechts-links).
Vom gemessenen Abstand werden je 8&nbsp;mm für Ober- und Unterteil abgezogen. Der Rest muss - in möglichst großen Stücken - aus folgenden Maßen zusammen kombiniert werden: 1&nbsp;mm, 2&nbsp;mm, 4&nbsp;mm, 8&nbsp;mm, 16&nbsp;mm, 24&nbsp;mm, 32&nbsp;mm

In unserem Beispiel haben wir 48&nbsp;mm gemessen. Nach Abzug der 16&nbsp;mm für Ober- und Unterteil bleiben 32&nbsp;mm. Dieses Maß ist zufälligerweise sogar exakt verfügbar! Ansonsten könnte es z.B. mit 24&nbsp;mm + 4&nbsp;mm + 4&nbsp;mm zusammen kombiniert werden. Bei solchen Kombinationen sollte ein möglichst großes Maß in der Mitte und über- und unterhalb möglichst symmetrische Maße gewählt werden (_nicht_ 24&nbsp;mm + 8&nbsp;mm in unserem Beispiel).

## Druckteile

 * 2x [Ober-/Unterteil](/druckmodelle/anleitung/fensterstopper/ObenUnten8mm.stl)
 * 2x [Schablone](/druckmodelle/anleitung/fensterstopper/Schablone.stl)
 * min. 1x Zwischenteil ([1&nbsp;mm](/druckmodelle/anleitung/fensterstopper/Zw1mm.stl), [2&nbsp;mm](/druckmodelle/anleitung/fensterstopper/Zw2mm.stl), [4&nbsp;mm](/druckmodelle/anleitung/fensterstopper/Zw4mm.stl), [8&nbsp;mm](/druckmodelle/anleitung/fensterstopper/Zw8mm.stl), [16&nbsp;mm](/druckmodelle/anleitung/fensterstopper/Zw16mm.stl), [24&nbsp;mm](/druckmodelle/anleitung/fensterstopper/Zw24mm.stl), [32&nbsp;mm](/druckmodelle/anleitung/fensterstopper/Zw32mm.stl))

## Zusammenbau und Schablone

Der Zusammenbau ist an sich schnell erledigt: Einfach zusammen stecken.
Lange Zapfen machen beim 3D-Druck hin und wieder Probleme. Sollte ein Zapfen zu sehr verformt sein und deshalb klemmen, bearbeitet diesen einfach so lange mit Messer, Feile oder einem ähnlichen Werkzeug, bis es passt. Im schlimmsten Fall sitzt das Zwischenteil nachher etwas wackelig. Das kann durch etwas Klebstoff ganz zum Schluss leicht behoben werden (zuerst fertigstellen und die Höhe kontrollieren!).

Die zwei Schablonen sollten auf beiden Seiten angedrückt werden und von alleine klemmen. Falls sie das nicht tun muss mit Klebeband nachgeholfen werden.
Achtung: Die Schablonen sind nicht symmetrisch, also auch nicht beliebig verdrehbar.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u$"><span class="image fit"><a href="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-stencil.jpg"><img src="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-stencil_small.jpg" alt="Ausrichtung der Schablone" /></a></span></div>
		<div class="2u"></div>
	</div>
</div>

<div class="box alt">
	<div class="row uniform">
		<div class="4u"><span class="image fit"><a href="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-assembly1.jpg"><img src="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-assembly1_small.jpg" alt="Zusammenbau" /></a></span></div>
		<div class="4u"><span class="image fit"><a href="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-assembly2.jpg"><img src="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-assembly2_small.jpg" alt="Schablonen Ansatz" /></a></span></div>
		<div class="4u$"><span class="image fit"><a href="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-assembly3.jpg"><img src="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-assembly3_small.jpg" alt="Zusammenbau mit Schablonen" /></a></span></div>
	</div>
</div>

## Anti-Rutsch-Schicht aus Silikon

Zuerst sollte die Arbeitsfläche mit einer Unterlage geschützt werden. Silikon-Flecken sind nicht gerade angenehm...

Dann wird der Hohlraum in der Schablone mit gewöhnlichem Sanitärsilikon gefüllt. Die Nuten im Ober-/Unterteil verbessern die Haftung des Silikons am Kunststoffteil. Achtet darauf, diese Nuten gut auszufüllen. Überflüssiges Silikon kann im Anschluss leicht entfernt werden, sparen ist hier also nicht angesagt.

Wenn der Hohlraum gut gefüllt ist, kann das überflüssige Silikon mit einem Gegenstand mit harter Kante (z.B. das Stahllineal vom Anfang) entlang der Schablone abgezogen werden. Danach sollte das Silikon vollständig durchhärten (üblich: ca. 1 Tag).

<div class="box alt">
	<div class="row uniform">
		<div class="1u"></div>
		<div class="2u"><span class="image fit"><a href="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-silicone1.jpg"><img src="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-silicone1_small.jpg" alt="Vorbereitung zu den Silokonarbeiten" /></a></span></div>
		<div class="2u"><span class="image fit"><a href="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-silicone2.jpg"><img src="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-silicone2_small.jpg" alt="Silikon aufgetragen" /></a></span></div>
		<div class="2u"><span class="image fit"><a href="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-silicone3.jpg"><img src="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-silicone3_small.jpg" alt="Bereit zum Silikon abziehen" /></a></span></div>
		<div class="2u"><span class="image fit"><a href="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-silicone4.jpg"><img src="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-silicone4_small.jpg" alt="Silokon glatt abgezogen" /></a></span></div>
		<div class="2u$"><span class="image fit"><a href="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-silicone5.jpg"><img src="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-silicone5_small.jpg" alt="Silokon durchtrocknen lassen" /></a></span></div>
		<div class="1u"></div>
	</div>
</div>

## Schablonen entfernen

Das Entfernen der Schablonen funktioniert ganz einfach: Einmal am Rand entlang einritzen (Messer oder langer Fingernagel) und Schablone abziehen.

Fertig ist der Fensterstopper!

<div class="box alt">
	<div class="row uniform">
		<div class="6u"><span class="image fit"><a href="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-cutter.jpg"><img src="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-cutter_small.jpg" alt="Schablone entfernen" /></a></span></div>
		<div class="6u$"><span class="image fit"><a href="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-antislip.jpg"><img src="/images/anleitung/fensterstopper/haefner-technology-fensterstopper-antislip_small.jpg" alt="Fertiger Fensterstopper" /></a></span></div>
	</div>
</div>

## Hinweise

Die Schablonen können natürlich wiederverwendet werden. Wir empfehlen trotzdem mindestens zwei Schablonen zu drucken, damit man wenigstens einen ganzen Fensterstopper (Ober- und Unterteil) in einem Arbeitsgang machen kann.

Das Silikon hat eine starke Anti-Rutsch-Wirkung. Dadurch kann das Fenster mit sehr wenig Kraft verklemmt werden.
Tipp: Fenster in gewünschte Position bringen (muss ja nicht unbedingt ganz offen sein!) und Fensterstopper quasi ohne Kraft darunter schieben. Fertig!

## Benötigte Materialien

 * Druckteile (siehe oben)
 * Sanitärsilikon, z.B. [Anthrazit](https://www.amazon.de/gp/product/B0166VOOH6/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=hairy-lion-21&creative=6742&linkCode=as2&creativeASIN=B0166VOOH6&linkId=e255c6b26fa3b27db25f3499116ed139)
 * Unterlage aus Zeitung, Altpapier, Küchenrolle, o.Ä.

## Benötigtes Werkzeug

 * [Stahllineal](https://www.amazon.de/gp/product/B005DKMMSU/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=hairy-lion-21&creative=6742&linkCode=as2&creativeASIN=B005DKMMSU&linkId=02fa3888769028a0faa1ae207f37bd43)
 * [Silikon-Kartuschenpresse](https://www.amazon.de/gp/product/B013SW1EO2/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=hairy-lion-21&creative=6742&linkCode=as2&creativeASIN=B013SW1EO2&linkId=48583a5260f8959c54e2f4e181a5a13e)
 * [Cutter Messer](https://www.amazon.de/gp/product/B00BJGE9Z6/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=hairy-lion-21&creative=6742&linkCode=as2&creativeASIN=B00BJGE9Z6&linkId=6ff6d8d6df75f5716d73f8011a52b0e0)
 


