---
title: Filament-Spulen-Sortierbox
categories: Anleitung
tags:
 - Sortierbox
 - Ordnung
 - Season2
 - Selfmade
date: 2018/12/01 13:37:42
cover_index: /images/anleitung/filament-spulen-sortierbox/haefner-technology-sortierbox-cover-index.jpg
amazon: true
sitemap: true
---
Wer kennt nicht das Sprichtwort: 'Ordnung ist das halbe Leben!' Gerade für Kleinzeugs und 'Kruscht' fehlen einem manchmal die Ablagefächer.
Anlehnend an das Design '[Bumblebee Spool Storage System (remix)](https://www.thingiverse.com/thing:2624537)' von MakerBeck haben wir heute eine kurze Anleitung, um eure Ordnung zu verbessern.

<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/filament-spulen-sortierbox/haefner-technology-sortierbox-00Titelbild.jpg"><img src="/images/anleitung/filament-spulen-sortierbox/haefner-technology-sortierbox-00Titelbild_small.jpg" alt="So kann die Sortierbox zum Schluss aussehen" /></a></span></div>
	</div>
</div>

Gesamtkosten: ca. 10€ pro Box-Ebene
Vorkenntnisse: bohren, kleben
Zeitaufwand: ca. 2h für eine 4-Ebenen-Box

## Druckteile

Sobald ihr die Druckteile vorliegen habt könnt ihr beginnen.

 * 4x [Box](/druckmodelle/anleitung/filament-spulen-sortierbox/Ablagefach_klein_M4.stl)
 * 1x [Bohrhilfe](/druckmodelle/anleitung/filament-spulen-sortierbox/Bohrhilfe_klein_M4.stl)
 * Restliche Teile des '[Bumblebee Spool Storage System](https://www.thingiverse.com/thing:2624537)'

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/filament-spulen-sortierbox/haefner-technology-sortierbox-01Materialien.jpg"><img src="/images/anleitung/filament-spulen-sortierbox/haefner-technology-sortierbox-01Materialien_small.jpg" alt="Übersicht der Materialien" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

## Start

Zuerst müssen die Spule(n) gebohrt werden. Dazu gibt es die Bohrschablone. Sucht euch ein vorhandenes Loch am Außenrand der Spule und bohrt dann unter 90° das nächste Loch. Dann setzt ihr die Bohrschablone auf das neue Loch und bohrt erneut. Diesen Prozess erneut wiederholen und ihr habt 4 Löcher, die gleichmäßig am Umfang verteilt sind.
Nun müsst ihr das gleiche auf der anderen Seite wiederholen. Achtung: Die gegenüberliegenden Löcher sollten ziemlich deckungsgleich sein! Deshalb kann man auch die Löcher auf einer Seite mit der Schablone bohren und dann diese Löcher als Ausrichtungshilfe für die gegenüberliegenden verwenden.
Die Löcher in jedem Fall entgraten!

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/filament-spulen-sortierbox/haefner-technology-sortierbox-02Bohrschablone.jpg"><img src="/images/anleitung/filament-spulen-sortierbox/haefner-technology-sortierbox-02Bohrschablone_small.jpg" alt="Bohrschablone" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>


## Boxen montieren

Je nach Spulenhöhe ist die Montage der Boxen eine einfach oder zeitintensive Sache. Im Prinzip muss man diese nur mit Schrauben und Muttern sichern. Ist eure Spule jedoch höher als eine Standardschraube, so solltet ihr die Schrauben entsprechend kürzen, damit diese nicht weiter als die Mutter überstehen.

<div class="box alt">
	<div class="row uniform">
		<div class="1u"></div>
		<div class="5u"><span class="image fit"><a href="/images/anleitung/filament-spulen-sortierbox/haefner-technology-sortierbox-03VerschraubungMutter.jpg"><img src="/images/anleitung/filament-spulen-sortierbox/haefner-technology-sortierbox-03VerschraubungMutter_small.jpg" alt="Verschraubung Mutter" /></a></span></div>
		<div class="5u"><span class="image fit"><a href="/images/anleitung/filament-spulen-sortierbox/haefner-technology-sortierbox-04VerschraubungSchraube.jpg"><img src="/images/anleitung/filament-spulen-sortierbox/haefner-technology-sortierbox-04VerschraubungSchraube_small.jpg" alt="Verschraubung Schraubenkopf" /></a></span></div>
		<div class="1u$"></div>
	</div>
</div>


Übrigens:
Es gibt auch die Möglichkeit mehr oder weniger als 4 Boxen pro Ebene zu verbauen! Und wir können die Boxen auch auf eure individuellen Spulen anpassen.

Kontakiert uns einfach unter: <a href="mailto:info@haefner.technology">info@haefner.technology</a>


## Füße ankleben

Damit euer Sortierboxen-Stapel später auch sicher steht kleben wir die Füße einfach unter 120° mit Sekundenkleber an der untersten Box fest.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/filament-spulen-sortierbox/haefner-technology-sortierbox-05Fuesse.jpg"><img src="/images/anleitung/filament-spulen-sortierbox/haefner-technology-sortierbox-05Fuesse_small.jpg" alt="Füße ankleben" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>


## Endmontage

Für die Endmontage geht ihr folgendermaßen vor:
Zuerst klebt ihr EINE Mutter an das Ende der Gewindestange. Dann beginnt ihr mit der untersten Zentrierscheibe, die einen Sechskant-Tasche für die Mutter hat. Darauf plaziert ihr die erste Box. Nun einfach weitere Zentrierungen und Boxen übereinander stapeln.
Den Griff lose aufsetzen und markieren, wo ihr die Gewindestange absägen wollt. Oder ihr lasst sie lang wie in unserem Beispiel, da wir den Stapel in der Zukunft noch vergrößern wollen.

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/filament-spulen-sortierbox/haefner-technology-sortierbox-06Verbindungsstueck.jpg"><img src="/images/anleitung/filament-spulen-sortierbox/haefner-technology-sortierbox-06Verbindungsstueck_small.jpg" alt="Zentrierung" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Zum Abschluss eine weitere Mutter mit Hilfe des Griffs einschrauben.
Und schon könnt ihr den ganzen Stapel rumtragen und verstauen.


<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/haefner-technology-sortierbox-00Titelbild.jpg"><img src="/images/anleitung/filament-spulen-sortierbox/haefner-technology-sortierbox-00Titelbild_small.jpg" alt="Sortierbox montiert" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>


## Materialien

 * [Sekundenkleber](https://www.amazon.de/gp/product/B004T2M0BC/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=filament-spulen-sortierbox-21&creative=6742&linkCode=as2&creativeASIN=B004T2M0BC&linkId=5064c1489c390cd4023be86dea4b6566)
 * [M6 Muttern](https://www.amazon.de/gp/product/B005HVYDZU/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=filament-spulen-sortierbox-21&creative=6742&linkCode=as2&creativeASIN=B005HVYDZU&linkId=c72229ed75107069fbbd570d2b456eab)
 * [Gewindestange](https://www.amazon.de/gp/product/B07GD62H42/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=filament-spulen-sortierbox-21&creative=6742&linkCode=as2&creativeASIN=B07GD62H42&linkId=adfcadc0cf0e00252a9b6143abb1bfa9)

 
 
## Benötigtes Werkzeug/Hilfsmittel

 * Bohrer Ø4.5
 * Cuttermesser

