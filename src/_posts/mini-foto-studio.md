---
title: Fotostudio
categories: Anleitung
tags:
 - Fotostudio
 - Photostudio
 - Fotografie
 - Foto
 - Dokumentation
 - Season5
 - Selfmade
date: 2020/03/01 13:37:42
cover_index: /images/anleitung/mini-foto-studio/haefner-technology-fotostudio-cover-index.jpg
amazon: false
sitemap: true
---

Hintergrund und Belichtung sind das A und O bei der Fotografie. Beides ist vor allem - aber nicht nur - bei 3D-Modellen oft problematisch.
Abhilfe schafft ein Fotostudio: Die perfekte Hilfe beim Präsentieren und Dokumentieren!

<div class="box alt">
	<div class="row uniform">
		<div class="12u">
			<span class="image fit">
				<a href="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-00Titelbild.jpg">
					<img src="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-00Titelbild.jpg" alt="Das ferige Fotostudio" />
				</a>
			</span>
		</div>
	</div>
</div>

Gesamtkosten: ca. 20-35 €
Vorkenntnisse: Machbar ohne jegliche Kenntnisse.
Zeitaufwand: ca. 1,5 h

## Druckteile

Das Fotostudio besteht aus acht Druckteilen: 3 Teile und 1 Verbinder für eine Seite, die andere Seite einfach gespiegelt.

 * 2x [Seitenteil Oben](/druckmodelle/anleitung/fotostudio/Seitenteil_Oben.stl)
 * 2x [Seitenteil Unten](/druckmodelle/anleitung/fotostudio/Seitenteil_Unten.stl)
 * 2x [Seitenteil Fuss](/druckmodelle/anleitung/fotostudio/Seitenteil_Fuss.stl)
 * 2x [Seitenteil Verbinder](/druckmodelle/anleitung/fotostudio/Seitenteil_Verbinder.stl)

## Materialien

 * [Aluminiumblech 1mm, 30 x 50 cm](https://www.amazon.de/gp/product/B081RZJW1P/ref=as_li_tl?ie=UTF8&camp=1638&creative=6742&creativeASIN=B081RZJW1P&linkCode=as2&tag=fotostudio03-21&linkId=ccde93ea5d6b84609e872cfe38672456)
 * [LED Strip](https://www.amazon.de/gp/product/B07SR8YBT6/ref=as_li_tl?ie=UTF8&camp=1638&creative=6742&creativeASIN=B07SR8YBT6&linkCode=as2&tag=fotostudio03-21&linkId=5982f5ad573e83f17541fe492b30970a)
 * Weißes DIN A3 Blatt

## Aufbau - Seiten

Zuerst geht es an die Seitenteile. Beim Zusammenstecken gibt es nur zwei Kleinigkeiten zu beachten:
1) Linke und Rechte Seite nicht verwechseln
2) Das Ganze soll ordentlich klemmen, damit nachher die Stabilität gewährleistet ist. Im Zweifelsfall mit Schraubzwinge und/oder Gummihammer nachhelfen.

<div class="box alt"><div class="row uniform"><div class="12u"><span class="image fit">
		<a href="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-01seitenteil.jpg">
		<img src="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-01seitenteil_small.jpg" alt="E" /></a>
</span></div></div></div>

## Aufbau - Rückwand

Im Anschluss wird das Aluminiumblech für die Rückwand gebogen. Das geht am besten, indem man die gesamte Breite über eine Tischkante drückt oder mithilfe eines Nudelholzes oder eines sonstigen runden Teils.
Falls euer Blech mit einer Schutzfolie beklebt ist, lasst sie in diesem Schritt noch dran, um Kratzer zu vermeiden.
Nehmt euch hier ein bisschen Zeit und kontrolliert immer wieder, wo noch etwas fehlt. Bei kleinen Abweichungenkann man auch nur den Rand von Hand biegen.
Insgesamt ist es von Vorteil, wenn das Blech NICHT perfekt passt. Wenn es ein bisschen klemmt wird der gesamte Aufbau viel stabiler!

<div class="box alt"><div class="row uniform"><div class="4u"><span class="image fit">
		<a href="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-02nudelholz.jpg">
		<img src="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-02nudelholz_small.jpg" alt="Rundholz als Biegehilfe" /></a>
	</span></div><div class="4u"> <span class="image fit">
		<a href="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-03biegenanfang.jpg">
		<img src="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-03biegenanfang_small.jpg" alt="Deutliche Abweichung" /></a>
	</span></div><div class="4u"> <span class="image fit">
		<a href="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-04biegenende.jpg">
		<img src="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-04biegenende_small.jpg" alt="Der Rest gibt nach" /></a>
</span></div></div></div>

## Aufbau - LED Strips

Nun kommt der aufwendigste Teil: Das Kleben des LED Strips.
Falls vorhanden, zieht jetzt die Schutzfolie des Aluminiumbleches ab.
Fangt unten links oder rechts an, den LED Strip aufzukleben und dann in einer Art eckigen Spirale Richtung Mitte.
Lasst zwischen zwei Bahnen ca. 3 cm Abstand, dann reicht ein handelsüblicher LED Stip mit 5 m ziemlich genau für die Fläche eines DIN A3 Blattes.
Die Ecken funktionieren ganz gut, wenn man sie als 2x 45° Ecken ausführt. Die LEDs sollten möglichst immer aufgeklebt sein; die bloße Leiterbahn kann ruhig etwas abstehen.
Da das ganze nachher diffus auf ein DIN A3 Blatt leuchtet, müssen die Bahnen auch nicht genau gerade bzw. parallel zu den anderen sein.

<div class="box alt"><div class="row uniform"><div class="6u"><span class="image fit">
		<a href="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-05startundwinkel.jpg">
		<img src="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-05startundwinkel_small.jpg" alt="Start des LED Strips und Ecken" /></a>
	</span></div><div class="6u"> <span class="image fit">
		<a href="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-06ledsgesamt.jpg">
		<img src="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-06ledsgesamt_small.jpg" alt="Gesamte Spirale" /></a>
</span></div></div></div>

## Aufbau - Finish

Jetzt fehlt nur noch der Diffusor: Dazu ein DIN A3 Blatt auf beiden kurzen Seiten ca. 5 mm ganz locker einknicken und dann in die vorderen Nuten von oben her einschieben.
Der leichte Knick macht die Kanten etwas stabiler, so dass sie nicht durch hängen.

<div class="box alt"><div class="row uniform"><div class="6u"><span class="image fit">
		<a href="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-07knickimblatt.jpg">
		<img src="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-07knickimblatt_small.jpg" alt="Die kurzen Kanten knicken - aber nicht zu scharf" /></a>
	</span></div><div class="6u"> <span class="image fit">
		<a href="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-08blatteinschub.jpg">
		<img src="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-08blatteinschub_small.jpg" alt="Blatt einführen" /></a>
</span></div></div></div>

Und fertig ist der beleuchtete Fotohintergrund!

<div class="box alt"><div class="row uniform"><div class="12u"><span class="image fit">
		<a href="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-09fertig.jpg">
		<img src="/images/anleitung/mini-foto-studio/haefner-technology-fotostudio-09fertig_small.jpg" alt="Super Beleuchtung" /></a>
</span></div></div></div>
