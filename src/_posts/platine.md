---
title: Platine
categories: Anleitung
tags:
 - Elektronik
 - circuitboard
 - PCB
 - electronics
 - Season3
 - selfmade
date: 2019/03/02 13:37:42
cover_index: /images/anleitung/platine/haefner-technology-platine-00cover-index.jpg
amazon: true
sitemap: true
---
Selbstgemachte Platinen sind der Traum jedes Elektronik-Bastlers. Doch nach den zwei Hürden "Schaltplan" und "Layout" kommt die vermutlich größte: Wie wird aus dem Plan ein tatsächliches Werkstück?
Steckbretter sind eher nur für den Nachweis der Machbarkeit geeignet und bei Lochrasterplatinen ist das Erstellen der Verbindsleitungen ziemlich umständlich.
Bei Bestellungen ist immer mit einer gewissen Lieferzeit zu rechnen. Selber Platinen ätzen ist aufwendig und eine ziemliche Sauerei. Und eine Platinenfräsmaschine werden die wenigsten Menschen privat besitzen...
Für alle, die einen 3D-Drucker haben, gibt es eine Alternative, die hier beschrieben wird:
<div class="box alt">
	<div class="row uniform">
		<div class="12u$"><span class="image fit"><a href="/images/anleitung/platine/haefner-technology-platine-00Titelbild.jpg"><img src="/images/anleitung/platine/haefner-technology-platine-00Titelbild.jpg" alt="Eine Platine aus dem 3D-Drucker" /></a></span></div>
	</div>
</div>

Gesamtkosten: ab ca. 25 €
Vorkenntnisse: Schaltungstechnik
Zeitaufwand: ca. 30 min

## Kurzfassung
Diese Anleitung beschreibt den Entwicklungsprozess, bei dem einige Versuche nicht funktioniert haben.
Die letztliche Lösung vorweg für alle Ungeduldigen: Die Leiterbahnen können mit Silberleitlack schnell und einfach hergestellt werden. Das funktioniert sogar ohne Löten und ist entsprechend vorallem auch für Anfäger gut geeignet.
Silberleitlack hat den Vorteil, dass er sehr leitfähig ist und dadurch die Widerstände der Leiterbahnen relativ gering werden.
Leider ist Silberleitlack relativ teuer. Man kann die Leiterbahnen mit gewöhnlichem Silberdraht verstärken, um an Leitlack zu sparen.

## Druckteile
Wer seine eigene Platine herstellen möchte muss sich natürlich auch ein eigenes Druckteil konstruieren. Gängige Platinen-Design-Programme (EDA) können das Layout so exportieren, dass es von 3D-Konstruktions-Programmen (CAD) einfach importiert werden kann.
In dieser Anleitung hat sich eine Platinendicke von 1&nbsp;mm und eine Leiterbahnen-Tiefe von 0,5&nbsp;mm bewährt.

## Einleitung

### Probleme
Ein 3D-Drucker ist durchaus in der Lage, Strukturen zu drucken, die fein genug für relativ kleine Elektronikbauteile sind. Da die gängigen 3D-Drucker im Privatbereich allerdings nur Kunststoffe drucken können, ergeben sich zwei Probleme:
 1. Wie kann das Kunststoffteil mit Metall beschichtet werden?
 2. Kann man schnell genug Löten, um nicht die gesamte Platine zu schmelzen?

### Ansätze
Folgende Lösungsansätze werden nun im Detail erläutert:
 - Kupferleitlack (günstig, aber eigentlich zum Galvanisieren)
 - Kupferpulver (günstig, Bahnen aus Vollkupfer)
 - Silberleitlack (teuer, nicht lötfähig)

### Generelles
Für alle Versuche gilt die Prämisse, dass sich eine Platine aus dem 3D-Drucker nur lohnt, wenn sie mit wenig Aufwand und "Sauerei" herstellbar ist.
Folgende Lösungsansätze wurden deshalb sofort wieder verworfen:
 - Die Leiterbahnen durch Galvanisieren aufbringen: Alle Leiterbahnen müssten elektrisch verbunden, und nach dem Vorgang wieder getrennt werden. Löchern müssten dann gebohrt werden. Wenn schon mit Chemikalien hantiert werden muss, dann können die Platinen auch gleich konventionell geätzt werden.
 - Platine negativ drucken und auf Gips o.Ä. übertragen. Leiterbahnen gießen (oder mit Kupfer-Zinn-Pulver sintern): Zu zeitintensiv und zu aufwendig mit vielen möglichen Fehlerquellen. Eine Bestellung wäre vermutlich fast schneller.
 - Leitfähige Tinte: Ist leider nicht leitfähig genug, sodass die Leiterbahnen mit zu hohen Widerstandswerten behaftet wären.
 
Der Arbeitsaufwand zur Fertigung der Platine sollte auf keinen Fall mehr sein, als alle Leiterbahnen noch einmal von Hand nachfahren zu müssen. Alle Leiterbahnen gleichzeitig wäre natürlich wünschenswert.
Um von der Rückseite ungestört arbeiten zu können, werden die meisten Bauteile auf der Vorderseite mit Epoxidharz bzw. Sekundenkleber fixiert. Beim Transistor sind die Löcher gerade klein genug geraten, dass er auch ohne Klebepunkt relativ stabil hält.

### Schaltung
Die Schaltung, die in dieser Anleitung exemplarisch dargestellt wird, schaltet per Magnet (Reed-Kontakt) einen Transistor, wodurch zwei LEDs bestromt werden und leuchten. Das Ausschalten wird durch einen Kondensator verzögert.
Alle Bauteile sind in Durchsteck-Technik (THT), Rastermaß 100&nbsp;mil, Leiterbahnbreite 50&nbsp;mil (wobei 25&nbsp;mil sicherlich auch wunderbar funktioniert hätten). Aufgrund ihrer Einfachheit wurde die Platine vollständig im CAD-Programm "FreeCAD" erstellt.
Wer noch mehr Details zum Projekt wissen möchte darf uns gerne kontaktieren!

## 1. und 2. Versuch Kupferleitlack
Die ersten zwei Versuche basieren auf der Beobachtung, dass Kupferleitlack leitend wird, wenn er - nach sehr langer Zeit - vollständig durchgetrocknet ist. Die Annahme war, dass dieser Prozess durch heiße Luft bzw. den Lötvorgang beschleunigt werden könnte.
Zum Einfüllen des Kupferleitlacks in die Aussparungen der Leiterbahnen wurde ein Stückchen Holz verwendet, das vorne spitz zulaufen ist. Das hat im ersten Versuch auch sehr gut funktioniert:
<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/platine/haefner-technology-platine-01kupfer1.jpg"><img src="/images/anleitung/platine/haefner-technology-platine-01kupfer1_small.jpg" alt="Leiterbahnen füllen" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Nachdem alle Leiterbahnen mit Kupferleitlack gefüllt waren, wurde dieser erste Versuch mit etwas warmer Luft getrocknet. Dabei zeigte der Lack enormen Volumenschwund:
<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/platine/haefner-technology-platine-02kupfer1.jpg"><img src="/images/anleitung/platine/haefner-technology-platine-02kupfer1_small.jpg" alt="Nur noch oberflächlich mit Kupfer bedeckt" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Außerdem ist der Lack so dünnflüssig, dass er zur Vorderseite durch drückt:
<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/platine/haefner-technology-platine-03kupfer1.jpg"><img src="/images/anleitung/platine/haefner-technology-platine-03kupfer1_small.jpg" alt="Kupferleitlack-Nasen auf der Vorderseite" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Beim zweiten Versuch wurden zuerst die Löcher von der Vorderseite mit Klebeband abgedichtet. Als Platzhalter für die später benötigten Löcher wurde Draht durch gestoßen:

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/platine/haefner-technology-platine-04kupfer2.jpg"><img src="/images/anleitung/platine/haefner-technology-platine-04kupfer2_small.jpg" alt="Vorbereitung für Versuch 2" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Beim Befüllen der Leiterbahnen mit Kupferleitlack konnte bestätigt werden, dass diese Abdichtung funktioniert. Bei diesem zweiten Versuch wurde der Trocknungsprozess durch heiße Luft beschleuningt. Dadurch wurde die Kuststoff-Platine allerdings beschädigt. Außerdem bildete sich eine Haut auf der Oberfläche des Kupferleitlacks, die anschließend zusammenfiel:

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/platine/haefner-technology-platine-05kupfer2.jpg"><img src="/images/anleitung/platine/haefner-technology-platine-05kupfer2_small.jpg" alt="Beschädigte Platine" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Trotz allem konnte auf den Leiterbahnen selbst nach mehreren Tagen keinerlei Leitfähigkeit gemessen werden.

## 3. Versuch Kupferpulver
Im zweiten Versuch fiel der Kupferlack in sich zusammen, nachdem sich an der Oberfläche eine Haut gebildet hatte und unterhalb das Lösungsmittel verdunstet war. Im dritten Versuch sollte dies behoben werden, indem die Leiterbahnen zuerst mit Kupferpulver befüllt wurden. Verklebt wurde dieses mit dem durchlötbaren Schutzlack "Plastik 70":

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/platine/haefner-technology-platine-06kupfer3.jpg"><img src="/images/anleitung/platine/haefner-technology-platine-06kupfer3_small.jpg" alt="Mit Kupferpulver gefüllte Leiterbahn" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

Danach wurde das Kupferpulver mit Kupferleitlack (wie in Versuch 1 & 2) vollends miteinander verklebt und anschließend gelötet. Bei einer auf diese Art und Weise produzierten Platine müssten vermutlich alle Leiterbahnen mit Lötzinn nachgezogen werden. Dies wäre vom Aufwand gerade noch im Rahmen des Akzeptablen. Leider konnte das Kupferpulver das Lötzinn jedoch nicht schnell genug aufnehmen, sodass eher die Kunststoffplatine schmilzt:

<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/platine/haefner-technology-platine-07kupfer3.jpg"><img src="/images/anleitung/platine/haefner-technology-platine-07kupfer3_small.jpg" alt="Angeschmolzene Platine mit kleiner Lötstelle" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

## 4. Versuch Silberleitlack

Parallel zum dritten Versuch wurde getestet, wie gut es möglich ist, Leiterbahnen mit einem Silberleitlack-Stift nachzuziehen. Diese Stifte werden normalerweise hauptsächlich zur Reparatur von Heckscheibenheizungen an KFZ verwendet. Das Ergebnis sieht sehr sauber und durchaus vielversprechend aus:
<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/platine/haefner-technology-platine-08silber.jpg"><img src="/images/anleitung/platine/haefner-technology-platine-08silber_small.jpg" alt="Test mit Silberleitlack" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>
Nach diesem Test wurde die Platine bestückt. Auch dieses Mal wurden alle Bauteile wieder mit Sekundenkleber auf die Vorderseite geklebt. Anders als bei "normalen" Leiterplatten wurde der überschüssige Draht auf der Rückseite allerdings nicht abgeschnitten, sondern in Form der Leiterbahnen gebogen. Zusätzlich wurde die lange Leiterbahn in mit gewöhnlichem Silberdraht verstärkt (mit Sekundenkleber fixiert):
<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/platine/haefner-technology-platine-09silber.jpg"><img src="/images/anleitung/platine/haefner-technology-platine-09silber_small.jpg" alt="Größtenteils Drähte statt Leiterbahnen" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>
Das Nachziehen der Leiterbahnen mit dem Silberleitlack-Stift ist ein Leichtes. Außerdem müssen nur die Stellen verbunden werden, an denen Drähte aufeinander treffen.
<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/platine/haefner-technology-platine-10silber.jpg"><img src="/images/anleitung/platine/haefner-technology-platine-10silber_small.jpg" alt="Leiterbahnen mit Silberleitlack fertig" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>
Das Aushärten dauert einige Minuten und kann mit warmer Luft beschleunigt werden. Nach ca. einer Stunde wurde die Platine getestet und hat auf Anhieb tadellos funktioniert! Durch das Aufkleben der Bauteile auf der Vorderseite sieht die Platine auch von Vorne sehr schön aufgeräumt aus:
<div class="box alt">
	<div class="row uniform">
		<div class="2u"></div>
		<div class="8u"><span class="image fit"><a href="/images/anleitung/platine/haefner-technology-platine-11silber.jpg"><img src="/images/anleitung/platine/haefner-technology-platine-11silber_small.jpg" alt="Leiterbahnen mit Silberleitlack fertig" /></a></span></div>
		<div class="2u$"></div>
	</div>
</div>

## Fazit
Platinen aus dem 3D-Drucker sind eine tolle Möglichkeit, um mäßig komplizierte Schaltungen schnell und (relativ) sauber zu fertigen.
Der Stift mit Silberleitlack ist mit 20&nbsp;€ zwar relativ teuer, allerdings ist er auch recht ergiebig.
Das Erstellen einer Leiterplatte ohne zu Löten ist zunächst einmal sehr ungewöhnlich. Allerdings wird dadurch auch eine ganz andere Arbeitsweise ermöglicht: In die Gräben der Leiterbahnen kann Draht geklebt werden und die dritte Dimension ist viel einfacher nutzbar.

3D-gedruckte Platinen mit Silberleitlack sind für zwei Nischen besonders geignet:
 1. Um sehr schnell und unkompliziert eine Schaltung aufzubauen
 2. Um eine Schaltung direkt ins 3D-gedruckte Gehäuse zu integrieren

## Verwendetes Werkzeug und Material

 * [Silber-Leitlack-Stift](https://www.amazon.de/gp/product/B00LTWQWXW/ref=as_li_tl?ie=UTF8&camp=1638&creative=6742&creativeASIN=B00LTWQWXW&linkCode=as2&tag=platine05-21&linkId=e39a051cc46b80f8f5bffd323a5df33d)
 * Sekundenkleber
 * 2K-Epoxidharz
 * [Kupferleitlack](https://www.amazon.de/gp/product/B00II26VE6/ref=as_li_tl?ie=UTF8&camp=1638&creative=6742&creativeASIN=B00II26VE6&linkCode=as2&tag=platine05-21&linkId=05ef4686c38299a9db4590c366966db1)
 * [Durchlötbarer Schutzlack "Plastik70"](https://www.amazon.de/gp/product/B000YIW7GS/ref=as_li_tl?ie=UTF8&camp=1638&creative=6742&creativeASIN=B000YIW7GS&linkCode=as2&tag=platine05-21&linkId=e71ce265a99c0e699fc316dfe5f3c875)
 * [Atemschutzmaske](https://www.amazon.de/gp/product/B008BHHZUM/ref=as_li_tl?ie=UTF8&camp=1638&creative=6742&creativeASIN=B008BHHZUM&linkCode=as2&tag=platine05-21&linkId=a709a18654974324ed615e129f40cb33)

