---
title: Web Entwicklung
layout: page
cover_index: /images/software-development.jpg
cover: /images/software-development.jpg
---

## Homepage Entwicklung

Wir bieten lokalen Vereinen und Gesch&auml;ften Dienste rund um das Erstellen und betreiben von Homepages und Websiten.

### Referenzen

 * [Kastell-Apotheke Osterburken](https://kastell-apotheke.de/): Lokale Apotheke
     Fokus auf sehr guter Performance und offline F&auml;higkeit
 * [Tri-Moraine Audubon Society](http://tri-moraineaudubon.org/) : Lokaler Verein, Teil der Audubon Society einer US-amerikanische Non-Profit-Umweltorganisation
     Fokus auf konsistenter Darstellung der statischen Inhalte auch auf mobilen Ger&auml;ten
 * [Solar B&uuml;rgerAktiv Hildrizhausen GbR / Photovoltaik Hildrizhausen GbR](https://christophhaefner.de/SolarGbR/): Lokale GbR zur gemeinsamen Finanzierung und Instandhaltung einer Solaranlage
     Fokus auf konsistenter Darstellung der statischen Inhalte und einfacher Bedienung auch auf mobilen Ger&auml;ten

## App Entwicklung

Wir bieten unseren Kunden ma&szlig;geschneiderte und platformunabh&auml;ngige L&ouml;sungen dank moderner Web-Technologien (Cordova, Ionic, React, PWA).
