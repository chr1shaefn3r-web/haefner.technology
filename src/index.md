---
title: Häfner Technology
layout: page
cover: /images/haefner.technology.logo.png
---

<section class="tiles">
	<article class="style1">
		<span class="image"><img src="/images/anleitung/lithophan-led-bilderrahmen/haefner-technology-lithophan-bilderrahmen-cover-index.jpg" alt="Lithophan LED Bilderrahmen" /></span>
		<a href="/categories/Anleitung/">
			<h2>Anleitungen</h2>
			<div class="content">
				<p>Anleitungen zum Selbstbau</p>
			</div>
		</a>
	</article>
	<article class="style4">
		<span class="image"><img src="images/3dprinting.jpg" alt="3D-Printing" /></span>
		<a href="3dprinting.html">
			<h2>3D-Printing</h2>
			<div class="content">
				<p>Services around 3D printing</p>
			</div>
		</a>
	</article>
	<article class="style5">
		<span class="image"><img src="images/software-development.jpg" alt="Web Entwicklung" /></span>
		<a href="web-development.html">
			<h2>Web Entwicklung</h2>
			<div class="content">
				<p>Homepage- und App-Entwicklung</p>
			</div>
		</a>
	</article>
	<article class="style7">
		<span class="image"><img src="images/hARTware.jpg" alt="hARTware" /></span>
		<a href="https://christophhaefner.de/hARTware/">
			<h2>hARTware</h2>
			<div class="content">
				<p>Recycled PC Parts on Canvas</p>
			</div>
		</a>
	</article>
</section>

<section>
	Wir unterst&uuml;tzen ein werbefreies und offenes Internet: <a href="https://brave.com/hfn794">Brave-Browser</a>
</section>

